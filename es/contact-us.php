<?php

return[


	'share-head'=>"<h6>Comparte</h6><h5>tu visión</h5>",

	'outlook-text'=>"<p>Haz una sugerencia o propuesta para el futuro de la plataforma #LocalizingSDGs.</p> <p>Por favor, dinos qué significa “localizar” para ti, qué tipo de herramientas, programas y servicios marcan la diferencia en la vida del gobierno y de los ciudadanos, y cómo un miembro de esta comunidad te ha ayudado.</p> <p>Agradecemos tus comentarios.</p><br><br>
	<a href='javascript:void(0);' class='contact-link' onclick='$('#outlook-form').show();'>Haz clic aquí para compartir tu visión</a>",

	'name'=>"Nombre",
	'email'=>"Correo electrónico",
	'organization'=>"Organización",
	'designation'=>"Posición",
	'coments'=>"Comentarios",
	'authorize'=>"Autorizo a esta iniciativa a publicar mis mensajes en la plataforma online y publicaciones relacionadas.",
	'wish'=>"Deseo permanecer anónimo.",
	'send-me'=>"Envíeme una copia del correo.",
	'enter Verification'=>"Introducir código de verificación",
	'phone'=>"Número de teléfono",
	'comments'=>"Tus comentarios o preguntas en detalle",
	'web-link'=>"Vínculo web del asunto",
	
	'send-head'=>"<h6>Enviar</h6><h5>Comentarios Técnicos</h5>",

	'technical-text'=>"<p>¿Tuviste problemas técnicos en la página web? Comparte sus comentarios</p>

						<p><strong>Algunas orientaciones:</strong><br/> 
						Por favor informa sobre un sólo problema por mensaje. Describe brevemente el asunto a continuación.</p>

						<p>Explica cómo llegar hasta el problema: qué pasos diste, qué funcionó mal y qué crees que debería haber ocurrido. Incluye ejemplos y todos los mensajes de error que haya recibido.</p>
						<a href='javascript:void(0);' class='contact-link' onclick='$('#feedback-form').show();'>Haz clic aquí para informar sobre problemas y dar comentarios</a>",

	'tech-name'=>"Nombre",
	'tech-email'=>"Correo electrónico",
	'tech-Phone'=>"Número de teléfono",
	'tech-organization'=>"Organización",
	'tech-coments'=>"Tus comentarios o preguntas en detalle",
	'tech-Web-Link'=>"Vínculo web del asunto",
	'tech-Enter-Verification'=>"Introducir código de verificación ",
	
	'contact-head'=>"<h6>Detalles</h6><h5>de Contacto</h5>",

	'gtf-head'=>"Grupo de Trabajo Global de Gobiernos Locales y Regionales",

	'gtf-sub-head'=>"GTF",

	'gtf-address'=>"Ponte en contacto con GTF si desea más información sobre el trabajo de los gobiernos locales y regionales sobre localización y la abogacíainternacional de los ODS.",

	// 'gtf-location'=>"Suite 5110, HCI Building<br/>1125 Colonel By Drive, Ottawa, K1S 586",

	// 'gtf-email'=>"ottawa@undp.com",

	// 'gtf-website'=>"www.gtf.com",



	'un-head'=>"Programa de Asentamientos Humanos de las Naciones Unidas",

	'un-sub-head'=>"ONU-Hábitat",

	'un-address'=>"Ponte en contacto con ONU-Hábitat si estás interesado en saber más acerca de la acción de la ONU hacia un mejor futuro urbano: donde las ciudades se convierten en agentes inclusivos y accesibles de crecimiento económico y desarrollo social.",

	// 'un-location' =>"Luisenstrasse 40<br/>10117 Berlin",

	// 'un-email' =>"berlin@undp.com",

	// 'un-website' =>"www.unhabitat.com",



	'undp-head'=>"Programa de las Naciones Unidas para el Desarrollo",

	'undp-sub-head'=>"PNUD",

	'undp-address'=>"Contacta al PNUD si deseas más información sobre el apoyo proporcionado por la ONU a los países socios en la aplicación eficaz de la nueva agenda de desarrollo a nivel local, hacia el logro de una prosperidad económica a largo plazo, bienestar humano y protección del medio ambiente.",
					  
	// 'undp-location' =>"One United Nations Plaza<br />New York, NY 10017 USA",

	// 'undp-email' =>"info@undp.com",

	// 'undp-website' =>"www.undp.com",

	'outlook-confirm'=>"GRACIAS POR COMPARTIR TU VISIÓN",

	'outlook-confirm-text'=>"Tu punto de vista es muy importante para nosotros. Nos ayuda a entender qué es lo más importante para ti. Nos aseguraremos de leer y tener en cuenta tus sugerencias y experiencias, y para ello podríamos ponernos en contacto contigo.",

	'report-confirm'=>"GRACIAS POR INFORMAR SOBRE EL PROBLEMA",

	'report-confirm-text'=>"Esperamos solucionar el problema cuanto antes.",


];
<?php
//es
return[

	'register'=>[
	    'subject'=>'Confirma tu registro ',
	    'body'=>'Estimado usuario, <br><br>
	        Gracias por tu registro para unirte a la comunidad Localizando los ODS.<br>
                <br>
                Estás a un paso de acceder a conocimientos, herramientas, recursos, historias, debates y eventos. A través de los mismos, podrás transformar los Objetivos de Desarrollo Sostenible en acción.<br>
                <br>
                <br>
                Haz clic en el siguiente enlace de confirmación para comenzar. Si no puedes hacer clic en el enlace directamente, por favor copiálo y pégalo en en la barra de direcciones de tu navegador.
                <br><br>
                <a href=":link">:link</a>
                <br><br>
                <br><br>
                Para cualquier ayuda o sugerencia, por favor comunícate con nosotros por medio de la sección <a href=":contact">Contacto</a> y te responderemos en breve.
                <br><br>
                Saludos cordiales, <br>
                El equipo<br><br>',
	    ],
	'newUser'=>[
	    'subject'=>'Bienvenido/a a la comunidad Localizando los ODS',
	    "body"=>"
	        Estimado usuario, <br><br>
            Te damos la bienvenida a la comunidad Localizando los ODS.<br><br>
            
            Tu cuenta ya está activa, ahora te recomendamos actualizar tu perfil. Un perfil más completo permitirá que los demás usuarios tengan una mejor idea de quién eres, facilitando la interacción entre ustedes.<br><br>
            
            Esperamos tu participación y aporte! <br><br>
            
            Para cualquier ayuda o sugerencia, por favor comunícate con nosotros por medio de la sección <a href=':contact'>Contacto</a> y te responderemos en breve. <br><br>
            
            Saludos cordiales,<br>
            
            El equipo<br><br>
	    ",
	    ],
	'forgot'=>[
	    'subject'=>"Restablecer tu contraseña",
	    'body'=>"Estimado usuario, <br><br>
                Restablecer tu contraseña es sencillo.<br>
                <br>
                Por favor, haz clic en el siguiente enlace y rellena los datos que se te solicitan. Si no puedes hacer clic en el enlace directamente, puedes también copiarlo y pegarlo en la barra de direcciones de tu navegador.<br>
                <br>
                <br>
                <a href=':link'>:link</a>
                <br>
                Esperamos verte de vuelta muy pronto.<br><br>
                ¡Que tengas un buen día!<br>
                El equipo<br><br>",
	    ],
	 'library'=>[
	     "subject"=>"Tu documento ha sido añadido",
	     "body"=>"Estimado usuario,<br><br>
                ¡Enhorabuena! <br><br>

                Tu documento ha sido añadido con éxito y está siendo revisado. Pronto recibirás noticias de nuestro equipo sobre el estado de tu solicitud - dentro de 15 días hábiles. <br><br>
                
                Si dentro de este plazono recibes noticias nuestras, puedes contactarnos a través de la sección <a href=':contact'>Contacto</a> en el sitio web.  <br><br>

                
                ¡Que tengas un buen día!<br>
                
                El equipo<br><br>",
	     ],
	 'story'=>[
	     "subject"=>"Tu nueva historia ha sido añadida",
	     "body"=>"Estimado usuario,<br><br>
                ¡ Enhorabuena!<br><br>

                Tu historia ha sido enviada con éxito y está siendo revisada.
Pronto irás recibir noticias de nuestro equipo sobre el estado de tu solicitud - dentro de 15 días hábiles.
<br><br>
                
                Si dentro de este plazo no recibes noticias nuestras, puedes contactarnos a través de la sección <a href=':contact'>Contacto </a> en el sitio web.  <br>
                Gracias nuevamente por compartir tu historia con la comunidad. <br><br>

                
                Saludos cordiales, <br>
                
                El equipo<br><br>",
	     ],
	 'event'=>[
	     "subject"=>"Tu evento",
	     "body"=>"Estimado usuario,<br><br>
               ¡ Enhorabuena!<br><br>

                Tu evento ha sido añadido con éxito y está siendo revisado. Pronto recibirás noticias de nuestro equipo sobre el estado de tu solicitud - dentro de 15 días hábiles.<br><br>
                
                Si dentro de este plazo no recibes noticias nuestras, puedes contactarnos a través de la sección <a href=':contact'>Contacto </a> en el sitio web.  <br>
                Gracias por compartir tu evento.<br><br>

                
                Saludos cordiales, <br>
                
                El equipo<br><br>",
	     ],
	  
	 'discussion'=>[
	     "subject"=>"Tu propuesta de debate",
	     "body"=>"Madame, Monsieur,<br><br>
                Félicitations! <br><br>

                Votre sujet de discussion a été ajouté et est en train d’être examiné.<br><br>
                Notre équipe va bientôt vous contacter concernant votre contribution, dans les prochains 15 jours ouvrables. <br><br>
                
                Au cas où vous ne recevrez pas de réponse de notre part dans les prochains 15 jours ouvrables, vous pouvez nous joindre à travers la section <a href=':contact'>Contactez-nous</a> sur le site web.  <br>
                Merci pour votre contribution,<br><br>

                
                L'équipe<br><br>",
	     ],
	 'libraryApp'=>[
	     'subject'=>'Tu documento está en línea',
	     'body'=>"Estimado usuario,<br><br>
                ¡ Enhorabuena! <br><br>

                Tu documento ha sido aprobado y fue publicado con éxito en el sitio web. Estamos seguros que será útil a los miembros de nuestra comunidad.<br><br>
                Puedes ver el documento aquí  (<a href=':link'>:link</a>) . <br><br>
                Muchas gracias por tu contribución y contamos con tu participación. <br><br>

                
                Saludos cordiales, <br>
                
               El equipo<br><br>",
	     ],
	 'storyApp'=>[
	     'subject'=>'Tu historia está en línea',
	     'body'=>"Estimado usuario,<br><br>
                    ¡ Enhorabuena! <br><br>

                    Tu historia ha sido aprobada y fue publicada con éxito en el sitio web. <br><br>
                    Puedes ver tu historia aquí (<a href=':link'>:link</a>) . <br><br>
                    Te animamos a responder a cualquier consultas, reacciones o comentarios que recibas de nuestra comunidad, ya que tu participación puede ayudar nuestros miembros a entender mejor tus puntos de vista.<br><br>
                    Muchas gracias por tu contribución y contamos con su participación. <br><br>

                    
                    
            Saludos cordiales, <br>
                    
                    El equipo<br><br>",
	     ],
	 'eventApp'=>[
	     'subject'=>'Tu evento está en línea',
	     'body'=>"Estimado usuario,<br><br>¡ Enhorabuena! <br><br>

                Tu evento ha sido aprobado y fue publicada con éxito en el sitio web.<br><br>
                Puedes ver tu evento aquí:<a href=':link'>:link</a> . <br><br>
               El evento es visible para toda la comunidad.<br><br>
                Muchas gracias por tu contribución y esperamos tener noticias tuyas pronto. <br><br>

                
               Saludos cordiales, <br>
                
                El equipo<br><br>",
	     ],
	   'discussionApp'=>[
	       'subject'=>'Tu propuesta de debate',
	       'body'=>"Estimado usuario,<br><br>
                    ¡ Enhorabuena!<br><br>

                    El tema propuesto para el debate ':topic' ha sido aprobado. Por favor escribe a <a href='mailto:info.art@undp.org'>info.art@undp.org</a>, e indica cuando estarás disponible para debatir con nuestra comunidad. <br><br>
                   
                    AComo serás el moderador oficial, te animamos a monitorear y responder activamente a los comentarios de todos los usuarios para una interacción viva y multifacética.<br><br>
                    Muchas gracias por tu contribución y esperamos tener noticias tuyas pronto.<br><br>

                    
                    Saludos cordiales, <br>
                    
                    El equipo<br><br>"
	       ],
	   'discussionCom'=>[
	       'subject'=>'Nuevo comentario en tu debate',
	       'body'=>"Estimado usuario,<br><br>

                   Un nuevo comentario fue agregado en tu debate  '<strong>:topic</strong>' por  '<strong>:user</strong>'<br><br>
                   Comentario:<br><strong>:comment</strong><br><br>
                   Enlace al debate - <br>
                   <a href=':link'>:link</a> <br><br>
                    Te animamos a monitorear y responder activamente a los comentarios de todos los usuarios para una interacción viva y multifacética. <br><br>

                    
                    Cordialmente,<br>
                    
                    El equipo<br><br>",
	       ],
	   'storyCom'=>[
	       'subject'=>'Nuevo comentario en tu historia  ',
	       'body'=>"Estimado usuario, <br><br>

                    Un nuevo comentario fue agregado en tu historia '<strong>:topic</strong>' por  '<strong>:user</strong>'<br><br>
                   Comentario:<br><strong>:comment</strong><br><br>
                   Enlace a la historia - <br>
                   <a href=':link'>:link</a> <br><br>
                    Te animamos a monitorear y responder activamente a los comentarios de todos los usuarios para una interacción viva y multifacética. <br><br>

                    
                  Cordialmente,<br>
                    
                   El equipo<br><br>",
	       ],
	  'outlook'=>[
	      'subject'=>'Gracias por tus aportaciones',
	      'body'=>"Estimado usuario,<br><br>        
                Gracias por compartir tus pensamientos. Consideramos tus comentarios muy útiles, y estaremos en contacto para discusiones adicionales cuando surja la oportunidad. <br><br>
                Nos alegraría recibir noticias tuyas. <br><br>

                
                Saludos cordiales, <br>
                
                El equipo<br><br>"
	      ],
	  'feedback'=>[
	      'subject'=>'Tus comentarios técnicos',
	      'body'=>"Estimado usuario, <br><br>

                Te damos las gracias por compartir tu comentario y por sugerir cómo mejorar la plataforma. Nuestro equipo técnico está examinando este asunto y deberíamos poder resolver el problema rápidamente. Te escribiremos en caso de que surja alguna pregunta adicional. <br><br>
                Gracias por alertarnos sobre el problema, y nos disculpamos por cualquier inconveniencia que esto pudo haber causado.<br><br>
                Esperamos contar con tu activa participación en la comunidad.  <br><br>

                
                Saludos cordiales, <br>
                
                El equipo<br><br>"
	      ],
	   
	      'sign'=>"<span style='color:rgb(127,127,127);font-weight:bold;font-size:12px'>Obtén ayuda Localizando los ODS</span><br>
                <span style='color:rgb(127,127,127);font-size:12px'>Aunque que no sea posible contestar a este correo electrónico, tu puedes contactarnos a través de nuestra sección <a href='".url('/contact-us')."'>Contacto</a> para asistencia.</span>",



];
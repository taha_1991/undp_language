<?php

return[
	
	'edit'=>"<h6>EDITAR</h6> <h5>PERFIL</h5>",

	'please'=>"Por favor, complete la información que le gustaría compartir con los miembros de esta comunidad.",

	'upload'=>"Subir imagen de perfil",

	'name'=>"Nombre",

	'organ'=>"Organización",

	'desig'=>"Puesto",
	
	'Contact'=>"Contacto",

	'location'=>"Localización o dirección",

	'telphone'=>"Número de teléfono",

	'email'=>"Dirección de correo electrónico",

	'facebook'=>"Enlace a la página de Facebook",

	'twitter'=>"Enlace a Twitter",

	'profile'=>"Enlace al perfil de LinkedIn",

	'website'=>"Enlace a sitio web",

	'about-me'=>"Sobre mí (máximo 300 palabras)",

	'pswd'=>"CAMBIA LA CONTRASEÑA",

	'old-pswd'=>"Contraseña anterior",

	'new-pswd'=>"Nueva contraseña",

	'retype'=>"Reescriba nueva contraseña",

	'change-pswd'=>"Cambia la contraseña",

	'pswd-must'=>"Las contraseñas deben:",

	'characters'=>"Incluir por lo menos 8 caracteres.",

	'numbers'=>"Incluir una combinación de letras y números.",

	'special-characters'=>"No incluir ningún carácter especial.",

	'log-out'=>"CERRAR SESIÓN",

	'logged-out'=>"Has cerrado la sesión.",

	'click-here'=>"Haz clic aquí para volver a la página de inicio.",

];
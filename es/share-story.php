<?php

return[
	
	'share-story-head'=>"<h6>COMPARTE TU </h6><h5>HISTORIA</h5>",

	'share-story-text'=>"<p>Podrías tener una historia atractiva para contar sobre los ODS a nivel local. Ya sea un camino personal en torno al desarrollo local, una nueva iniciativa que te entusiasma, desafíos a los que te enfrentas o la manera en la que generaste un impacto positivo. Nos encantaría conectarte a esta comunidad.</p> <p>También, por supuesto, te invitamos a evaluar tu experiencia en el uso de las herramientas presentadas en esta plataforma. Esto nos ayudará a mejorar la experiencia y ayudará a otros a tomar decisiones más informadas. Queremos que nuestros usuarios puedan encontrar y publicar la información de forma rápida.</p> <p>Por favor ten en cuenta que podríamos necesitar editar las historias y los títulos por razón de extensión, claridad y orientaciones editoriales.</p>",

	'title'=>"Título de la historia",

	'story'=>"Historia",

	'cover'=>"Foto",

	'country'=>"País",
	
	'select'=>"Seleccione País",

	'province'=>"Provincia / Estado",

	'select-province'=>"Seleccione Provincia / Estado",

	'city'=>"Ciudad",

	'select-city'=>"Seleccione ciudad",

	'publications'=>"Autorizo a esta iniciativa a citar mi historia en sus campañas y publicaciones.",

	'mail'=>"Enviarme una copia del correo electrónico.",

	'thank'=>"¡GRACIAS POR COMPARTIR TU HISTORIA CON NOSOTROS!",

	'approval'=>"Tu historia aparecerá en línea tan pronto sea aprobada.",

	'notification'=>"Recibirás una notificación.",

	'stories'=>"Hasta entonces, revisa las otras <a href=':link'><strong>Historias</strong></a>",

		'Choose-file'=>"Elegir un archivo",

		'image-should'=>"La imagen debe estar en formato PDF y no exceder 2Mb",




];
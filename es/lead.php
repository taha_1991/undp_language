<?php

return[
	
	'lead-head'=>"<h6>LIDERAR </h6><h5>UN DEBATE</h5>",

	'lead-text'=>"Inicia conversaciones sobre temas e ideas relevantes y busca distintas perspectivas sobre cómo alcanzar los ODS a nivel local. Podrás sugerir un tema, y podríamos elegir a tu organización para moderar de manera oficial nuestro próximo debate.",

	'share'=>"Compartetu pregunta",

	'intro'=>"Introducción al debate",

	'file'=>"Archivo",

	'edit'=>"Editar",
	
	'insert'=>"Insertar",

	'view'=>"Vista",

	'format'=>"Formato",

	'table'=>"Tabla",

	'tools'=>"Herramientas",

	'upload'=>"Subir Foto / Logo",

	'choose'=>"<p><strong>Elegir un archivo</strong></p> <br/><p>La imagen debe estar en formato PDF y no exceder 2Mb.</p>",

	'suggest'=>"Sugerir un tema",

	'thank'=>"GRACIAS POR EXPRESAR TU INTERÉS EN MODERAR UNA DISCUSIÓN.",

	'aim'=>"¡Esperamos que puedas ser moderador!",

	'notify'=>"Te informaremos si la discusión que sugieres se activa.",

	'until'=>"Hasta entonces, revisa los <a href=':link'><strong>Últimos Debates</strong></a>",


];
<?php

return[
 

'create-head'=>"<h6>CREAR</h6> <h5>CUENTA</h5>",

'create-text'=>"¡Crea tu cuenta y obtén acceso completo! En cuanto te registres, podrás compartir historias, participar en debates, publicar eventos, publicar documentos en la biblioteca y conectarte con otros miembros de la comunidad.",

'email'=>"Correo electrónico",

'retype-email'=>"Repite tu correo electrónico",

'confirmation'=>"Se te enviará una confirmación a tu correo electrónico. Este servirá para que puedas iniciar sesión.",

'pswd'=>"Contraseña",

'retype'=>"Repite tu contraseña",

'pswd-must'=>"Las contraseñas deben:",

'pswd-must-one'=>"Incluir al menos 8 caracteres.",

'pswd-must-two'=>"Incluir una combinacion de letras y números.",

'pswd-must-three'=>"No incluir ningún carácter especial.",
 
'personal'=>"Perfil personal",

'personal-name'=>"Nombre",

'personal-last'=>"Apellidos",

'personal-organ'=>"Organización",

'country'=>"País",

'province'=>"Provincia / Estado",

'city'=>"Ciudad",

	'profile-photo'=>"Foto de perfil",

	'choose-file'=>"<strong>Elige un archivo<strong>",

	'images-should'=>"Las imágenes deben estar en formato PDF y no superar los 2 MB",

'promotional-head'=>"<h4>Información promocional</h4>",

'promotional-text'=>"Deseo recibir actualizaciones mensuales sobre el nuevo contenido en el sitio web.",

'verify'=>"Comprobar el registro",

'verify-text'=>"Introduce el texto que aparece aquí:",

'agree'=>"Estoy de acuerdo con los <a href=':terms' target='_blank'>Términos y Condiciones</a> y con la <a target='_blank' href=':privacy'>Política de Privacidad.</a>",

'thankyou'=>"¡GRACIAS POR FORMAR PARTE DE ESTA COMUNIDAD! TE DAMOS LA BIENVENIDA COMO MIEMBRO ACTIVO.",

'thankyou-text'=>"<p>Te hemos enviado un correo electrónico de confirmación con un enlace para la activación de tu cuenta. Por favor, revisa tu correo electrónico y haz clic en el enlace.</p>
<p>Si no recibiste el correo electrónico, por favor revisa tu carpeta de correo no deseado. Si todavía no encuentras el correo, por favor haz clic aquí:</p>",

'resend'=>"Reenviar correo electrónico",

'active'=>"¡Tu cuenta está activa!",

'update'=>"Actualiza tu <a href=':link'>Página de Perfil</a> para que los miembros de la comunidad puedan establecer posibles colaboraciones. Tu perfil muestra quién es eres y te ayuda a interactuar.",

'continue'=>"Sigue Explorando",
'org'=>'e.g. Programa de las Naciones Unidas para el Desarrollo (PNUD)',

];
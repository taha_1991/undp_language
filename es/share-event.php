<?php

return[
	
	'share-event-head'=>"<h6>COMPARTE UN</h6> <h5>EVENTO</h5>",

	'share-event-text'=>"<p>Este espacio está abierto a las organizaciones, individuos y comunidades para la promoción de sus eventos. Nos encantaría conocer eventos que sumen voces al mundo del desarrollo local.</p>
	<p>Puedes solicitar presentar tus eventos, como talleres interactivos, charlas en la comunidad, conferencias, cumbres, etc., relacionadas con la localización de los ODS. Esto permitirá a tu comunidad involucrarse contigo.</p>
	<p>Para asegurar que el calendario muestra contenido relevante, todas las solicitudes serán aprobadas antes de su publicación.</p>",

	'title'=>"Título del Evento",

	'organ'=>"Organizadores del Evento",

	'description'=>"Descripción",

	'file'=>"Archivo",
	
	'edit'=>"Editar",

	'insert'=>"Insertar",
 
	'view'=>"Vista",

	'format'=>"Formato",

	'table'=>"Tabla",

	'tools'=>"Herramientas",

	'date'=>"Fecha",

	'Seledate'=>"Seleccionar fecha",

	'time'=>"Hora",

	'Selefrom'=>"Desde",

	'Seleto'=> "Hasta",

	'country'=>"País",

	'Selecoun'=>"Seleccionar país",

	'state'=>"Provincia / Estado",

	'Selestate'=>"SeleccionarProvincia / Estado",

	'city'=>"Ciudad",

	'Selecity'=>"Seleccionar ciudad",

	'venue'=>"Lugar del evento",

	'cover-photo'=>"Imagen",

	'upload'=>"Añadir información adicionaln",

	'add-more-docu'=>"Añadir más documentos",

	'agenda-pdf'=>"Por favor indica el título de tu documento para indicar su contenido (p.ej. nota de concepto / agenda.pdf)",

	'web-link'=>"Enlace web del evento",

	'thank-you'=>"GRACIAS POR COMPARTIR ESTE EVENTO.",

	'it-will'=>"Se promocionará en esta web tan pronto como sea aprobado por el moderador.",

	'you-will'=>"Recibirás una notificación.",

	'until-then'=>"Hasta entonces, revisa otros <a href=':link'><strong>Eventos</strong></a>",

	'choose-file'=>"Elegir un archivo",

	'image-should'=>"Las imágenes deben estar en formato PDF y no superar los 2 MB",



]; 
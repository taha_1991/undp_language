<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'search-head'=>"<h6>Buscar</h6> <h5>en la Biblioteca</h5>",
	'by-type'=>"Por Tipo",
	'by-actor'=>"Por Actor",
	'by-region'=>"Por Región",
	'by-language'=>"Por Idioma",
	'by-tool'=>"Por Herramienta",
	'search-text'=>"Esta iniciativa está evolucionando, y actores como tú están compartiendo recursos. Aquí encontrarás los documentos e instrumentos recogidos hasta el momento.",
	'background'=>"ANTECEDENTES Y CONTEXTO: AGENDA 2030",
	'initial'=>"INCIANDO EL PROCESO DE LOS ODS",
	'enable'=>"FACILITANDO ARREGLOS INSTITUCIONALES PARA LA IMPLEMENTACIÓN DE LOS ODS",
	'capacity'=>"FORTALECIMIENTO DE CAPACIDADES",
	'recent'=>"<h6>Contenido</h6>
			<h5>Reciente</h5>",
	'ad-head'=>"¿Tienes algún documento o herramienta que quieres aportar?",
	'ad-text'=>"¡Nos encantaría tenerlo!",
	'ad-btn'=>"Contribuye",
	//search-results
	'search-results'=>"<h6>RESULTADOS</h6>
			<h5>DE BÚSQUEDA PARA:</h5>",
	'concept'=>"Notas conceptuales y documentos",
	'case'=>"Estudios de casos y buenas prácticas",
    'guidance'=>"Guías y sistematización de experiencias",
    'tools'=>"Herramientas",
    //library-inner
    'info'=>"Información",
    'share'=>"Compartir",
    'publish'=>"¡Tú también puedes publicar!",
    'available'=>"Disponible en inglés, francés y español",
    'downlaod'=>"Descargar PDF",
    'favorites'=>"Agregar a favoritos",
    'report'=>"Informar sobre un problema",
    'related'=>"<h6>Publicaciones</h6>
			<h5>relacionadas</h5> ",
    'report-text'=>"*Se espera que losusuarios se adhieran a los <a href='".url('/terms')."'>Términos y Condiciones</a> de este sitio web.",
    
];

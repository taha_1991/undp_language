<?php

return[

	'stories-discussions-head'=>"<h5>HISTORIAS, DEBATES Y EVENTOS</h5>",

	'stories-text'=>"Un espacio para intercambiar ideas, experiencias y eventos para todos.",

	'local-head'=>"<h6>HISTORIAS</h6><h5>LOCALES</h5>",

	'current-head'=>"<h6>DEBATES</h6><h5>ACTUALES</h5>",

	'events-head'=>"<h5>EVENTOS</h5>",

	'share-btn'=>"Comparte tu Historia",

	'lead-btn'=>"Sugiere un Tema o Lidere un Debate",

	'post-btn'=>"Publica un evento",

	'stories-head'=>"<h6>Historias</h6><h5>más recientes</h5>",

	'your-stories'=>"Tus historias son el centro de todo. Ya sean intentos o éxitos, reveladoras o inspiradoras, nos encantaría escuchar tu historia y considerarla para su publicación.",

	'share-your-story-btn'=>"Comparte tu historia",

	'discussion-head'=>"<h5>Debates</h5>",

	'evnt-head'=>"<h5>Eventos</h5>",

	'have-any'=>"¿Tienes algúna historia que quieres aportar?",
	
	'we-love'=>"¡Nos encantaría tenerla!",
				
	'share-button'=>"Comparte tu historia",

	'facebook-btn'=>"Compartir en Facebook",

	'twitter-btn'=>"Compartir en Twitter",

	'email-btn'=>"Compartir via Email",

	'contact-btn'=>"Contactar al colaborador",

	'add-to'=>"Añadir a favoritos",
	
	'report-btn'=>"Informar sobre un problema",

	'other-head'=>"<h6>Otras</h6><h5>historias</h5>",

	'comments'=>"Comentarios",

	'write-comm'=>"Escribe sus comentarios",

	'post'=>"Publica",

	'responses'=>"Respuestas",

	'reply'=>"Responder",

	'report'=>"Reportar",

	'today'=>"Hoy",

	'yesterday'=>"Ayer",

	'days-ago'=>"Hace :number días ",

	'administrator'=>"*Los comentarios podrán ser controlados si se estiman no adecuados u ofensivos. El criterio es exclusivo del administrador.",

	'latest-head'=>"<h6>Últimos</h6><h5>Debates</h5>",

	'chat-responses'=>"Respuestas",

	'we-like-to'=>"Nos gustaría debatir sobre temas importantes para ti. Si tu organización desea liderar un debate sobre un tema relevante para la comunidad #LocalizingSDG, estaríamos encantados de tenerte como nuestro próximo moderador.",

	'suggest-btn'=>"Sugiere un Tema",

	'previous-heading'=>"<h6>Discusiones</h6><h5>anteriores</h5>",

	'stories-heading'=>"Historias",

	'events-heading'=>"Eventos",

	'does-your'=>"¿Querría tu Organización iniciar un debate?",

	'featured'=>"¡Nos encantaría publicarlo!",

	'suggest-a-topic'=>"Sugiere un Tema",

	'feature'=>"Agregar un Comentario",

	'other-current'=>"<h6>Otras</h6><h5>Debates Actuales</h5>",

	'previous-discussions'=>"<h6>Debates</h6><h5>Anteriores</h5>",

	'we-look-forward'=>"Esperamos recibir tus insumos y perspectivas. Mientras formulas tus respuestas, será útil que leas los comentarios anteriores. Esto te permitirá avanzar en la conversación, construyendo sobre las publicaciones anteriores. Ayudará a que la discusión sea concisa y relevante. Incluso podrás enseñarnos algo nuevo, o aplicar un concepto de una manera nueva.",

	'search-head'=>"<h6>Busca eventos </h6><h5>próximos a ti</h5>",

	'searchbar-text'=>"Buscar",

	'upcoming-head'=>"<h6>Próximos</h6><h5>Eventos</h5>",

	'help-us'=>"Ayúdanos a unir las voces que promueven la localización de los ODS. Compartiendo un evento que ayude a promover la iniciativa, crearás más oportunidades para que personas y actores se involucren y beneficien. Podemos promocionar su evento – sea una conferencia, seminario o taller – en la comunidad #LocalizingSDGs.",

	'submit-an-event'=>"Anuncia un Evento",

	'past-head'=>"<h6>Evento</h6><h5>Pasado<h5>",

	'promote'=>"¿Tienes un evento que quieras promocionar?",

	'feature-it'=>"¡Nos encantaría publicarlo!",

	'share'=>"Comparte un evento",

	'other'=>"<h6>Otros</h6><h5>eventos</h5>",
	'discuss-comment'=>'Esperamos recibir tus insumos y perspectivas. Mientras formulas tus respuestas, será útil que leas los comentarios anteriores. Esto te permitirá avanzar en la conversación, construyendo sobre las publicaciones anteriores. Ayudará a que la discusión sea concisa y relevante. Incluso podrás enseñarnos algo nuevo, o aplicar un concepto de una manera nueva.',
	
];
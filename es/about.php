<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'banner-head'=>"¿Necesitas métodos prácticos <br> para alcanzar los Objetivos de <br>Desarrollo Sostenible a nivel local?",
    'banner-text'=>"El PNUD, ONU-Hábitat y el Grupo de Trabajo Global de Gobiernos Locales<br> y Regionales están recopilando herramientas y orientaciones para apoyarte.<br>¡Aporta también tus ideas!",
    
    'intro'=>"<span class='esIntro'>PRESENTAMOS: LA CAJA DE HERRAMIENTAS</span>",
    'purpose'=>"<span class='esIntro'>PROPÓSITO</span>",
    'local'=>"<span class='esIntro'>¿QUÉ ES LA ‘LOCALIZACIÓN’?</span>",
    'intro-text'=>"<p class='animated fadeInDown'>Flexible. Simple. Y al mismo tiempo, concreta.</p>
            <p class='animated fadeInDown'>Esta Caja de Herramientas no contiene orientaciones rígidas ni tampoco soluciones únicas que sean aplicables a todos los casos. Posee, sin embargo, mecanismos e instrumentos prácticos y adaptables que abordan diversos desafíos de desarrollo, que empoderan a los actores locales y ayudan a declinar objetivos globales en acciones locales.</p>
            <p class='animated fadeInDown'>La búsqueda de las herramientas y estrategias más apropiadas para “localizar” los ODS es clave para el diseño, implementación,  revisión y éxito de la Agenda 2030 para el Desarrollo Sostenible. Las herramientas que encuentras aquí pueden ser empleadas en escenarios heterogéneos y complejos, en los que las características políticas, institucionales, económicas y sociales pueden diferir –no únicamente entre países, sino también entre territorios del mismo país e incluso dentro de distintas partes de un mismo territorio. Estas herramientas han sido desarrolladas considerando una amplia variedad de experiencias regionales, y a través de las aportaciones realizadas por diferentes actores que contribuyen a su mejora.</p>
            <p class='animated fadeInDown'>Nuestra intención es ser sencillos, proporcionando orientaciones que sean aplicables a entornos cambiantes. También buscamos mantenernos flexibles, pero al mismo tiempo concretos. Trabajamos para dar una orientación práctica a la planificación, implementación, monitoreo y evaluación y de políticas locales, de acuerdo con las estrategias de consecución de los Objetivos de Desarrollo Sostenible.</p>",

    'purpose-text'=>"<p class='animated fadeInDown'>El objetivo principal de esta Caja de Herramientas es facilitar un conjunto articulado de herramientas para apoyar a los actores locales y a sus redes, bajo el liderazgo de los gobiernos locales, regionales y nacionales. </p>

			<p class='animated fadeInDown'>Esta Caja de Herramientas busca <strong>sensibilizar</strong> a los actores locales y nacionales sobre los ODS. Tiene por objetivo mejorar su conocimiento sobre la Agenda 2030, familiarizarlos con las implicaciones, oportunidades y desafíos de la localización, y contribuir a que comprendan su rol fundamental en la misma. Como herramienta de <strong>promoción</strong>, busca también crear un ambiente idóneo para el proceso de localización, apoyar la apropiación local y asegurar la integración de los ODS en las estrategias y planes subnacionales. </p>

			<p class='animated fadeInDown'>La Caja de Herramientas<strong> tiene en cuenta </strong>y revisa las herramientas ya existentes, sistematiza resultados y construye sobre las conclusiones de los / las responsables de políticas, oficiales locales, expertos y actores de relevancia local. Busca convertirse en un <strong>apoyo práctico</strong> para los actores, en particular para los gobiernos locales y regionales, a través de la puesta a disposición de aquellas prácticas más fiables y replicables para una eficiente implementación, diseño y monitoreo, en línea con los ODS. </p>
				
			<p class='animated fadeInDown'>El proceso empodera a los actores dentro de la nueva arquitectura de desarrollo, incluyendo a los gobiernos locales, regionales y nacionales, organizaciones de la sociedad civil, sector privado, universidades e institutos de investigación. Nuestro propósito es mejorar la conexión entre distintos actores a través de los dinámicos panoramas sociales, políticos y económicos de los territorios. No importa en qué etapa esté tu iniciativa, o cuál sea el ODS que quieras lograr; aquí encontrarás herramientas útiles para ayudarte a alcanzarlo.</p>",

	'local-text'=>"<p class='animated fadeInDown'>La localización del desarrollo implica <strong>tomar en cuenta contextos subnacionales</strong> para la consecución de la Agenda 2030, desde el establecimiento de objetivos y metas, hasta la determinación de los <strong>medios de implementación</strong> y el <strong>uso de indicadores</strong> para medir y monitorear el progreso. Se trata además de poner en el centro del desarrollo sostenible a los territorios y a las prioridades, necesidades y recursos de sus habitantes. Esto, en el marco de una articulación entre los niveles global, nacional y local.</p>

			<p class='animated fadeInDown'>En el pasado, la localización se entendía principalmente como la implementación de objetivos a nivel local por parte de actores subnacionales, en particular gobiernos locales y regionales. Pero este concepto ha evolucionado. Todos los ODS tienen metas directamente relacionadas con las responsabilidades de los GLR. Es por esto que el logro de los ODS depende, más que nunca, de la <strong>habilidad de los gobiernos locales y regionales para promover un desarrollo territorial integrado, inclusivo y sostenible.</strong></p>

			<p class='animated fadeInDown'><strong>Los gobiernos subnacionales son responsables de creación de políticas</strong>, catalizadores de cambio y el nivel gubernamental mejor posicionado para vincular objetivos globales con comunidades locales. La localización del desarrollo es por tanto un proceso para empoderar a los actores locales, cuyo objetivo es crear un desarrollo sostenible más consciente, y en consecuencia, más relevante para las necesidades y aspiraciones locales. Los objetivos de desarrollo pueden lograrse sólo si los actores locales participan plenamente,no sólo en la implementación, sino también en la definición y monitoreo de la agenda. </p>

			<p class='animated fadeInDown'>Además, la participación requiere que las políticas públicas no sean impuestas desde arriba, sino que toda la cadena de políticas sea compartida. Todos los actores relevantes deben estar involucrados en el proceso de toma de decisiones, a través de <strong>mecanismos de consulta y participación</strong> a nivel local y nacional.</p>",

	'go-toolbox'=>"IR A LA CAJA DE HERRAMIENTAS",
	'when'=>"<h6>¿CUÁNDO </h6><h5>COMENZÓ TODO?</h5>",    
	'work'=>"<h6>TRABAJANDO </h6><h5>CON LAS HERRAMIENTAS</h5>",
	'thinks'=>"Aquí está lo que nuestra comunidad piensa",
    
    
];

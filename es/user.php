<?php

return[

	'messages'=>"Mensajes",

	'favourites'=>"Favoritos",

	'edit'=>"Editar Perfil",

	'change'=>"Cambiar contraseña",

	'log'=>"Cierre de sesión",

	'organ'=>"Organización",

	'location'=>"Localización",

	'total'=>"Número de publicaciones",

	'about'=>"Sobre mí",

	'documents'=>"Documentos",

	'stories'=>"Historias",

	'discussions'=>"Debates",

	'events'=>"Eventos",

	'send'=>"Enviar un mensaje",

	'profile-photo'=>"Foto de portada",

	'choose-file'=>"Elige un archivo ",

	'image-shuld'=>"Las imágenes deben estar en formato PDF y no superar los 2 MB",

	'coming'=>'Próximamente',





];

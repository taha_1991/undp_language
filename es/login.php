<?php

return[

	'practical-head'=>"<h2>¿Necesitas métodos prácticos para lograr desarrollo sostenible a nivel local?</h2>",

	'practical-text'=>"La localización de ODS posee recursos y elementos para apoyarte.",

	'log-in-head'=>"<h3>Inicia sesión en tu cuenta</h3>",
	'email'=>"Dirección de correo electrónico",
	'pswd'=>"Contraseña",
	'remember'=>"Recordarme",
	'forgot'=>"Olvidé mi contraseña",
	'login-btn'=>"Iniciar sesión",
	'account'=>"Crear tu cuenta",


];
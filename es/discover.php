<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'banner'=>"<p>Identifica áreas relevantes para tu territorio, comunidad, agenda política o proyecto y haz clic para encontrar herramientas flexibles que puedes ajustar en función de tus necesidades. Las herramientas pueden ser ajustadas a distintos escenarios y desafíos de desarrollo.</p>
    <p>Al utilizar estos recursos, tendrás amplio acceso a contenido aplicable y generado a nivel local. Todo está disponible para que lo descubras.</p>",
    'discover-head'=>"<h6>DESCUBRE</h6> <h5>HERRAMIENTAS</h5>",
    'initial'=>"Comenzando el proceso de ODS",
    'raising-awareness'=>"Sensibilización",
    'diagnostics'=>"Diagnóstico",
    'strategies-and-plans'=>"Estrategias y Planes",
    'monitoring-and-evaluation'=>"Monitoreo y Evaluación",
    'enable'=>"Facilitando acuerdos institucionales para la implementación de los ODS",
    'multilevel-governance'=>"Gobernanza Multinivel",
    'territorial'=>"Enfoque Territorial Multi-Actor",
    'accountability'=>"Rendición de Cuentas",
    'development-cooperation-effectiveness'=>"Eficacia de la Cooperación al Desarrollo",
    'capacity-strengthening'=>"Fortalecimiento de Capacidades",

    'diagnostics-text'=>"<p><strong>Propósito</strong> <br>Establecer un diagnóstico es una manera de definir una línea de base cualitativa y cuantitativa sobre la situación de un territorio o sobre una temática concreta. Destaca los desafíos y oportunidades y constituye una base para el proceso de implementación.</p>
    <p><strong>¿A qué contribuyen las herramientas?</strong> <br>Las metodologías disponibles permitirán identificar brechas y recursos, y así como hacer un mapeo de los actores y relaciones antes de involucrarse en cualquier tipo de reforma.</p>",    
    
    'explore'=>"Explora la Biblioteca",

    'raising-awareness-text'=>'<p><strong>Propósito</strong> <br>La abogacía y la sensibilización son el primer paso para empezar a localizar los ODS. La idea es comunicar a los funcionarios locales la importancia de la nueva agenda y concienciarlos sobre el papel crítico que tienen sus decisiones para lograr los ODS.</p>
    <p><strong>¿A qué contribuyen las herramientas?</strong> <br>A hacer lobby para lograr una mayor atención hacia los actores y gobiernos locales, a nivel local, regional, nacional e internacional.</p>',

    'strategies-and-plans-text'=>"<p><strong>Propósito</strong> <br>Las estrategias y planes pueden asegurar la traducción de la agenda política en objetivos de desarrollo y resultados tangibles. Dan un marco general al desarrollo (utilización de recursos, servicios, necesidades financieras, etc.) y buscan coordinar el trabajo local y de otras esferas del gobierno.</p>
    <p><strong>¿A qué contribuyen las herramientas?</strong> <br>La localización de los ODS puede brindar un marco para la política de desarrollo local. La integración de los ODS en la planificación a nivel subnacional es un paso crucial en el aterrizaje de la nueva agenda en regiones y ciudades.</p>",

    'monitoring-and-evaluation-text'=>"<p><strong>Propósito</strong> <br>El monitoreo de los ODS se realizará a través de un sistema de 231 indicadores, muchos de los cuales pueden localizarse a través de la recolección de datos a nivel territorial. Al monitorear el progreso y revisar los resultados de los planes nacionales, los datos subnacionales han de tenerse en cuenta.</p>
    <p><strong>¿A qué contribuyen las herramientas?</strong> <br>A la localización del seguimiento de la Agenda 2030, al promover la participación de los gobiernos locales y regionales en el monitoreo nacional y al adaptar los indicadores naciones a los contextos regionales y locales.</p>",

    'multilevel-governance-text'=>"<p><strong>Propósito</strong> <br>La Agenda 2030 para el Desarrollo busca ir más allá de “la gobernanza habitual”, para poder alcanzar los ODS.</p><br><br>
    <p>La implementación de los ODS requiere una gobernanza multinivel. La idea es mirar hacia las estructuras de gobierno y los mecanismos de cooperación interinstitucionales para habilitar marcos para el logro de los ODS.</p>
    <p><strong>¿A qué contribuyen las herramientas?</strong><br> Las herramientas ofrecidas aquí buscan la coherencia vertical de las políticas y mecanismos de colaboración entre los niveles de gobernanza local, nacional e internacional. Esta visión compartida debería reforzar el diseño de políticas, la planificación y la implementación. Las herramientas también promueven la inclusión de los grupos sociales marginados y promueven arreglos institucionales específicos para áreas complejas. Se desglosarán en distintas áreas de interés y relevancia para la localización de ODS (por ejemplo: políticas de desarrollo económico local, procesos de políticas de planificación pública).</p>",

    'territorial-text'=>"<p><strong>Propósito</strong> <br>El enfoque territorial es un marco de políticas que puede facilitar la implementación efectiva de los ODS. Estas herramientas promueven una  amplia participación, al generar diálogos entre centro y periferia, entre miembros de la sociedad civil y sector privado. El enfoque también permite mejorar la relación de las instituciones locales e interactuación entre ellas, especialmente a través de la incorporación de conocimiento específico y saber-hacer práctico dentro de la elaboración de políticas. Esto permite una respuesta inclusiva a los desafíos del desarrollo en un territorio determinado.</p>
    <p><strong>¿A qué contribuyen las herramientas?</strong> <br>Las herramientas permiten articular varios actores en un territorio determinado. Promueven partenariados multiactor, incluyendo al gobierno, la sociedad civil y el sector privado, destacando la necesidad de interacción y coordinación en la sociedad.</p>",

    'accountability-text'=>"<p><strong>Propósito</strong> <br>Dentro de los respectivos territorios, los sistemas de “rendición de cuentas para resultados” permiten a los actores monitorear el progreso sobre los objetivos pactados, examinar los obstáculos de la implementación y sugerir cambios y acciones correctivas para esas políticas.</p>
    <p><strong>¿A qué contribuyen las herramientas?</strong> <br>Al observar la gestión de los datos locales (incluyendo los indicadores), el monitoreo local y los mecanismos locales específicos permitirán aumentar la transparencia, el acceso a la información, la sensibilización y apropiación de la Agenda 2030. Esto lleva la rendición de cuentas más de cerca de las personas, y refuerza su voz.</p>",

    'development-cooperation-effectiveness-text'=>"<p><strong>Propósito</strong> <br>La implementación de los principios de la eficacia de la cooperación al desarrollo a nivel local implica facilitar la alineación y armonización de los actores del desarrollo. Esto refuerza los resultados del desarrollo donde más importa - y donde impacta la vida de las personas. Las prácticas de cooperación desarrollo a nivel local para implementar los ODS puede retroalimentar la agenda global; pueden permitir la aplicación de lecciones aprendidas y proporcionar evidencias para políticas más eficaces.</p>
    <p><strong>¿A qué contribuyen las herramientas?</strong> <br>La utilización de marcos de resultados de cooperación a nivel local previene la proliferación de iniciativas de desarrollo aisladas y aumenta la alineación, armonización, responsabilidad y transparencia.</p>",

    'capacity-strengthening-text'=>"<p><strong>Propósito</strong> <br>Los gobiernos locales y regionales, junto con otros actores locales, tienen un papel crucial en la implementación y monitoreo de los ODS. Esta sección pretende brindar herramientas que apoyen el desarrollo y fortalecimiento de sus capacidades para el aterrizaje de la Agenda 2030.</p>
    <p><strong>¿A qué contribuyen las herramientas</strong> <br>Ya que se considera que el desarrollo de capacidades es el motor de los procesos de desarrollo, estas herramientas buscan guiar a individuos, organizaciones y sociedades para obtener, reforzar y mantener las capacidades para definir y lograr sus propios objetivos de desarrollo en coherencia con los ODS.</p>",
    'small'=>'Esta iniciativa está evolucionando, y actores como tú están compartiendo recursos. Aquí encontrarás los instrumentos recogidos hasta el momento.',

];
<?php

return[

	'forgot-head'=>"<h6>¿Olvidaste </h6><h5>la contraseña?</h5>",

	'forgot-text'=>"Introduce tu dirección de correo electrónico, te ayudaremos a restablecer tu contraseña.",

	'email'=>"Dirección de correo electrónico",

	'enter'=>"Introduce el código de verificación",

	'reset'=>"Cambiar mi Contraseña",

	'please'=>"<strong>Por favor, comprueba tu bandeja de entrada,</strong> te hemos enviado un mensaje para restablecer la contraseña.",

	'click'=>"<p>Haz clic en el enlace proporcionado en el correo electrónico e introduce una nueva contraseña.</p>
	<p>Si no has recibido un correo electrónico, por favor revisa tu carpeta de correo no deseado. Si todavía tiene problemas para encontrarlo, haz clic aquí:</p>",

	'resend'=>"Reenviar email",

	'not-reg'=>"Lo lamentamos, este correo electrónico no está registrado.",

	

];
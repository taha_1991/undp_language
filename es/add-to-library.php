<?php

return[

	'add-head'=>"<h6>AÑADIR</h6><h5>A LA BIBLIOTECA</h5>",

	'purpose'=>"El propósito de esta biblioteca",

	'add-text'=>"<p>¿Tiene un documento práctico que podrían ayudar a los actores locales a alcanzar los objetivos relacionados con los objetivos de desarrollo sostenible? ¡Contribuye a esta biblioteca!</p>
	<p>Para asegurarse de que la biblioteca se alimenta decontenido relevante, todas las contribuciones serán aprobadas antes de ser publicadas. Tenga en cuenta que podemos hacer / sugerir cambios menores durante la moderación.</p>",

	'title'=>"Título del documento",
	'author'=>"Autor",

	'developed'=>"Desarrollado por: Nombre de la organización (es)",

	'type'=>"Tipo de Documento",

	'Seledoc'=> "Seleccionar tipo de documento",

	'language'=>"Idioma",

	'Selelang'=>"Selección de idioma",

	'upload'=>"Tu contribución",

	'choose'=>"Elige un archivo",

	'image'=>"Las imágenes deben estar en formato PDF y no superar los 5 MB.",

	'description'=>"Por favor, introduce una breve descripción",

	'region'=>"Región relacionada con el contenido",

	'Selereg'=> "Seleccionar Región",

	'country'=>"País relacionado con el contenido",

	'Selecon'=> "Seleccionar país",

	'add-more'=>"Añadir más",

	'year'=>"Documento publicado en",

	'Seleyear'=>"Seleccione el año",

	'another-language'=>"Añadir este documento en otro idioma",

	'terms'=>"Estoy de acuerdo con los <a href=':terms'>Términos y Condiciones</a> y con la <a href=':privacy'>Política de Privacidad</a>",

	'thank'=>"GRACIAS POR COMPARTIR ESTE TRABAJO",

	'contribution'=>"<p>Tu contribución aparecerá publicada en cuanto el moderador la apruebe.</p><br/>
	<p>Recibirás una notificación.<p>",

	'until'=>"Hasta entonces, revisa la <a href=':link'><strong>Biblioteca</strong></a>",


	'purpose-text'=>"<p>A pesar de toda la información disponible en Internet, todavía hay muchas cosas interesantes que no están en línea. De especial interés para esta comunidad es el conocimiento aún no explotado de los procesos de desarrollo y las soluciones encontradas almacenadas en las bibliotecas de muchas oficinas. Y mientras tanto, a veces se “reinventa la rueda” y se utilizan procesos que en realidad ya han sido completados por otros.</p>
	<p>Existen estupendas colecciones y herramientas en internet. Este nuevo y ambicioso proyecto, Localizar los ODS, busca centralizar todos esos ricos conocimientos y <strong>construir un depósito relacionado con el logro de los ODS a nivel local.</strong> El objetivo es ser inclusivo y participativo y descubrir cuáles son los agentes de cambio más efectivos para las comunidades locales.</p>
	<p>Por ello, estás invitado a compartir tus conocimientos. Tu trabajo puede empoderar a otros actores. También podrá dar a otros miembros la posibilidad de guiarte en la dirección correcta, dar comentarios a tus ideas y ayudarte a saber si alguien más está trabajando en temas similares.</p>",
	

];
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'banner-head'=>"CAJA DE HERRAMIENTAS PARA LOCALIZAR LOS<br><span id='goals'>OBJETIVOS DE DESARROLLO SOSTENIBLE</span>",
    'banner-main-text'=>"Un universo de información práctica y personalizada te espera ",
    'banner-text'=>"Descubre herramientas y recursos, así como experiencias y opiniones de diversos actores del desarrollo",
    'banner-sign-up'=>"Inscríbete aquí para unirte a nuestra comunidad",
    'banner-video'=>"Video",
    'why'=>"<h6>¿POR QUÉ DEBERÍAS</h6> <h5>ESTAR AQUÍ?</h5>",
    'discover'=>"Descubre herramientas",
    'discover-text'=>"Alrededor del mundo, gobiernos y organizaciones están trabajando desde sus contextos locales específicos para impulsar un progreso sostenible. Hemos reunido aquí una gran cantidad de elementos de aprendizaje en forma de herramientas y orientaciones prácticas y flexibles para tu uso.",
    'contribute'=>"Aporta documentos",
    'contribute-text'=>"¿Has desarrollado ideas que podrían ser útiles para otros actores locales? ¡Añádelas a nuestra biblioteca y hazlas visibles! Buscamos construir una biblioteca digital de soluciones y prácticas de desarrollo.",
    'story'=>"Comparte historias",
    'story-text'=>"Las historias tienen el poder de conectarnos con las trayectorias de otros actores; también son una valiosa oportunidad para interactuar. Comparte tu historia y construye una relación más profunda con nuestra comunidad.",
    'discuss'=>"Debate",
    'discuss-text'=>"Inicia conversaciones en torno a temas e ideas pertinentes sobre localización y encuentra puntos de vista diferentes. ¡Puedes proponer un tema, y tu organización podría ser la elegida para moderar nuestro próximo debate!",
    'event'=>"Publica eventos",
    'event-text'=>"Promueve tus eventos en nuestro calendario. Damos relevancia a eventos que busquen localizar los ODS a nivel mundial.",
    
    'latest-head'=>"<h6>ÚLTIMAS </h6><h5>ACTUALIZACIONES</h5>",
    'twitter'=>"<h6>ÚNETE A LA </h6><h5>CONVERSACIÓN</h5>",
    
    'local'=>"<h6>NUESTROS LOCALES</h6><h5>COLABORADORES</h5>",
    'local-text'=>"<span class='spanishTextMap'>Mira cómo nuestros colaboradores se extienden globalmente.</span>",
    

];

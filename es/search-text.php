<?php

return[

'type-1'=>"Notas conceptuales y documentos",

'type-2'=>"Estudios de casos y buenas prácticas",

'type-3'=>"Guías y sistematización de experiencias",

'type-4'=>"Herramientas",

'actor-1'=>"Generales",

'actor-2'=>"Gobiernos Locales y Regionales",

'actor-3'=>"Gobiernos Nacionales",

'actor-4'=>"Organizaciones de la Sociedad Civil",

'actor-5'=>"Sector Privado",

'actor-6'=>"Actores de la Economía Social",

'actor-7'=>"Filantropía y Fundaciones",

'actor-8'=>"Instituciones Académicas y de Investigación",


'region-1'=>"Mundial",

'region-2'=>"Asia",

'region-3'=>"África",

'region-4'=>"América",

'region-5'=>"Europa",

'region-6'=>"Oceanía",


'language-1'=>"Inglés",

'language-2'=>"Francés",

'language-3'=>"Español",

'language-other'=>"Otros",


'tools-1'=>"Sensibilización",

'tools-2'=>"Diagnóstico",

'tools-3'=>"Estrategias y Planes",

'tools-4'=>"Monitoreo y Evaluación",

'tools-5'=>"Gobernanza Multinivel",

'tools-6'=>"Enfoque Territorial Multi-Actor",

'tools-7'=>"Rendición de Cuentas",

'tools-8'=>"Eficacia de la Cooperación al Desarrollo",

'tools-9'=>"Fortalecimiento de Capacidades",





];
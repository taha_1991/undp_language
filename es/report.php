<?php

return[
	
	'report-head'=>"<h6>Informar</h6> <h5>sobre un problema</h5>",

	'inaccurate'=>"La información es incorrecta.",

	'legitimate'=>"La información no viene de una fuente legítima.",

	'authority'=>"El usuario no tiene la autoridad para compartir la información.",

	'Comments'=>"Comentarios / Sugerencias",

	'Policy'=>"Estoy de acuerdo con los <a href=':link'>Términos y Condiciones</a> y la <a href=':link'>Políticas de Privacidad</a>",
	
	'thank'=>"Gracias por tomarte el tiempo de informar sobre el problema.",



];



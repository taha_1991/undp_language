<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'core'=>"<h6>SOCIOS</h6> <h5>PRINCIPALES</h5>",
    'gtf'=>"El Grupo de Trabajo Global de Gobiernos Locales y Regionales ",
    'gtf-small'=>"GTF",
    'gtf-text'=>"<strong>El Grupo de Trabajo Global de Gobiernos Locales y Regionales</strong> es un mecanismo de coordinación que vincula a las grandes redes internacionales de gobiernos locales para una abogacía conjunta relativa a los procesos de políticas internacionales, particularmente la agenda de cambio climático, los Objetivos de Desarrollo Sostenible y Hábitat III.",
    'habitat'=>"Programa de Naciones Unidas para los Asentamientos Humanos",
    'habitat-small'=>"ONU-Hábitat",
    'habitat-text'=>"<strong>ONU-Hábitat</strong> es la agencia líder para los asentamientos humanos y fue encomendada por la Asamblea General de las Naciones Unidas para promover ciudades y pueblos social y ambientalmente sostenibles, para proporcionar vivienda adecuada para todas las personas. ONU-Hábitat también tiene el mandato específico dentro del Sistema de las Naciones Unidas de actuar como punto central para los gobiernos locales y sus asociaciones, respaldando a los gobiernos locales y territoriales como actores esenciales de desarrollo.",
    'undp'=>"Programa de las Naciones Unidas para el Desarrollo",
    'undp-small'=>"PNUD",
    'undp-text'=>"<strong>PNUD</strong> trabaja en casi 170 países y territorios, ayudando a la erradicación de la pobreza y a la reducción de las desigualdades y la exclusión. Ayudamos a los países a desarrollar políticas, competencias de liderazgo y partenariados, capacidades institucionales y creación de resiliencia para unos resultados de desarrollo sostenibles. PNUD también enfoca su trabajo en el fortalecimiento de procesos de inclusión y rendición de cuentas de los gobiernos subnacionales. Se asegura de que tengan la capacidad de gestionar las oportunidades y responsabilidades derivadas de la descentralización y devolución de poderes. También busca optimizar el potencial y rol de esos actores como socios del desarrollo.",
    'contributing'=>"<h6>SOCIOS</h6><h5>PARTICIPANTES</h5>",
    'contributing-text'=>"La participación de todos estos actores es el fundamento de esta iniciativa. Estaríamos encantados de dar voz a otros participantes, según recibamos más contribuciones.",
    
    'lrg'=>"Gobiernos Locales y Regionales",
    'ng'=>"Gobiernos<br> Nacionales",
    'io'=>"Organizaciones <br>Internacionales",
    'cso'=>"Organizaciones de <br>la Sociedad Civil",
    'ps'=>"Sector <br>Privado",
    'ari'=>"Instituciones Académicas y de Investigación",
    
    'lrg-head'=>"<h6>Gobiernos</h6> <br><h5>Locales y Regionales</h5>",
    'ng-head'=>"<h6>Gobiernos</h5> <br><h5>Nacionales</h5>",
    'io-head'=>"<h6>Organizaciones</h6> <br><h5>Internacionales </h5>",
    'cso-head'=>"<h6>Organizaciones de la</h6> <br><h5>Sociedad Civil</h5>",
    'ps-head'=>"<h6>Sector</h6> <br><h5>Privado</h5>",
    'ari-head'=>"<h6>Instituciones</h6> <br><h5>Académicas y de Investigación</h5>",
						
	'go-to-website'=>"Ir al sitio web",
	'why'=>"¿Por qué son importantes para los ODS?",
	'role'=>"Rol en la localización de los ODS",
	
	'lrg-pop-head'=>"GOBIERNOS LOCALES Y REGIONALES (y sus asociaciones)",
	'ng-pop-head'=>"GOBIERNOS NACIONALES",
	'cso-pop-head'=>"ORGANIZACIONES DE LA SOCIEDAD CIVIL",
	'io-pop-head'=>"ORGANIZACIONES INTERNACIONALES",
	'ps-pop-head'=>"SECTOR PRIVADO",
	'ari-pop-head'=>"INSTITUCIONES ACADÉMICAS Y DE INVESTIGACIÓN",
	
	'lrg-role'=>"<ul><li>Siendo la esfera de gobierno más próxima a la gente y la primera puerta de entrada para su participación en los asuntos públicos, los GLR se hallan en una posición única para identificar y responder a las necesidades y brechas de desarrollo, respaldando objetivos locales y también nacionales.</li>
				<li>Su liderazgo político es esencial para impulsar la coordinación y la coherencia entre distintos niveles de gobierno.</li>
				<li>Su poder de convocatoria juega un rol facilitador para lograr consenso entre diferentes actores institucionales y grupos de interés (sector privado, sector público, academia, etc.). Pueden tomar el liderazgo en promover una visión  conjunta y una planificación estratégica de su territorio.</li>
				<li>Los  GLR están también mejor posicionados para obtener resultados efectivos de desarrollo a nivel local. Refuerzan la apropiación de los proyectos sobre el terreno; promueven alianzas inclusivas entre autoridades públicas, sector privado y OSC; se enfocan en resultados tangibles e incrementan la rendición de cuentas y la transparencia.</li>
				<li>El rol activo de los GLR en la cooperación internacional para el desarrollo es crucial para lograr resultados de desarrollo, democratizar procesos y promover la apropiación inclusiva.</li>
				<li>Las asociaciones de GLR reflejan la diversidad de los gobiernos locales (en términos de su tamaño, geografía, capacidades, etc.). Hacen abogacía, forman a sus miembros y representan una voz unificada al dirigirse a otros actores.</li>
			</ul>
			<p>Esta Caja de Herramientas busca identificar las herramientas, mecanismos y prácticas existentes más efectivas para fortalecer las capacidades de los GLR, con el fin de promover de forma más efectiva los ODS dentro de sus territorios. Busca apoyar a los GLR en convertirse en “campeones” y facilitadores de procesos de desarrollo a nivel local, integrados, sostenibles e inclusivos.</p>",

	'ng-role'=>"<ul><li>Los gobiernos nacionales lideran las políticas de desarrollo vinculadas con la agenda de los ODS. Tienen <strong>un rol de desarrollo y coordinación</strong> en cuanto al proceso de implementación de la Agenda 2030. Son responsables de la definición y desarrollo de estrategias nacionales coherentes con los ODS.</li>
				<li>El compromiso de los gobiernos nacionales es crucial para facilitar una efectiva localización de los ODS. Son ellos quienes, en última instancia, establecen un <strong>ambiente favorable</strong> y un marco descentralizado que permite a los gobiernos y actores locales y regionales prestar servicios.</li>
				<li>Tienen la capacidad de establecer diálogo y relaciones colaborativas con el abanico de actores en el desarrollo local. Pueden diseñar instrumentos apropiados para hacer que los actores, particularmente los GLR, sean capaces de gestionar estrategias de desarrollo local.</li>
				<li>Los gobiernos nacionales son responsables ante sus ciudadanos de la implementación de los ODS, así como ante la comunidad internacional, a través de los diálogos anuales en los Foros Políticos de Alto Nivel. <strong>Allí pueden mostrar éxitos concretos</strong> promovidos desde el terreno.</li>
				<li>Son responsables del <strong>diseño de un marco nacional de monitoreo y de la presentación de informes</strong>, con sus respectivos indicadores nacionales. Estos deberían incluir también indicadores desagregados que deben ser obtenidos a nivel local. Los gobiernos nacionales obtienen retroalimentación a nivel local, para informar después acerca de los progresos de implementación y actualizar los informes anuales de desarrollo sostenible.</li>
			</ul>
			<p>Este Caja de Herramientas propone instrumentos, metodologías y prácticas existentes que pueden fortalecer las capacidades de los gobiernos nacionales en la inclusión efectiva de los GLR en la definición, implementación y monitoreo de las estrategias de los ODS.</p>",
			
	'io-role'=>"<ul><li>Las Organizaciones Internacionales pueden apoyar la comunicación de la nueva agenda, mejorar las alianzas para su implementación y llenar los vacíos existentes en cuanto a los datos disponibles para el monitoreo y la evaluación.</li>
				<li>En este marco, el Grupo de Desarrollo de las Naciones Unidas ha desarrollado una estrategia para apoyar una implementación efectiva y coherente de la nueva agenda, a través del acrónimo “MAPS” (por sus siglas en inglés: Integración, Aceleración y Apoyo a las Políticas).</li>
				<li>A través del enfoque MAPS, las Naciones Unidas apoyan a los países de tres maneras diferentes. Primero, ayudando a los gobiernos a reflejar la nueva agenda global en los planes y políticas nacionales de desarrollo. Esta labor ya está en marcha en muchos países a solicitud de los propios gobiernos. En segundo lugar, apoyando a los países a acelerar el progreso respecto a las metas de los ODS. Por último, poniendo a disposición la experiencia de las Naciones Unidas en cuanto a políticas de desarrollo sostenible y gobernanza, a lo largo de todas las fases de implementación.</li>
				<li>Equipados con este paquete de servicios de apoyo a las políticas, las Naciones Unidas y sus agencias y programas están listos para apoyar a los países socios en la implementación de la nueva agenda de desarrollo  de forma efectiva y en la promoción de prosperidad económica, humana y ambiental a largo plazo.</li>
				
			</ul>
			<p>La Caja de Herramientas propone instrumentos, metodologías y las prácticas existentes idóneas para aterrizar los ODS a nivel local, con el apoyo de las Organizaciones Internacionales.</p>",

	'cso-role'=>"<ul><li>Las OSC son esenciales en la implementación de los ODS porque representan <strong>participación comunitaria</strong> e involucración de voluntarios. Pueden ayudar a <strong>incrementar la sensibilidad de los ODS</strong> a nivel comunitario. Tienen también un rol promotor de las campañas nacionales e internacionales, suministrando ejemplos de cómo aterrizar la Agenda 2030 y hacerla relevante.</li>
				<li>Las OSC juegan un importante <strong>rol como mediadores</strong> en el desarrollo de políticas: Suministran el vínculo entre la sociedad y los tomadores de decisiones. Ayudan a la identificación de prioridades de desarrollo, sugieren soluciones prácticas y detectan oportunidades de políticas para localizar los ODS.</li>
				<li>Las OSC participativas pueden impulsar la posibilidad de <strong>integrar voces ciudadanas en los procesos de toma de decisiones</strong>, especialmente en lo que respecta a los GLR. Las OSC pueden ofrecer espacios neutrales para el diálogo entre los múltiples actores y pueden crear conexiones con los procesos de toma de decisiones y los medios de implementación. Los mecanismos de coordinación pueden ayudar a articular adecuadamente la cooperación territorial y asegurar la efectiva localización de los ODS.</li>
				<li>Las OSC pueden proporcionar herramientas para que los ciudadanos <strong>monitoreen los progresos</strong> y hagan responsables a sus gobiernos de la implementación de los ODS, de un modo transparente y participativo.</li>
				<li>También pueden <strong>formar a actores relevantes</strong> y a los tomadores de decisiones en las prácticas de implementación.</li>
				<li>Las OSC pueden asegurar la  <strong>recogida transparente de información </strong> y la recopilación de datos globales utilizados en relación a asuntos relevantes para los ODS, tanto con los gobiernos como junto a éstos.</li>
			</ul>
			<p>La Caja de Herramientas se enfoca en el rol de las OSC para la localización de las políticas de desarrollo, con el fin de proponer herramientas prácticas para hacer de su experiencia algo valioso, con el fin de una implementación de los ODS más efectiva.</p>",

	'ps-role'=>"<ul>
				<li>Cuando el sector privado  es flexible y basado en aspectos sociales, puede ser un <strong>medio estratégico y valioso para llegar a la población local</strong> y satisfacer sus necesidades.</li>
				<li>El sector privado puede apoyar aspectos específicos de gobernanza y ser partícipe de la implementación de los ODS. Las estructuras regulatorias y los sistemas de incentivos pueden impulsar la cooperación, facilitando <strong>alianzas entre público- privadas y mecanismos de financiamiento</strong>.</li>
				<li>Las empresas deben ser incluidas en la planificación de las agendas nacionales y locales para implementar los ODS. Cuando los gobiernos incluyen al sector privado en la planificación y no sólo en la implementación, este <strong>se apropia también </strong> de los desafíos y de las potenciales soluciones.</li>
				<li>Las alianzas inclusivas entre gobiernos, empresas y organizaciones de la sociedad civil son fundamentales para promover el diálogo, diseñar agendas de políticas comunes, movilizar recursos y asegurar la rendición de cuentas. Permiten abordar desafíos más allá de iniciativas individuales y las alianzas basadas exclusivamente en proyectos. Las empresas pueden generar <strong>vínculos en áreas clave como la investigación y la innovación, la formación profesional</strong>, el desarrollo de cadenas de valor, el crecimiento de pequeñas y medianas empresas, etc.</li>
				<li><strong>La inversión del sector privado y las soluciones basadas en el mercado</strong> son esenciales para alcanzar un impacto sostenido. Un modelo de negocios y un modelo de desarrollo pueden alinearse para crear una estrategia híbrida. Por ejemplo, la dimensión filantrópica y la comercial pueden combinarse para impulsar diferentes competencias y recursos.</li>
				<li>Las empresas pueden ayudar a <strong>crear parámetros cuantificables</strong> para los ODS, con respecto a la selección de metas, monitoreo, evaluación y rendición de cuentas. Esto ayudará a medir el impacto de las operaciones empresariales, alinear indicadores e integrar los resultados en la gestión del rendimiento y mecanismos de información. Sistemas de monitoreo independientes también podrían verificar los resultados de las alianzas de desarrollo.</li>
			</ul>
			<p>Esta Caja de Herramientas busca identificar y documentar prácticas y herramientas existentes. Busca estructurar posibles canales para aumentar la sensibilización, el compromiso y la movilización de los actores privados, junto con otros actores, para la implementación de los ODS.  </p>",
			
	'ari-role'=>"<ul>
				<li>Las Universidades juegan un rol fundamental en la <strong>capacitación de recursos humanos</strong>. Pueden educar y formar a ciudadanos y gerentes en la localización de los ODS, y -de modo más general- en la importancia de las políticas de desarrollo.</li>
				<li>Llevan a cabo investigación sobre desarrollo y contribuyen con <strong>aportaciones</strong> para la elaboración de políticas. Esto incrementa el bagaje de aprendizajes sobre los ODS, creando un conocimiento que puede ser relevante para las políticas. También ayuda a definir políticas y esquemas conceptuales de gobernanza apropiados para localizar los ODS.</li>
				<li>La academia juega el rol principal de legitimar y <strong>definir marcos teóricos</strong> para las ideas prácticas, haciéndoles accesibles a las comunidades y al público.</li>
				<li>Las instituciones académicas y de investigación también juegan un rol en la <strong>difusión de conocimiento</strong> sobre los mecanismos de esta Caja de Herramientas, enriqueciendo el debate científico sobre la localización del desarrollo. Pueden compartir información, datos, materiales y experiencias sobre procesos de localización.</li>
				<li>Pueden contribuir con el diálogo sobre políticas en sus países y en la comunidad internacional, al reforzar la colaboración con funcionarios públicos y los tomadores de decisiones, y <strong>establecer alianzas</strong>.</li>
			</ul>
			<p>Esta Caja de Herramientas examina las prácticas existentes y estudios de caso respecto al rol de la academia en el desarrollo, proponiendo herramientas prácticas para reforzar la participación de la misma en la implementación de los ODS.</p>",
			'nudge'=>"<span class='nudge-text'>¿Está compartiendo herramientas, documentos, historias o eventos, o está moderando un debate?<br>
Estaríamos encantados de añadirlo a la lista de los socios para la localización de los ODS.<br></span>
<span class='nudge-main'>Por favor, póngase en <a href='mailto:info.art@undp.org'>contacto con nosotros</a> para ser publicado en la página web.</span>",
];

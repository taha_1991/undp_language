<?php

return [

        /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'su'=>'_es',

    'links'=> [
        'about'=>"Acerca de",
        'partner'=>"Socios",
        'contact'=>"Contáctanos",
        'discover'=>"<span class='headerFontes'>Descubre las herramientas</span>",
        'library'=>"<span class='headerFontes'>Biblioteca</span>",
        'discuss'=>"<span class='headerFontes'>Debate y participa</span>",
    ],
    'languages'=>[
        'english'=>"Inglés",
        'french'=>"Español",
        'spanish'=>"Francés"
    ],
    'login'=>"Inicia sesión",
    'footer'=>"UNA INICIATIVA APOYADA POR",
    'all'=>"Todos los derechos reservados",
    'terms'=>"Términos y condiciones",
    'privacy'=>"Política de privacidad ",
    'tool'=>"Herramientas",
    'document'=>"Documentos",
    'story'=>"Historia",
    'discussion'=>"Debates",
    'event'=>"Eventos",
    'vid-link'=>'<iframe width="532" height="300" src="https://www.youtube.com/embed/PRq8D_W1F84?enablejsapi=1" frameborder="0" allowfullscreen></iframe>',
    'home'=>"Inicio",
    'search-results'=>"Resultados de búsqueda",
    'search-results-for'=>"Resultados de búsqueda para:",
    'partner'=>"Socio",
    'partner-listing'=>"Listado de Socios",
    'discover-tools'=>"Descubre las herramientas",

    'general'=>"Elementos generales",
    'develop'=>"Desarrollado por:",
    'posted'=>"Publicado por:",
    'moderated'=>"Moderado por:",
    'organized'=>"Organizado por:",
    'view-all'=>"Ver todo",
    'view-more'=>"Ver más",
    'add-to-cal'=>"Añadir al calendario",
    'published'=>"Publicado en",
    'sub'=>"Enviar",
    
    'january'=>"Enero",
    'february'=>"Febrero",
    'march'=>"Marzo",
    'april'=>"Abril",
    'may'=>"Mayo",
    'june'=>"Junio",
    'july'=>"Julio",
    'august'=>"Agosto",
    'september'=>"Septiembre",
    'october'=>"Octubre",
    'november'=>"Noviembre",
    'december'=>"Diciembre",

    'jan'=>"Ene",
    'feb'=>"Feb",
    'mar'=>"Mar",
    'apr'=>"Abr",
    'may'=>"May",
    'jun'=>"Jun",
    'jul'=>"Jul",
    'aug'=>"Ago",
    'sept'=>"Sept",
    'oct'=>"Oct",
    'nov'=>"Nov",
    'dec'=>"Dic",
    'first'=>"1º",
    'second'=>"2º",
    'third'=>"3º",

    'search'=>"Buscar",

    'submit'=>"Enviar",
    'home'=>"Inicio",
    'result'=>"Resultados de búsqueda",
    'raise'=>"SENSIBILIZACIÓN",
    'library'=>"Biblioteca",
    'stories'=>"Historias",
    'discussions'=>"Debates",
<<<<<<< HEAD
    'events'=>"Eventos",
=======
    'events'=>"eventos", 
>>>>>>> origin/undp_abhijit
    'register'=>"Registrarse",
    'active'=>"Cuenta Activa",
    'forgot'=>"Olvidé la contraseña ",
    'reset'=>"Restablecer contraseña",
    'messages'=>"Mensajes",
    'add-to-library'=>"Añadir a la biblioteca",
    'report'=>"Informar Problema",
    'lead'=>"Liderar un Debate",
    'share-story'=>"Comparte tu Historia",
    'share-event'=>"Comparte un Evento",



];

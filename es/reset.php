<?php

return[

'reset-head'=>"<h6>¿Restablecer</h6> <h5>contraseña?</h5>",

'pswd'=>"Nueva contraseña",

'retype'=>"Reescriba nueva contraseña",

'set-pswd'=>"Establece lacontraseña",

'Passwords-must'=>"Las contraseñas deben:",

'Passwords-must-one'=>"Incluir al menos 8 caracteres.",

'Passwords-must-two'=>"Incluir una combinación de letras y números.",

'Passwords-must-three'=>"No incluir ningún carácter especial.",

'successfully'=>"¡Tu contraseña ha sido restablecida!",

'return'=>"<a  href='javascript:void(0);' class ='check'><strong>Haz clic aquí</strong></a> para volver a la página de inicio de sesión.",

'old-pass'=>"Contraseña anterior",


];
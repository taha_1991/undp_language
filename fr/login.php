<?php

return[

	'practical-head'=>"<h2>Avez-vous besoin de méthodes pratiques pour parvenir à un développement durable au niveau local ?</h2>",

	'practical-text'=>"Localiser les ODD propose des ressources et des fonctionnalités pour vous soutenir.",

	'log-in-head'=>"<h3>Connectez-vous à votre compte</h3>",
	'email'=>"Adresse e-mail",
	'pswd'=>"Mot de passe",
	'remember'=>"Se souvenir de moi ?",
	'forgot'=>"Mot de passe oublié",
	'login-btn'=>"Connexion",
	'account'=>"Créez votre compte",


];
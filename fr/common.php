<?php

return [
    
        /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'su'=>'_fr',
    'links'=> [
        'about'=>"À propos",
        'partner'=>"Partenaires",
        'contact'=>"Nous contacter",
        'discover'=>"Découvrez les outils",
        'library'=>"Bibliothèque",
        'discuss'=>"Discuter et Participer",
    ],
    'languages'=>[
        'english'=>"English",
        'french'=>"Español",
        'spanish'=>"Français"
    ],
    'login'=>"Connectez-vous à votre compte",
    'footer'=>"UNE INITIATIVE SOUTENUE PAR",
    'all'=>"Tous Droits Réservés",
    'terms'=>"Termes & Conditions",
    'privacy'=>"Politique de confidentialité",
    'tool'=>"Outils",
    'document'=>"Documents",
    'story'=>"Histoires",
    'discussion'=>"Débats",
    'event'=>"Evènements",
    'vid-link'=>'<iframe width="532" height="300" src="https://www.youtube.com/embed/guprs6xoLNc?enablejsapi=1" frameborder="0" allowfullscreen></iframe>',
    'home'=>"Accueil",
    'search-results'=>"Résultats de la Recherche",
    'search-results-for'=>"Résultats de la recherche:",
    'partner'=>"Partenaire",
    'partner-listing'=>"Liste des partenaires",
    'discover-tools'=>"Découvrez les outils",

    'general'=>"Éléments Généraux",
    'develop'=>"Développé par:",
    'posted'=>"Publie par:",
    'moderated'=>"Modérée par:",
    'organized'=>"Organisé par:",
    'view-all'=>"Voir tout",
    'view-more'=>"Voir plus",
    'add-to-cal'=>"Ajouter au calendrier",
    'published'=>"Publié dans",
    'sub'=>"Envoyer",
    'january'=>"Janvier",
    'february'=>"Février",
    'march'=>"Mars",
    'april'=>"Avril",
    'may'=>"Mai",
    'june'=>"Juin",
    'july'=>"Juillet",
    'august'=>"Août",
    'september'=>"Septembre",
    'october'=>"Octobre",
    'november'=>"Novembre",
    'december'=>"Décembre",
    'jan'=>"Jan",
    'feb'=>"Feb",
    'mar'=>"Mars",
    'apr'=>"Avr",
    'may'=>"Mai",
    'jun'=>"Juin",
    'jul'=>"Juill",
    'aug'=>"Août",
    'sept'=>"Sept",
    'oct'=>"Oct",
    'nov'=>"Nov",
    'dec'=>"Déc",
    'first'=>"1er",
    'second'=>"2e",
    'third'=>"3e",

    'search'=>"Rechercher",

<<<<<<< HEAD
    'submit'=>"Envoyer",
    'home'=>"Accueil",
=======
    'submit'=>"Envoyer", 
     'home'=>"Accueil",
>>>>>>> origin/undp_abhijit
    'result'=>"Résultats de la Recherche",
    'raise'=>"SENSIBILISATION",
    'library'=>"Bibliothèque",
    'stories'=>"Histoires",
    'discussions'=>"Discussions",
    'events'=>"Evènements",
    'register'=>"Inscription",
    'active'=>"Compte activé",
    'forgot'=>"Mot de passe oublié",
    'reset'=>"Réinitialiser le mot de passe",
    'messages'=>"Messages",
    'add-to-library'=>"Ajouter à la bibliothèque",
    'report'=>"Rapport de signalement",
    'lead'=>"ANIMER UNE DISCUSSION",
    'share-story'=>"Partagez votre histoire",
    'share-event'=>"Partager un événement",



];

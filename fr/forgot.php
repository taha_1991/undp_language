<?php

return[

	'forgot-head'=>"<h6>oublié ?</h6><h5>Mot de passe </h5>",

	'forgot-text'=>"Entrez votre adresse email, nous allons vous aider à le réinitialiser.",

	'email'=>"Adresse e-mail",

	'enter'=>"Saisissez le code de vérification",

	'reset'=>"Réinitialiser mon mot de passe",

	'please'=>"Veuillez consulter votre boîte de réception de courrier électronique, vous devriez y trouver un e-mail de réinitialisation de votre mot de passe. ",

	'click'=>"<p>Cliquez sur le lien fourni dans cet e-mail et saisissez un nouveau mot de passe.</p><p>Si vous n’avez pas reçu notre e-mail, veuillez vérifier votre dossier de courriers indésirables / spam. Si vous n’arrivez toujours pas à le trouver, cliquez ici :</p>",

	'resend'=>"Renvoyer l’e-mail",

	'not-reg'=>"Désolé, cette adresse e-mail n'est pas enregistrée.",



];

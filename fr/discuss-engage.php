<?php

return[

	'stories-discussions-head'=>"<h5>HISTOIRES, DISCUSSIONS ET ÉVÉNEMENTS</h5>",

	'stories-text'=>"Un terreau fertile pour partager idées, expériences et événements.",

	'local-head'=>"<h6>HISTOIRES</h6><h5>LOCALES</h5>",

	'current-head'=>"<h6>DISCUSSIONS</h6><h5>EN COURS</h5>",

	'events-head'=>"<h5>ÉVÉNEMENTS</h5>",

	'share-btn'=>"Partagez votre histoire",

	'lead-btn'=>"Proposez un sujet ou lancez une discussion",

	'post-btn'=>"Publiez un événement",

	'stories-head'=>"<h6>Dernières</h6><h5>histoires</h5>",

	'your-stories'=>"Vos histoires sont au cœur de tout. Qu’elle soit éprouvante ou triomphante, source de réflexion ou d’inspiration, nous aimerions entendre votre histoire et pourquoi pas la publier.",

	'share-your-story-btn'=>"Partagez votre histoire",

	'discussion-head'=>"<h5>Discussions</h5>",

	'evnt-head'=>"<h5>Événements</h5>",

	'have-any'=>"Vous avez une histoire que vous souhaiteriez partager ?",

	'we-love'=>"Nous serions ravis de la recueillir !",
	
	'share-button'=>"Partagez votre histoire",

	'facebook-btn'=>"Partager sur Facebook",

	'twitter-btn'=>"Partager sur Twitter",

	'email-btn'=>"Partager par e-mail",

	'contact-btn'=>"Contacter le contributeur",

	'add-to'=>"Ajouter aux favoris",
	
	'report-btn'=>"Signaler un problème",

	'other-head'=>"<h6>Autres</h6><h5>histoires</h5>",

	'comments'=>"Commentaires",

	'write-comm'=>"Rédigez votre commentaire",

	'post'=>"Publier",

	'responses'=>"Réponses",

	'reply'=>"Répondre",

	'report'=>"Signaler",

	'today'=>"Aujourd’hui",

	'yesterday'=>"Hier",

	'days-ago'=>"Il y a :number jours",

	'administrator'=>"*Les commentaires peuvent être modérés a posteriori s’ils sont jugés inappropriés ou offensants. Seul l’administrateur a autorité sur la modération.",

	'latest-head'=>"<h6>Dernières</h6><h5>Discussions</h5>",

	'chat-responses'=>"Réponses",

	'we-like-to'=>"Nous aimerions discuter des sujets qui vous importent. Si votre organisation souhaite mener une discussion sur un sujet pertinent pour la communauté #LocalizingSDGs, nous serions ravis de vous compter comme notre prochain modérateur.",

	'suggest-btn'=>"Proposer un sujet de discussion",

	'previous-heading'=>"<h6>Discussions</h6><h5>précédentes</h5>",

	'stories-heading'=>"Histoires",

	'events-heading'=>"Événements",

	'does-your'=>"Votre organisation souhaite-t-elle lancer une discussion ?",

	'featured'=>"Nous serions ravis de l’héberger !",

	'suggest-a-topic'=>"Proposer un sujet",

	'feature'=>"Ajouter un commentaire",

	'other-current'=>"<h6>Autres</h6> <h5>discussions en cours</h5>",

	'previous-discussions'=>"<h6>Discussions</h6> <h5>précédentes</h5>",

	'we-look-forward'=>"Nous attendons vos réfections et points de vue. Pour rédiger vos réponses, il est utile de lire tous les commentaires déjà publiés. Cela vous aidera à approfondir la conversation en tirant parti des messages précédents. Ainsi, la discussion pourra rester concise et pertinente. Vous pouvez même nous apprendre quelque chose de nouveau ou appliquer un concept d’une manière innovante.",

	'search-head'=>"<h6>Recherchez des événements </h6><h5>proches de vous</h5>",

	'searchbar-text'=>"Rechercher",

	'upcoming-head'=>"<h6>Événements</h6> <h5>à venir</h5>",

	'help-us'=>"Aidez-nous à réunir les voix parlant de localiser les ODD. En partageant un événement qui contribue à faire avancer l’initiative, vous créez davantage d’opportunités pour permettre à tous, acteurs et citoyens, de participer et d’en bénéficier. Qu’il s’agisse d’une conférence, d’un séminaire ou d’un atelier, nous aimerions présenter votre événement à la communauté#LocalizingSDGs.",

	'submit-an-event'=>"Proposer un événement",

	'past-head'=>"<h6>Événements</h6><h5>passés<h5>",

	'promote'=>"Vous avez un événement que vous souhaitez promouvoir ?",

	'feature-it'=>"Nous serions ravis de le présenter !",

	'share'=>"Partager un événement",

	'other'=>"<h6>Autres</h6> <h5>événements</h5>",
	'discuss-comment'=>'Nous attendons vos réfections et points de vue. <br>Pour rédiger vos réponses, il est utile de lire tous les commentaires déjà publiés. <br>Cela vous aidera à approfondir la conversation en tirant parti des messages précédents. <br>Ainsi, la discussion pourra rester concise et pertinente. <br>Vous pouvez même nous apprendre quelque chose de nouveau ou appliquer un concept d’une manière innovante.'


	
];
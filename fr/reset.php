<?php

return[

'reset-head'=>"<h6>Réinitialiser</h6> <h5>votre mot de passe ?</h5>",

'pswd'=>"Nouveau mot de passe",

'retype'=>"Vérification du nouveau mot de passe",

'set-pswd'=>"Changer le mot de passe",

'Passwords-must'=>"Les mots de passe doivent ",

'Passwords-must-one'=>"inclure une combinaison de lettres et de chiffres ;",

'Passwords-must-two'=>"ne comporter aucun caractère spécial.",

'Passwords-must-three'=>"Réinitialiser votre mot de passe ?",

'successfully'=>"Votre mot de passe a été réinitialisé avec succès ",

'return'=>"<a href=':link'> <strong>Cliquez ici</strong></a> pour revenir à la page de connexion.",

'old-pass'=>"Ancien mot de passe",

'return'=>"<a href='javascript:void(0);' class ='check'><strong>Cliquez ici</strong></a> pour revenir à la page de connexion.",


];
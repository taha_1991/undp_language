<?php

return[


	'share-head'=>"<h6>Partagez</h6><h5>votre perspective</h5>",

	'outlook-text'=>"<p>Faites une suggestion ou exprimez vos souhaits pour l’avenir de la plate-forme #LocalizingSDGs.</p><p>Dites-nous ce que « localiser » signifie pour vous, quelles sortes d’outils, de programmes et de services font une différence dans la vie des citoyens et des gouvernements, et comment un membre de cette communauté a pu vous aider.</p> <p>Nous nous réjouissons de votre participation.</p></br></br><br><br><br>
	<a href='javascript:void(0);' class='contact-link' onclick='$('#outlook-form').show();'>Cliquez ici pour partager votre perspective</a>",

	'name'=>"Nom",
	'email'=>"Adresse e-mail",
	'organization'=>"Organisation",
	'designation'=>"Désignation",
	'coments'=>"Commentaires",
	'authorize'=>"<span class='checkboxfr'>J'autorise cette initiative à citer mon message sur la site web</br> <span class='checkbox1fr'> et publications connexes.</span>",
	'wish'=>"<span class='checkbox2fr'>Je souhaite rester anonyme</span>",
	'send-me'=>"<span class='checkbox3fr'>Envoyez-moi une copie de cet email.</span>",
	'enter Verification'=>"Entrez le code de vérification",
	'phone'=>"Numéro de téléphone",
	'comments'=>"Vos commentaires ou questions en détail",
	'web-link'=>"Lien vers la site Web ",
	
	'send-head'=>"<h6>Envoyer Des</h6><h5>commentaires techniques</h5>",

	'technical-text'=>"<p>Avez-vous rencontré des problèmes techniques sur ce site ?Merci de nous faire part de vos retours ! </p>

						<p><strong>Quelques recommandations:</strong><br/> 
						Veuillez ne rapporter qu’un seul problème par message. Saisissez une description concise expliquant votre problème</p>

						<p>Expliquez comment reproduire votre problème : les étapes que vous avez suivies, le problème survenu, ce qui aurait dû se produire. Ajoutez, le cas échéant, des exemples ainsi que tous les messages d’erreur que vous avez pu recevoir.</p>
						<a href='javascript:void(0);' class='contact-link' onclick='$('#feedback-form').show();'>Cliquez ici pour signaler des problèmes et envoyer vos retours</a>",

	'tech-name'=>"Nom",
	'tech-email'=>"Adresse e-mail",
	'tech-Phone'=>"Numéro de téléphone",
	'tech-organization'=>"Organisation",
	'tech-coments'=>"Vos commentaires ou questions en détail",
	'tech-Web-Link'=>"Lien vers la site Web",
	'tech-Enter-Verification'=>"Entrez le code de vérification",
	
	'contact-head'=>"<h6>Détails</h6><h5>de contact</h5>",

	'gtf-head'=>"Global Taskforce des gouvernements locaux et régionaux",

	'gtf-sub-head'=>"GTF",

	'gtf-address'=>"Contactez le GTF si vous voulez plus d'informations sur le travail des gouvernements locaux et régionaux sur localisation et le plaidoyer pour les ODD.",

	// 'gtf-location'=>"Suite 5110, HCI Building<br/>1125 Colonel By Drive, Ottawa, K1S 586",

	// 'gtf-email'=>"ottawa@undp.com",

	// 'gtf-website'=>"www.gtf.com",



	'un-head'=>"Programme des établissements humains des Nations Unies",

	'un-sub-head'=>"ONU-HABITAT",

	'un-address'=>"Contactez ONU-Habitat si vous êtes intéressés à en savoir plus sur l'action de l'ONU vers un meilleur avenir urbain: où les villes deviennent des agents inclusifs de la croissance économique et le développement social.",

	// 'un-location' =>"Luisenstrasse 40<br/>10117 Berlin",

	// 'un-email' =>"berlin@undp.com",

	// 'un-website' =>"www.unhabitat.com",



	'undp-head'=>"Programme de Développement des Nations Unies",

	'undp-sub-head'=>"PNUD",

	'undp-address'=>"Contactez le PNUD si vous souhaitez de plus amples informations sur le soutien de l'ONU fourni aux pays partenaires, afin de mettre efficacement en œuvre le nouveau programme de développement au niveau local, et de promouvoir la prospérité économique à long terme etle bien-être de l'humanité et de l'environnement.",


	// 'undp-location' =>"One United Nations Plaza<br />New York, NY 10017 USA",

	// 'undp-email' =>"info@undp.com",

	// 'undp-website' =>"www.undp.com",

	'outlook-confirm'=>"MERCI D’AVOIR PARTAGÉ VOS RÉFLEXIONS",

	'outlook-confirm-text'=>"Vos points de vue sont extrêmement précieux ! Ils nous aident à comprendre ce qui vous importe le plus. Nous lisons et prenons en compte toutes vos suggestions et tous vos retours d’expérience, et nous pouvons être amenés à vous contacter à l’occasion.",

	'report-confirm'=>"MERCI D’AVOIR SIGNALÉ UN PROBLÈME",

	'report-confirm-text'=>"Nous espérons résoudre votre problème rapidement.",


];
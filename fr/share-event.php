<?php

return[
	
	'share-event-head'=>"<h6>PARTAGER UN </h6><h5>ÉVÉNEMENT</h5>",

	'share-event-text'=>"<p>Cet espace permet aux organisations, individus et communautés de promouvoir leurs événements. Nous serions ravis de connaître des événements qui suscitent des discussions et intègrent de nouvelles voix au monde du développement local.</p><p>Vous pouvez demander à présenter des événements tels qu’ateliers interactifs, discussions communautaires, conférences, colloques, etc. liés à la localisation des ODD. Ce faisant, vous permettrez à notre communauté d’être totalement engagée à vos côtés. </p><p>Pour garantir la pertinence des événements dans le calendrier, les demandes de promotion ne seront publiées qu’après approbation.</p>",

	'title'=>"Intitulé de l’événement",

	'organ'=>"Organisateur(s) de l’événement",

	'description'=>"Description",

	'file'=>"Fichier",
	
	'edit'=>"Édition",

	'insert'=>"Insertion",

	'view'=>"Affichage",
 
	'format'=>"Format",

	'table'=>"Tableau",

	'tools'=>"Outils",

	'date'=>"Date",

	'Seledate'=>"Sélectionnez la date",

	'time'=>"Heure",

	'Selefrom'=>"de",

	'Seleto'=>"à",

	'country'=>"Pays",

	'Selecoun'=>"Sélectionnezle pays",

	'state'=>"Province / État",

	'Selestate'=>"SélectionnezProvince / État",

	'city'=>"Ville",

	'Selecity'=>"Sélectionnezla Ville",

	'venue'=>"Lieu",

	'cover-photo'=>"Illustration de couverture",

	'upload'=>"Télécharger des informations supplémentaires",

	'add-more-docu'=>"Ajouter des documents supplémentaires ",

	'agenda-pdf'=>"S'il vous plaît veuillez formater le titre de votre document pour indiquer son contenu (par exemple Concept Note.pdf / Agenda.pdf)",

	'web-link'=>"Lien vers le site web de l’événement",

	'thank-you'=>"MERCI D’AVOIR PARTAGÉ CET ÉVÉNEMENT.",

	'it-will'=>"Il sera mis en ligne après approbation par le modérateur.",

	'you-will'=>"Vous en serez notifié.",

	'until-then'=>"D’ici là, vous pouvez consulter les autres <a href=':link'><strong>Événements</strong></a>",
		
	'choose-file'=>"<strong>Sélectionnez un fichier</strong>",

	'image-should'=>"Les images doivent être au format JPEG / PNG et ne pas dépasser 2Mo",



];
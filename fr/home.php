<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'banner-head'=>"LA BOITE À OUTIL POUR LOCALISER<br><span id='goals'>LES OBJECTIFS DE DÉVELOPPEMENT DURABLE</span>",
    'banner-main-text'=>"Une mine d’or de ressources pratiques et sur-mesure pour vous !",
    'banner-text'=>"Découvrez les outils et les ressources pratiques, ainsi que les conseils et expériences de nombreux acteurs du développement !",
    'banner-sign-up'=>"Inscrivez-vous ici et rejoignez notre communauté",
    'banner-video'=>"Vidéo",
    'why'=>"<h6>POURQUOI</h6><h5>NOUS REJOINDRE?</h5>",
    'discover'=>"Découvrez les outils",
    'discover-text'=>"Les gouvernements et organisations du monde entier œuvrent pour le progrès durable, dans des contextes qui leurs sont propres. Nous avons rassemblé les précieuses leçons qui nous ont été transmises et les avons capitalisées dans des outils et des guides intuitifs et pratiques, pour vous.",
    'contribute'=>"Partagez vos ressources",
    'contribute-text'=>"Vous avez déjà développé des idées pour aider les acteurs locaux? Contribuez à notre bibliothèque et faites-vous connaitre! Notre objectif est de constituer un référentiel en ligne de solutions et de pratiques du développement.",
    'story'=>"Racontez votre histoire",
    'story-text'=>"Les histoires ont le pouvoir de nous connecter à la vie et aux expériences des autres. Un bon récit est un tremplin puissant vers des échanges et interactions. Partagez vos histoires et renforcez vos liens avec notre communauté.",
    'discuss'=>"Echangez vos points de vue",
    'discuss-text'=>"Commencez des discussions sur des sujets et des idées pertinentes sur la  « localisation », et ouvrez-vous à diverses perspectives. Vous pouvez même suggérer un thème et votre organisation peut être sélectionnée pour animer notre prochain débat !",
    'event'=>"Publiez vos évènements",
    'event-text'=>"Diffusez vos évènements grâce à notre calendrier. Nous faisons la promotion d’évènements à travers le monde visant à localiser les ODD.",
    
    'latest-head'=>"<h6>DERNIÈRES</h6><h5>ACTUALITÉS</h5>",
    'twitter'=>"<h6>REJOIGNEZ LE </h6><h5>DÉBAT</h5>",
     
    'local'=>"<h6>NOS CONTRIBUTEURS</h6><h5>LOCAUX</h5>",
    'local-text'=>"Déploiement mondial <br>de nos contributeurs",
    

];

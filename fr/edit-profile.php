<?php

return[
	
	'edit'=>"<h6>EDIT</h6> <h5>PROFILE</h5>",

	'please'=>"S'il vous plait veuillez remplir les informations que vous souhaitez partager avec les membres de cette communauté.",

	'upload'=>"Télécharger image de profil",

	'name'=>"Nom",

	'organ'=>"Organisation",

	'desig'=>"Désignation",
	
	'Contact'=>"Détails de contact",

	'location'=>"Adresse",

	'telphone'=>"Numéro de téléphone",

	'email'=>"Adresse e-mail",

	'facebook'=>"Lien vers la page Facebook",

	'twitter'=>"Lien vers Twitter RSS",

	'profile'=>"Lien vers le profil LinkedIn",

	'website'=>"Lien vers la Site Web",

	'about-me'=>"À propos de moi (maximum 300 mots)",

	'pswd'=>"CHANGER LE MOT DE PASSE",

	'old-pswd'=>"Ancien mot de passe",

	'new-pswd'=>"Nouveau mot de passe",

	'retype'=>"Vérification du nouveau mot de passe",

	'change-pswd'=>"Changer le mot de passe",

	'pswd-must'=>"Les mots de passe doivent:",

	'characters'=>"comporter au moins 8 caractères ;",

	'numbers'=>"inclure une combinaison de lettres et de chiffres ; ",

	'special-characters'=>"ne comporter aucun caractère spécial.",

	'log-out'=>"SE DÉCONNECTER",

	'logged-out'=>"Vous êtes maintenant déconnecté",

	'click-here'=>"Cliquez ici pour retourner à la page d'accueil.",

];
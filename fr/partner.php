<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'core'=>"<h6>PARTENAIRES</h6> <h5>PRINCIPAUX</h5>",
    'gtf'=>"Le Groupe de Travail des Gouvernements locaux et régionaux",
    'gtf-small'=>"GTF",
    'gtf-text'=>"Le <strong>Groupe de Travail des Gouvernements locaux et régionaux</strong> est un mécanisme de coordination qui rassemble les principaux réseaux internationaux de gouvernements locaux afin de mener un plaidoyer conjoint dans les processus de politique internationale, en particulier l’Agenda pour le changement climatique, les Objectifs de Développement Durable et Habitat III.",
    'habitat'=>"Le Programme des Nations Unies pour les Établissements Humains",
    'habitat-small'=>"ONU-Habitat",
    'habitat-text'=>"<strong>ONU-Habitat</strong> est l’Agence pour les villes et les établissements humains durables. Elle est mandatée par l’Assemblée Générale de l’ONU pour soutenir des villes socialement et écologiquement durables et vise à proposer un logement adéquat pour tous. Au sein du Système des Nations Unies, ONU-Habitat a également le mandat spécifique d’agir en tant que point focal des gouvernements locaux et de leurs associations, en soutenant les gouvernements locaux et régionaux en tant qu’acteurs essentiels du développement.",
    'undp'=>"Le Programme des Nations Unies pour le Développement",
    'undp-small'=>"PNUD",
    'undp-text'=>"<strong>Le PNUD</strong> est présent dans près de 170 pays et territoires, œuvrant à l’éradication de la pauvreté, et la réduction des inégalités et de l’exclusion. Nous aidons les pays à élaborer des politiques, à développer des compétences d’encadrement, de partenariat, des capacités institutionnelles, et à construire la résilience afin d’obtenir des résultats durables. Le PNUD se consacre également au renforcement de l’ouverture et de la responsabilisation des gouvernements locaux. Il s’assure que ces derniers ont la capacité à gérer les opportunités et les responsabilités que la décentralisation leur confère. Enfin, il vise aussi à optimiser leur potentiel et leur rôle en tant que partenaires du développement.",
    'contributing'=>"<h6>PARTENAIRES </h6><h5>CONTRIBUTEURS</h5>",
    'contributing-text'=>"La participation de tous les acteurs est une pierre angulaire de cette initiative. Nous serions heureux de représenter d'autres voix, si nous recevons plus de contributions.",
    
    'lrg'=>"Gouvernements locaux et régionaux",
    'ng'=>"Gouvernements <br>nationaux",
    'io'=>"Organisations <br>internationales",
    'cso'=>"Organisations de la <br>société civile",
    'ps'=>"Secteur <br>privé",
    'ari'=>"Universités et Instituts de recherche",
    
    'lrg-head'=>"<h6>GOUVERNEMENTS</h6> <br><h5>LOCAUX ET REGIONAUX</h5>",
    'ng-head'=>"<h6>GOUVERNEMENTS</h6> <br><h5>NATIONAUX</h5>",
    'io-head'=>"<h6>ORGANISATIONS</h6> <br><h5>INTERNATIONALES </h5>",
    'cso-head'=>"<h6>ORGANISATIONS</h6> <br><h5>DE LA SOCIÉTÉ CIVILE</h5>",
    'ps-head'=>"<h6>SECTEUR</h6> <br><h5>PRIVÉ</h5>",
    'ari-head'=>"<h6>Universités ET</h6> <br><h5>INSTITUTS RECHERCHE</h5>",
						
	'go-to-website'=>"Aller sur le site web",
	'why'=>"Pourqoi sont-ils essentiels  aux ODD?",
	'role'=>"Rôle dans la localisation des ODD",
	
	'lrg-pop-head'=>"GOUVERNEMENTS LOCAUX ET REGIONAUX (et leurs associations)",
	'ng-pop-head'=>"GOUVERNEMENTS NATIONAUX",
	'cso-pop-head'=>"ORGANISATIONS DE LA SOCIÉTÉ CIVILE",
	'io-pop-head'=>"ORGANISATIONS INTERNATIONALES",
	'ps-pop-head'=>"SECTEUR PRIVÉ",
	'ari-pop-head'=>"UNIVERSITES ET INSTITUTS DE RECHERCHE",
	
	'lrg-role'=>"<ul><li>En tant que sphère gouvernementale la plus proche des citoyens et leur principal accès pour participer aux affaires publiques, les gouvernements locaux et régionaux (GLR) sont dans une position unique pour définir et répondre aux besoins de développement, en soutenant les objectifs locaux et nationaux.</li>
				<li>Leur pouvoir de mobilisation facilite la recherche du consensus entre les différents acteurs institutionnels, les groupes d’intérêts et les acteurs (secteur privé, public, académique, etc.). Ils peuvent promouvoir des initiatives conjointes et une planification stratégique dans leurs domaines respectifs.</li>
				<li>Les GLR sont également les mieux placés pour améliorer l’efficacité du développement au niveau local. Ils renforcent l’appropriation des projets sur le terrain ; ils promeuvent des partenariats ouverts entre les autorités publiques, le secteur privé et les organisations de la société civile ; ils se concentrent sur des résultats tangibles, la responsabilité et la transparence.</li>
				<li>Le rôle actif des GLR dans la coopération internationale pour le développement est essentiel à l’accomplissement de résultats de développement, à la démocratisation des processus, et à l’appropriation par tous les acteurs. </li>
				<li>Les associations de GLR représentent une diversité de gouvernements locaux (en termes de taille, de géographie, de capacité, etc.). Ils participent au plaidoyer, à la formation de leurs membres, et parlent d’une même voix auprès des autres acteurs.</li>
			</ul>
			<p>La Boîte à outils cherche à identifier les meilleurs instruments, mécanismes et bonnes pratiques afin de renforcer les compétences des GLR pour mieux promouvoir et mettre en œuvre les ODD au sein de leurs territoires. Son objectif est de permettre aux GLR de devenir les champions et les facilitateurs de processus de développement intégrés, durables et ouverts au niveau local.</p>",

	'ng-role'=>"<ul><li>Les gouvernements nationaux dirigent les politiques de développement en lien avec les ODD. Ils ont un <strong>rôle de développement et de coordination</strong> dans le processus de mise en œuvre des ODD. Ils sont responsables de la définition et de la conception de stratégies nationales en accord avec les ODD.</li>
				<li>L’engagement des gouvernements nationaux est essentiel à la localisation effective des ODD. Ce sont eux qui au final mettent en place un <strong>environnement favorable</strong> et un cadre de décentralisation, permettent aux gouvernements locaux et régionaux et autres acteurs de fournir des services.</li>
				<li>Ils ont la capacité d’établir un dialogue et des relations collaboratives avec le large spectre des acteurs du développement local. Ils peuvent concevoir des outils adaptés pour rendre les acteurs, et particulièrement les GLR, capables de gérer des stratégies locales de développement.</li>
				<li>Les gouvernements nationaux sont responsables devant les citoyens de la mise en œuvre des ODD, mais aussi aux yeux de la communauté internationale à travers les dialogues annuels du Forum Politique de Haut Niveau. Afin de préparer les dialogues régionaux et mondiaux relatif à la mise en œuvre des ODD, ils peuvent présenter des <strong>exemples concrets de réussite</strong> sur le terrain.</li>
				<li>Ils sont responsables de la <strong>conception d’une cadre national de suivi et de reporting,</strong> de pair avec un ensemble d’indicateurs nationaux, y compris des indicateurs désagrégés relevés au niveau local. Les gouvernements nationaux recueillent des retours du niveau local afin de pouvoir rapporter les progrès de mise en œuvre et compiler – ou mettre à jour – les rapports annuels sur le développement durable.</li>
			</ul>
			<p>La Boîte à outils propose des instruments, des méthodologies et des bonnes pratiques visant à renforcer les capacités des gouvernements nationaux, afin d’inclure réellement les GLR dans la définition, la mise en œuvre et le suivi des stratégies des ODD.</p>",
			
	'io-role'=>"<ul><li>Les organisations internationales favorisent la communication sur le nouvel agenda, peuvent renforcer les partenariats pour sa mise en œuvre et fournir des données pour le suivi et la revue.</li>
				<li>Dans ce contexte, le Groupe de Développement des Nations Unies a spécialement établi une stratégie de soutien à la mise en œuvre effective et cohérente du nouvel agenda de développement durable, le ‘IASP’ (Intégration, Accélération et Soutien des Politiques).</li>
				<li>Grâce à l’approche IASP, les Nations Unies accompagnent les pays de trois manières différentes. Tout d’abord, en aidant les gouvernements à refléter le nouvel agenda mondial dans leurs stratégies et politiques nationales de développement. Ensuite, en appuyant les pays à accélérer l’atteinte des cibles ODD. Enfin, en mettant à disposition des gouvernements l’expertise des Nations Unies en matière de développement durable et de gouvernance, à toutes les étapes de la mise en œuvre.</li>
				<li>Cet ensemble intégré d’outils de soutien permet aux Nations Unies, à ses agences et ses programmes d’accompagner les pays partenaires, afin d’effectivement mettre en œuvre le nouvel agenda de développement et de faire de la prospérité économique, du bien-être humain et environnemental une réalité de long terme.</li>
			</ul>
			<p>La Boîte à outils propose des instruments, des méthodologies et des bonnes pratiques permettant d’ancrer les ODD au niveau local, avec le soutien des organisations internationales.</p>",

	'cso-role'=>"<ul><li>Les Organisations de la société civile (OSC) jouent un rôle clé dans la mise en œuvre des ODD car pour les atteindre, il est nécessaire de <strong>régulièrement mobiliser les communautés</strong> et de s’appuyer sur un engagement volontaire. Elles sont ainsi un <strong>levier de sensibilisation des ODD</strong> au niveau des communautés. Elles ont également un rôle de plaidoyer au sein des campagnes nationales et internationales, en montrant des exemples concrets de comment rendre l’agenda 2030 pertinent et l’intégrer aux actions locales.</li>
				<li>Les OSC jouent également un rôle de <strong>médiateur</strong> lors de la conception des politiques : elles sont le lien entre les citoyens et les responsables politiques. Elles aident à identifier les priorités de développement, proposent des solutions pratiques et repèrent les opportunités de localisation des ODD.</li>
				<li>Les OSC collaboratives peuvent encourager l’<strong>inclusion des citoyens dans les processus de prise de décision</strong> notamment par rapport aux GLR. Les OSC peuvent offrir des espaces neutres propices à des dialogues réguliers entre les différents acteurs, et permettent de faire le lien entre les processus de décision et les mesures de mise en œuvre. Les mécanismes de coordination peuvent aider à articuler la coopération territoriale et à assurer la localisation des ODD.</li>
				<li>Les OSC peuvent aussi fournir aux citoyens des outils pour <strong>suivre les progrès</strong> effectués et tenir leurs gouvernements responsables de la mise en œuvre des ODD, de façon transparente et participative.</li>
				<li>Elles peuvent également <strong>formerles acteurs concernés</strong> et les décisionnaires aux pratiques de mise en œuvre et d’application. </li>
				<li>Les OSC peuvent assurer la <strong>transparence de la collecte des informations</strong> et recueillir des données fiables, utilisées mondialement, et avec l’appui des gouvernements.</li>
			</ul>
			<p>La Boîte à outils se concentre sur l’analyse du rôle des OSC dans la localisation des politiques de développement, afin de fournir des outils pratiques pour valoriser leur expérience et favoriser une mise en œuvre efficace des ODD.</p>",

	'ps-role'=>"<ul>
				<li>Les entreprises, lorsqu’elles sont ouvertes et sociales, peuvent représenter un moyen efficace et stratégique pour <strong>atteindre les populations locales</strong> et répondre aux besoins locaux.</li>
				<li>Elles peuvent appuyer des questions de gouvernance particulières et s’engager dans la mise en œuvre des ODD. Les structures régulées et les systèmes d’incitation favorisent la coopération et <strong>encouragent les partenariats publics-privés et les mécanismes de financement.</strong> </li>
				<li>Les entreprises doivent participer à la planification des agendas de mise en œuvre des ODD à l’échelle locale et nationale. Inclure le secteur privé dans la planification, et non le limiter à la mise en œuvre, poussent les entreprises à <strong>partager la responsabilité</strong> d’affronter les défis et de trouver des solutions.</li>
				<li>Les partenariats ouverts entre les gouvernements, les entreprises et les organisations de la société civile sont indispensables à la création d’un dialogue, à l’établissement d’agendas politiques communs, à la mobilisation des ressources, et à la garantie d’une responsabilité partagée. Ils permettent de faire face à des défis systémiques, au-delà des initiatives individuelles et des partenariats fondés sur des projets. Les entreprises peuvent <strong>créer des liens dans des secteurs clés comme la recherche et de l’innovation, la formation professionnelle</strong>, le développement de la chaine de valeurs, de la croissance des petites et moyennes entreprises, etc.</li>
				<li><strong>L’investissement du secteur privé et les solutions de marché </strong>sont nécessaires pour atteindre une échelle certaine et un impact durable. L’alignement des modèles d’entreprise et de développement peut créer une stratégie hybride. Les dimensions philanthropiques et commerciales peuvent par exemple être associées afin de tirer profit des compétences et des ressources disponibles.</li>
				<li>Les entreprises peuvent contribuer à la <strong>création de métriques quantifiables</strong> pour les ODD, en termes de cible, de suivi, d’évaluation et de responsabilité. Cela permet de mesurer l’impact des opérations commerciales, d’aligner les indicateurs des entreprises, et d’intégrer les résultats dans des mécanismes de gestion de la performance et d’évaluation. Les systèmes de suivi indépendants doivent également prendre en compte les résultats des partenariats de développement.</li>
			</ul>
			<p>La Boîte à outils a pour objectif d’identifier et de documenter les pratiques et outils existants. Elle vise à explorer toutes les voies possibles pour accroitre la sensibilisation, l’engagement et la mobilisation des acteurs privés, en partenariat avec les autres acteurs pour la mise en œuvre des ODD.</p>",

	'ari-role'=>"<ul>
				<li>Les universités jouent un rôle essentiel de <strong>formation de ressources humaines qualifiées</strong>. Elles éduquent et forment les citoyens et les dirigeants à la localisation des ODD et, plus généralement, à l’importance des politiques de développement.</li>
				<li>Elles conduisent des études sur le développement et fournissent des <strong>analyses</strong> utiles à la définition de politiques pertinentes. Cela permet d’enrichir les connaissances existantes sur les ODD, appuyant ainsi les efforts de définition de schémas conceptuels de politiques et de gouvernance pour localiser les ODD.</li>
				<li>Le secteur académique a le rôle principal de légitimation et de <strong>définition de cadres théoriques</strong> liées à des idées pratiques, et peut les rendre accessibles aux communautés et aux citoyens.</li>
				<li>Les institutions universitaires et de recherche peuvent également être un élément clé dans la <strong>diffusion des connaissances </strong>sur les mécanismes de la Boîte à outils, et dans l’enrichissement du débat scientifique sur la localisation du développement. Elles partagent des informations, des données, des documents et des expériences sur les processus de localisation.</li>
				<li>Elles participent au dialogue politique dans leurs pays et au sein de la communauté internationale en consolidant la collaboration avec les représentants et responsables publiques, et en <strong>formant des partenariats.</strong></li>
			</ul>
			<p>La Boîte à outils met à jour les bonnes pratiques existantes et les exemples concrets sur le rôle du secteur académique dans le développement, et propose des outils pratiques pour renforcer son implication dans la mise en œuvre des ODD. </p>",
			'nudge'=>"<span class='nudge-text'>Est-ce que vous êtes en train de partager des outils, des documents, des histoires ou des événements, ou est-ce que vous modérez une discussion ?<br>
Nous serions heureux de vous énumérer en tant que partenaire dans la localisation des ODD.<br></span>
<span class='nudge-main'>Veuillez s'il vous plaît <a href='mailto:info.art@undp.org'>nous contacter</a> pour être publié sur le site.</span>",
];

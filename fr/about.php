<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'banner-head'=>"Vous avez besoin de <br>méthodes pratiques pour<br>atteindre les ODD au niveau local?",
    'banner-text'=>"Le PNUD,ONU-Habitat et la Global Taskforce vous proposent <br> des outils et des guides pour vous soutenir.<br>Faites-nous part de vos idées !",
    
    'intro'=>"<span class='frIntro'>PRÉSENTATION DE LA BOITE À OUTILS</span>",
    'purpose'=>"<span class='frIntro'>OBJECTIF</span>",
    'local'=>"<span class='frIntro'>QU’EST CE QUE LA LOCALISATION?</span>",
    'intro-text'=>"<p class='animated fadeInDown'>Intuitive, simple et concrète.</p><p class='animated fadeInDown'>Cette Boîte à outils ne contient ni directives strictes, ni solutions universelles. En revanche, elle offre des mécanismes et des instruments pratiques et adaptables, répondant aux différents défis du développement. Les ressources que nous vous proposons permettent de valoriser les acteurs locaux et vous aident à intégrer les objectifs mondiaux à des actions locales. </p><p class='animated fadeInDown'>La recherche d’outils et de stratégies adaptés pour « localiser » les ODD est un élément clé de la conception, de la mise en œuvre, du suivi et du succès de l’Agenda 2030 pour le Développement Durable. Les outils que vous trouverez ici peuvent être utilisés dans des contextes hétérogènes et complexes, où les paramètres politiques, institutionnels, économiques et sociaux peuvent varier - non seulement entre les pays, mais également entre les territoires d’un même pays, voire au sein d’un territoire particulier. Ces instruments sont conçus en prenant en compte un large éventail d’expériences régionales, et avec l’apport de différents acteurs.</p><p class='animated fadeInDown'>Notre objectif est de rester simple et de fournir des conseils applicables à des environnements changeants. Nous nous efforçons également d’être souples tout en demeurant concrets. Nous travaillons à fournir des conseils pratiques pour évaluer, planifier, mettre en œuvre et suivre les politiques locales, en accord avec les stratégies de réussite des Objectifs de Développement Durable.</p>",
    
    'purpose-text'=>"<p class='animated fadeInDown'>Le principal objectif de cette Boîte à outils est de proposer un ensemble articulé d’outils permettant d’appuyer les acteurs locaux et leurs réseaux, sous le leadership des gouvernements locaux, régionaux et nationaux.</p>

				<p class='animated fadeInDown'>La Boîte à outils cherche à <strong>sensibiliser</strong> les acteurs locaux et nationaux aux Objectifs de Développement Durable. Elle vise à mieux les informer sur l’Agenda 2030, à les familiariser avec les implications, les opportunités et défis inhérents à la localisation des ODD, et à les inciter à prendre conscience de leur rôle essentiel. La Boîte à outils est également une plateforme de <strong>plaidoyer</strong> et de mobilisation : elle cherche à créer un environnement propice au processus de localisation afin d’accompagner l’appropriation par les acteurs locaux et d’assurer l’intégration des ODD dans les stratégies et plans infranationaux.</p>

				<p class='animated fadeInDown'>La boîte à outils <strong>capitalise</strong> et revoit des instruments existants, systématise les résultats et les partage avec les responsables locaux, les experts et les acteurs locaux. Son objectif est d’apporter un <strong>soutien pratique </strong>  aux acteurs locaux, notamment aux gouvernements locaux et régionaux, en identifiant des bonnes pratiques réplicables, ceci afin de concevoir, mettre en œuvre et suivre les politiques en accord avec les ODD.</p>
				
				<p class='animated fadeInDown'>Le processus permet de valoriser les acteurs au sein de la nouvelle architecture du développement, y compris les gouvernements locaux, régionaux et nationaux, les organisations de la société civile, le secteur privé, les universités et les instituts de recherche. Notre objectif est de mieux intégrer les divers acteurs aux paysages sociaux, régionaux, politiques et économiques des territoires. Peu importe le niveau atteint par votre initiative, ou l’ODD que vous cherchez à accomplir, vous trouverez ici des outils cohérents et structurés pour vous y aider. </p>",

	'local-text'=>"<p class='animated fadeInDown'>Localiser le développement signifie <strong>prendre en compte les contextes locaux et régionaux </strong> dans l’accomplissement de l’Agenda 2030, depuis la conception des objectifs et des cibles, à la  <strong>définition des moyens de mise en œuvre,</strong> en passant par <strong>l’utilisation d’indicateurs </strong>pour mesurer et suivre les progrès. Il s’agit également de mettre les priorités, les besoins et les ressources des territoires et des populations au cœur du développement durable. Enfin, localiser le développement implique des échanges continus entre les acteurs mondiaux, nationaux et locaux.</p>

				<p class='animated fadeInDown'>Auparavant, la localisation était principalement comprise comme la mise en œuvre des objectifs au niveau local, par des acteurs infranationaux, notamment par les gouvernements locaux et régionaux. Depuis, ce concept a évolué. Tous les ODD ont des cibles directement liées aux responsabilités des gouvernements locaux et régionaux.  C’est la raison pour laquelle la réussite des ODD dépend plus que jamais de <strong>la capacité des gouvernements locaux et régionaux à promouvoir un développement territorial intégré, ouvert et durable.</strong></p>

				<p class='animated fadeInDown'><strong>Les gouvernements locaux et régionaux sont des décideurs politiques</strong>, des catalyseurs du changement et le niveau de gouvernement le mieux placé pour faire le lien entre les objectifs généraux et les communautés locales. Localiser le développement est ainsi un processus de valorisation des acteurs locaux, visant à rendre le développement durable plus réactif, et ainsi, pertinent par rapport aux aspirations et aux besoins locaux. Les objectifs du développement sont réalisables seulement si les acteurs locaux participent pleinement, non seulement à leur mise en œuvre, mais aussi à leur définition et à leur suivi.</p>

				<p class='animated fadeInDown'>Afin de stimuler la participation des acteurs locaux, l’élaboration des politiques publiques doit être ouverte et partagée tout au long de la chaine politique, et non imposée par le haut. Tous les acteurs importants doivent être inclus dans la prise de décision au niveau local et national, grâce à des<strong> mécanismes consultatifs et participatifs</strong>.</p>",
                
	'go-toolbox'=>"DÉCOUVRIR LA BOITE À OUTILS",
	'when'=>"<h6>AU </h6><h5>COMMENCEMENT...</h5>",
	'work'=>"<h6>TRAVAILLER </h6><h5>AVEC LES OUTILS</h5>",
	'thinks'=>"Ce que pense notre communauté",
    
    

];

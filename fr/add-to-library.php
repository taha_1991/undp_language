<?php

return[

	'add-head'=>"<h6>AJOUTER À</h6><h5>LA BIBLIOTHÈQUE</h5>",

	'purpose'=>"Objectif de cette bibliothèque",

	'add-text'=>"<p>Avez-vous un document pratique susceptible d’aider des communautés locales à atteindre les cibles liées aux Objectifs de Développement Durable ? Contribuez à cette bibliothèque !</p><p>Pour garantir la pertinence des contenus de la bibliothèque, les contributions ne seront publiées qu’après approbation. Veuillez noter que nous pouvons apporter ou suggérer des modifications mineures durant la modération.</p>",

	'title'=>"Titre du document",
	'author'=>"Auteur",

	'developed'=>"Développé par : nom(s) de(s) organisation(s)",

	'type'=>"Type de document",

	'Seledoc'=> "Choisissez le type de document",

	'language'=>"Langue",

	'Selelang'=>"Sélectionnez la langue",

	'upload'=>"Votre téléchargement",

	'choose'=>"Sélectionnez un fichier ou déposez-le ici",

	'image'=>"Les images doivent être au format PDF et ne pas dépasser 5Mo.",

	'description'=>"Veuillez saisir une description succincte",

	'region'=>"Région associée",

	'Selereg'=>"Sélectionnez la région",

	'country'=>"Pays associé",

	'Selecon'=>"Choisissez le pays",

	'add-more'=>"Ajouter",

	'year'=>"Document publié dans",

	'Seleyear'=>"Sélectionnez l’année",

	'another-language'=>"Proposer ce document dans une autre langue",

	'terms'=>"J’accepte les <a href=':terms'>Conditions d’utilisation</a> et la <a href=':privact'>Politique de confidentialité</a>",

	'thank'=>"MERCI D’AVOIR PARTAGÉ CE DOCUMENT",

	'contribution'=>"<p>Votre contribution apparaîtra en ligne si le modérateur l’approuve.</p><br/><p>Vous en serez notifié.<p>",

	'until'=>"D’ici là, vous pouvez consulter la <a href=':link'><strong>Bibliothèque</strong></a>",

	'purpose-text'=>"<p>Malgré tout ce qu’il est possible de lire et d’étudier sur Internet, il y a encore tant de choses qui ne sont pas encore disponibles en ligne. Cette communauté porte un intérêt tout particulier pour toutes les connaissances encore inexploitées concernant les processus de développement et les solutions découvertes, amassés dans les bibliothèques d’organisations diverses. Pendant ce temps, d’autres organisations « réinventent la roue » et naviguent à travers des processus déjà explorés par d’autres. </p> 
	<p>Il existe déjà de formidables collections et outils en ligne. Ce projet nouveau et ambitieux de Localiser les ODD cherche à centraliser cette masse de connaissances et à <strong>construire un référentiel en ligne de documents liés à la réalisation des ODD au niveau local.</strong> L’objectif est d’être inclusif et participatif, et de découvrir quels agents du changement ont été identifiés comme étant les plus efficaces par les communautés locales.
	Par conséquent, vous êtes invités à contribuer vos idées. Partager votre travail permettra à d’autres acteurs de se renforcer. Cela donnera également à d’autres membres l’occasion de vous montrer d’autres directions, de vous donner des retours sur vos idées, et de vous aider à découvrir si d’autres personnes travaillent sur des thèmes similaires.</p>",
	

];
<?php
//fr
return[

	'register'=>[
	    'subject'=>'Confirma tu registro ',
	    'body'=>"Madame, Monsieur,<br><br>
	        Merci pour votre demande d’inscription à la communauté pour la Localisation des ODD.<br>
                <br>
                Vous êtes à juste une étape de distance d'avoir accès à de connaissances stratégiques, des outils, des ressources, des histoires, des discussions et des événements. Ceux-ci peuvent vous soutenir dans la mise en œuvre des Objectifs de Développement Durable.<br>
                <br>
                <br>
                Cliquez sur lien de confirmation suivant pour commencer. Si vous ne parvenez pas à cliquer directement sur le lien, vous pouvez également le copier et coller dans la barre d'adresse de votre navigateur.
                <br><br>
                <a href=':link'>:link</a>
                <br><br>
                <br><br>
                Pour toute suggestion ou soutien, veuillez nous joindre à travers la section <a href=':contact'>Contactez Nous</a> Nous vous répondrons dans les meilleurs délais
                <br><br>
               Cordialement, <br>
                L’équipe<br><br>",
	    ],
	'newUser'=>[
	    'subject'=>'Bienvenue à la communauté Décentraliser les ODD',
	    "body"=>"
	        Madame, Monsieur,<br><br>
            Bienvenue dans la communauté pour la Localisation des ODD.<br><br>
            
            Votre compte est maintenant actif, et nous vous encourageons à mettre à jour votre profil. Un profil complet permet aux partenaires de se faire une idée de qui vous êtes, en les aidants à mieux interagir avec vous.<br><br>
            
            Nous nous réjouissons de votre participation et contribution! <br><br>
            
            Pour toute suggestion ou soutien, veuillez nous joindre à travers la section  <a href=':contact'>Contactez Nous</a> .  Nous vous répondrons dans les meilleurs délais. <br><br>
            
            Cordialement,<br>
            
            L’équipe<br><br>
	    ",
	    ],
	'forgot'=>[
	    'subject'=>"votre mot de passe",
	    'body'=>"Cher utilisateur, <br><br>
                Réinitialiser votre mot de passe est très facile.<br>
                <br>
                Cliquez sur le lien suivant et remplissez les informations demandées. Si vous ne parvenez pas à cliquer directement sur le lien, vous pouvez également le copier et coller dans la barre d'adresse de votre navigateur<br>
                <br>
                <br>
                <a href=':link'>:link</a>
                <br>
                Nous attendons votre retour avec impatience.<br><br>
                Passez une bonne journée !<br>
                L'équipe<br><br>",
	    ],
	 'library'=>[
	     "subject"=>"Votre document a été téléchargé",
	     "body"=>"Cher utilisateur,<br><br>
                Félicitations! <br><br>

                Votre document a été téléchargé avec succès et il en train d’être examiné. Notre équipe va bientôt vous contacter concernant votre contribution (dans les prochains 15 jours ouvrables). <br><br>
                
                Au cas où vous ne recevrez pas de réponse de notre part dans les prochains 15 jours ouvrables, vous pouvez nous joindre à travers la section <a href=':contact'>Contactez-nous</a> sur le site.<br><br>

                
                Passez une bonne journée!<br>
                
                L'équipe<br><br>",
	     ],
	 'story'=>[
	     "subject"=>"Votre historie a bien été soumise",
	     "body"=>"Madame, Monsieur,<br><br>
                Félicitations!<br><br>

                Votre historie a bien été soumise et est en train d’être examiné. 
Notre équipe va bientôt vous contacter concernant votre contribution, dans les prochains 15 jours ouvrables.
 <br><br>
                
                Au cas où vous ne recevrez pas de réponse de notre part dans les prochains 15 jours ouvrables, vous pouvez nous joindre à travers la section <a href=':contact'>Contactez-nous </a> sur le site web. <br>
               Merci pour partager votre histoire avec la communauté.<br><br>

                
                Cordialement,<br>
                
                L'équipe<br><br>",
	     ],
	 'event'=>[
	     "subject"=>"Votre événement",
	     "body"=>"Madame, Monsieur,<br><br>
                Félicitations!<br><br>

                Votre évènement a été ajouté et est en train d’être examiné.Notre équipe va bientôt vous ouvrables concernant votre contribution, contacter dans les prochains 15 jours.<br><br>
                
                Au cas où vous ne recevrez pas de réponse de notre part dans les prochains 15 jours ouvrables, vous pouvez nous joindre à travers la section <a href=':contact'>Contactez-nous</a> sur le site web.  <br>
                Merci pour votre contribution,<br><br>

                
                El equipo<br><br>",
	     ],
	     
	 'discussion'=>[
	     "subject"=>"Votre sujet de discussion ",
	     "body"=>"Madame, Monsieur,<br><br>
                Félicitations! <br><br>

                Votre sujet de discussion a été ajouté et est en train d’être examiné.<br><br>
                Notre équipe va bientôt vous contacter concernant votre contribution, dans les prochains 15 jours ouvrables. <br><br>
                
                Au cas où vous ne recevrez pas de réponse de notre part dans les prochains 15 jours ouvrables, vous pouvez nous joindre à travers la section <a href=':contact'>Contactez-nous </a> sur le site web.<br>
                Merci pour votre contribution,<br><br>

                
                L'équipe<br><br>",
	     ],
	 'libraryApp'=>[
	     'subject'=>'Votre document est maintenant en ligne',
	     'body'=>"Madame, Monsieur,<br><br>
                Félicitations! <br><br>

                Votre document a été approuvé, et il a été publié avec succès sur le site. Nous sommes convaincus qu’il sera utile pour les membres de notre communauté grandissante. <br><br>
                Vous pouvez le voir ici  (<a href=':link'>:link</a>) . <br><br>
                Nous vous remercions de votre participation, et nous espérons que vous puissiez continuer à contribuer à la plateforme. <br><br>

                
                Cordialement,<br>
                
                L'équipe<br><br>",
	     ],
	 'storyApp'=>[
	     'subject'=>'Votre histoire est maintenant en ligne',
	     'body'=>"Madame, Monsieur,<br><br>
                    Félicitations !<br><br>

                    Votre histoire a été approuvée, et elle a été publiée avec succès sur le site<br><br>
                    Vous pouvez la voir ici (<a href=':link'>:link</a>) . <br><br>
                    Nous vous encourageons à répondre à toute question, réaction ou commentaire que vous recevrez de la communauté, puisque vos idées pourraient faciliter la compréhension de votre point de vue.<br><br>
                    Nous vous remercions de votre participation, et nous espérons que vous puissiez continuer à contribuer à la plateforme <br><br>
                    
            Cordialement, <br>
                    
                   L'équipe<br><br>",
	     ],
	 'eventApp'=>[
	     'subject'=>'Votre événement est maintenant en ligne',
	     'body'=>"Madame, Monsieur ,<br><br>Félicitations! <br><br>

                Votre événement an été approuvé et il a été publié avec succès sur le site.<br><br>
                Vous pouvez le voir ici <a href=':link'>:link</a> . <br><br>
                L'événement est maintenant visible par toute la communauté. <br><br>
                Nous vous remercions de votre contribution, et nous attendons votre retour avec impatience. <br><br>

                
                Cordialement,<br>
                
                L'équipe<br><br>",
	     ],
	   'discussionApp'=>[
	       'subject'=>'Votre discussion ',
	       'body'=>"Madame, Monsieur,<br><br>
                    Félicitations!<br><br>

                   Le thème de discussion ':topic' que vous avez proposé a été approuvé. Veuillez écrire à  <a href='mailto:info.art@undp.org'>info.art@undp.org</a>, et indiquez dans quand vous serez prêts à ouvrir la discussion à notre communauté.<br><br>
                   
                    Comme vous serez en charge en tant que modérateur officiel, nous vous encourageons à surveiller la discussion et à répondre aux commentaires de tous les utilisateurs pour une interaction active et saine.<br><br>
                    Nous vous remercions de votre contribution, et nous attendons votre retour avec impatience.<br><br>

                    
                    Cordialement,<br>
                    
                    L'équipe<br><br>"
	       ],
	   'discussionCom'=>[
	       'subject'=>'Nouveau commentaire dans votre discussion',
	       'body'=>"Monsieur, Madame,<br><br>

                    Un nouveau commentaire a été ajouté à votre discussion '<strong>:topic</strong>' par  '<strong>:user</strong>'<br><br>
                   Commentaire:<br><strong>:comment</strong><br><br>
                   Lien vers la discussion - <br>
                   <a href=':link'>:link</a> <br><br>
                    Nous vous encourageons à surveiller activement la conversation et à répondre aux commentaires de tous les utilisateurs pour une interaction vivante et saine. <br><br>

                    
                    Cordialement,<br>
                    
                    L'équipe<br><br>",
	       ],
	   'storyCom'=>[
	       'subject'=>'Nouveau commentaire dans votre historie',
	       'body'=>"Monsieur, Madame, <br><br>

                    Un nouveau commentaire a été ajouté à votre historie '<strong>:topic</strong>' par  '<strong>:user</strong>'<br><br>
                   Commentaire:<br><strong>:comment</strong><br><br>
                   Lien vers l'histoire - <br>
                   <a href=':link'>:link</a> <br><br>
                    Nous vous encourageons à surveiller activement la conversation et à répondre aux commentaires de tous les utilisateurs pour une interaction vivante et saine. <br><br>

                    
                    Cordialement,<br>
                    
                    L'équipe<br><br>",
	       ],
	  'outlook'=>[
	      'subject'=>'Merci pour vos contribution',
	      'body'=>"Monsieur, madame,<br><br>        
                Merci pour avoir partagé vos idées. Nous trouvons vos commentaires utiles. Nous continuerons à rester en contact pour poursuivre la discussion lorsque l'occasion se présentera.<br><br>

                
                Cordialement,<br>
                 L'équipe<br><br>"
	      ],
	  'feedback'=>[
	      'subject'=>'Vos commentaires techniques',
	      'body'=>"Madame, Monsieur<br><br>

                Nous vous remercions d'avoir partagé vos idées et suggestions sur comme améliorer la plate-forme. Notre équipe technique est en train de les analyser et devrait être en mesure de résoudre le problème assez rapidement. Nous vous écrirons en cas de questions supplémentaires.<br><br>
                Nous vous remercions de nous avoir informés sur la question, et nous nous excusons pour tout inconvénient que cela aurait pu causer.<br><br>
                Nous nous réjouissons de votre participation active à la communauté.<br><br>

                
                Cordialement,<br>
                 L'équipe<br><br>"
	      ],
	      
	      
	      'sign'=>"<span style='color:rgb(127,127,127);font-weight:bold;font-size:12px'>Obtenir de l'aide pour localiser les ODD</span><br>
                <span style='color:rgb(127,127,127);font-size:12px'>Alors que vous ne pouvez pas répondre à ce message, pour toute assistance vous pouvez entrer en contact avec nous à travers la section <a href='".url('/contact-us')."'>Contactez-nous</a></span>",



];
<?php

return[

'type-1'=>"Notes et documents conceptuels",

'type-2'=>"Études de cas et bonnes pratiques",

'type-3'=>"Orientation et capitalisation d’expériences",

'type-4'=>"Outils",

'actor-1'=>"Généraux",

'actor-2'=>"Gouvernements Locaux et Régionaux",

'actor-3'=>"Gouvernements Nationaux",

'actor-4'=>"Organisations de la Société Civile",

'actor-5'=>"Secteur Privé",

'actor-6'=>"Acteurs de l'économie Sociale",

'actor-7'=>"Philanthropie et Fondations",

'actor-8'=>"Universités et Instituts de Recherche",


'region-1'=>"Mondial",

'region-2'=>"Asie",

'region-3'=>"Afrique",

'region-4'=>"Amériques",

'region-5'=>"Europe",

'region-6'=>"Océanie",


'language-1'=>"Anglais",

'language-2'=>"Français",

'language-3'=>"Español",

'language-other'=>"Autres",


'tools-1'=>"Sensibilisation",

'tools-2'=>"Diagnostics",

'tools-3'=>"Stratégies et Plans",

'tools-4'=>"Suivi et Évaluation",

'tools-5'=>"Gouvernance Multi-Niveaux",

'tools-6'=>"Approche Territoriale Multi-Acteurs",

'tools-7'=>"Reddition de Comptes",

'tools-8'=>"Efficacité de la Coopération au Développement",

'tools-9'=>"Renforcement des Capacités",





];
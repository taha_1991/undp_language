<?php

return[
	
	'report-head'=>"<h6>Rapport</h6> <h5>un problème</h5>",

	'inaccurate'=>"Cette information est inexacte.",

	'legitimate'=>"Cette information ne provient pas d’une source légitime.",

	'authority'=>"L’utilisateur n’a pas l’autorité requise pour partager cette information.",

	'Comments'=>"Vos commentaires / suggestions",

	'Policy'=>"J’accepte les Conditions d’utilisation et la Politique de confidentialité",
	
	'thank'=>"Merci d’avoir pris le temps de signaler le problème que vous avez remarqué.",



];



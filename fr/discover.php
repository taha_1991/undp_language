<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'banner'=>'<p>Identifiez les domaines pertinents pour votre territoire, votre communauté, votre agenda politique ou votre projet, et découvrez en quelques clics des outils flexibles que vous pouvez adapter à vos besoins. Différents réglages vous permettent de les ajuster à différents contextes et défis de développement.</p>
    <p>Grâce à ces ressources, vous aurez un accès inégalé à des contenus applicables et générés localement.À vous de découvrir toutes ces possibilités. </p>',
    'discover-head'=>"<h6>DÉCOUVREZ</h6> <h5>LES OUTILS</h5>",
    'initial'=>"Initialiser le processus ODD",
    'raising-awareness'=>"Sensibilisation",
    'diagnostics'=>"Diagnostics",
    'strategies-and-plans'=>"Stratégies et Plans",
    'monitoring-and-evaluation'=>"Suivi et Évaluation",
    'enable'=>"Faciliter les dispositifs institutionnels pour mettre en œuvre les ODD",
    'multilevel-governance'=>"Gouvernance Multi-Niveaux",
    'territorial'=>"Approche Territoriale Multi-Acteurs",
    'accountability'=>"<span class='resFr' style='padding-left: 0px!important'>Responsabilisation</span>",
    'development-cooperation-effectiveness'=>"Efficacité de la Coopération au Développement",
    'capacity-strngthening'=>"Renforcement des Capacités",
    
    'diagnostics-text'=>"<p><strong>À quoi cela sert-il ?</strong> <br>Mettre en place un diagnostic est un moyen de définir une référence qualitative et quantitative de la situation d’un territoire ou d’une question thématique spécifique. Cela met en lumière les défis et opportunités, et constitue la base du processus de mise en œuvre.</p>
    <p><strong>Qu’est-ce que les outils permettent de réaliser ?</strong> <br>Les méthodologies permettront d’identifier les lacunes et les atouts, et elles dresseront la carte des acteurs et des relations avant d’engager les réformes plus avant.</p>",
    
    'explore'=>"Explorez la bibliothèque",

    'raising-awareness-text'=>"<p><strong>À quoi cela sert-il ?</strong> <br>Le plaidoyer ou la sensibilisation constitue la première étape dans la localisation des ODD. L’idée est de communiquer aux responsables locaux l’importance du nouvel agenda et de les informer du rôle critique de leurs décisions dans la réalisation des ODD.</p>
    <p><strong>Qu’est-ce que les outils permettent de réaliser ?</strong> <br>Mener des campagnes spécifiques pour attirer l’attention des gouvernements et acteurs locaux, aux niveaux international, national, régional et local.</p>",

    'strategies-and-plans-text'=>"<p><strong>À quoi cela sert-il ?</strong> <br>Les stratégies et plans peuvent assurer la traduction de l’agenda politique en objectifs de développements et résultats tangibles. Ils fournissent un cadre global pour le développement (utilisation des ressources, services, besoins financiers correspondants, etc.) et visent à coordonner le travail des sphères de gouvernement locales avec les autres.</p>
    <p><strong>Qu’est-ce que les outils permettent de réaliser ?</strong> <br>La localisation des ODD peut fournir un cadre pour la politique de développement local. L’intégration des ODD au sein de la planification régionale et locale constitue une étape cruciale dans la réussite du nouvel agenda dans les régions et les villes.</p>",

    'monitoring-and-evaluation-text'=>'<p><strong>À quoi cela sert-il ?</strong> <br>Les ODD feront l’objet d’un suivi et d’une évaluation à travers un système de 231 indicateurs, dont un grand nombre peut être localisé par la collecte de données au niveau du territoire. Même si le suivi des progrès et l’évaluation des résultats se fait au niveau des programmes nationaux, les données régionales et locales doivent être prises en compte.</p>
    <p><strong>Qu’est-ce que les outils permettent de réaliser ?</strong> <br>La localisation du suivi de l’Agenda 2030, à travers la promotion de la participation des gouvernements locaux et régionaux au suivi national et à travers l’adaptation des indicateurs nationaux aux contextes locaux et régionaux.</p>',

    'multilevel-governance-text'=>"<p><strong>À quoi cela sert-il ?</strong> <br>L’Agenda 2030 pour le développement pousse à aller au-delà du statu quo en matière de gouvernance afin d’atteindre les ODD. La mise en œuvre des ODD repose sur une gouvernance multi-niveaux adéquate. L’idée consiste à examiner les structures de gouvernance et les mécanismes de coopération inter-institutions pour établir des cadres permettant la réalisation des ODD.</p>
    <p><strong>Qu’est-ce que les outils permettent de réaliser ?</strong> <br>Les outils proposés ici encouragent la cohérence des politiques verticales et les mécanismes de collaboration entre les couches de gouvernance locales, nationales et internationales. Cette vision commune devrait renforcer la conception, la planification et la mise en œuvre des politiques. Les outils favorisent également l’inclusion de groupes sociaux marginalisés et ils promeuvent des dispositifs institutionnels spécifiques pour les zones complexes. Ils se décomposent suivant les domaines d’intérêt et des aspects spécifiques de la localisation des ODD (par exemple les politiques de développement économique locales, les processus de planification des politiques publiques).</p>",

    'territorial-text'=>"<p><strong>À quoi cela sert-il ?</strong> <br>L’approche territoriale est un cadre politique visant à faciliter la mise en œuvre efficace des ODD. Ces outils encouragent une large participation en facilitant le dialogue entre le centre et les périphéries, et parmi les membres de la société civile et le secteur privé. L’approche améliore également la façon dont les institutions locales communiquent et interagissent entre elles, particulièrement en intégrant à l’élaboration des politiques leurs connaissances spécifiques et savoir-faire pratiques. Cela permet une réponse inclusive aux défis du développement pour un territoire donné.</p>
    <p><strong>Qu’est-ce que les outils permettent de réaliser ?</strong> <br>Les outils aident à mettre en relation et coordonner les différents acteurs d’un territoire donné. Ils encouragent des partenariats multi-acteurs pertinents intégrant le gouvernement, la société civile et le secteur privé, tout en soulignant le besoin d’interactions et de coordination à travers la société.</p>",

    'accountability-text'=>"<p><strong>À quoi cela sert-il ? </strong> <br>Au sein des territoires respectifs, des systèmes de « responsabilisation vis-à-vis des résultats et du changement constructif » permettent aux acteurs de suivre les progrès réalisés vers des objectifs établis, d’étudier les obstacles à la mise en œuvre, et de proposer des changements et des actions de remédiation à ces politiques.</p>
    <p><strong>Qu’est-ce que les outils permettent de réaliser ?</strong> <br>L’étude de la gestion locale des données (y compris les indicateurs), du suivi local et des mécanismes locaux spécifiques permettra d’améliorer la transparence, l’accès à l’information, la sensibilisation et l’appropriation de l’Agenda 2030. Cela déplace la boucle de responsabilisation au plus près des populations et cela renforce leurs voix.</p>",

    'development-cooperation-effectiveness-text'=>"<p><strong>À quoi cela sert-il ?</strong> <br>La mise en œuvre au niveau local des principes d’efficacité de la coopération au développement signifie faciliter l’alignement et l’harmonisation des acteurs du développement. Cela renforce les résultats du développement là où cela importe le plus – et là où cela impacte la vie des gens. Les pratiques de coopération au développement au niveau local, dans le cadre de la mise en œuvre des ODD, peuvent nourrir l’agenda global. Elles permettent de mettre en application les enseignements et, ainsi, d’obtenir les preuves nécessaires en faveur de politiques plus efficaces.</p>
    <p><strong>Qu’est-ce que les outils permettent de réaliser ?</strong> <br>Utiliser les cadres de résultats de coopération au niveau local évite la prolifération d’initiatives de développement isolées et améliore l’alignement, l’harmonisation, la responsabilisation et la transparence.</p>",

    'capacity-strengthening-text'=>"<p><strong>À quoi cela sert-il ?</strong> <br>Les gouvernements locaux et régionaux, ainsi que d’autres acteurs locaux, ont un rôle crucial à jouer dans la mise en œuvre et le suivi des ODD. Cette section vise à fournir des outils soutenant la création et le renforcement de leurs capacités en matière de développement et de réalisation de l’Agenda 2030.</p>
    <p><strong>Qu’est-ce que les outils permettent de réaliser ?</strong> <br>Le développement des capacités étant considéré comme le moteur des processus de développement, ces outils fournissent un guide pour permettre aux individus, organisations et sociétés d’obtenir, de renforcer et de maintenir leurs capacités afin d’établir et de réaliser leurs propres objectifs de développement en cohérence avec les ODD.</p>",
    'small'=>"Cette initiative est en pleine évolution, et des acteurs comme vous partagent ces ressources. Vous trouverez ici les outils recueillis jusqu'à présent. ",
];

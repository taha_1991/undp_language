<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'search-head'=>"<h6>Rechercher</h6> <h5>dans la bibliothèque</h5>",
	'by-type'=>"Par type",
	'by-actor'=>"Par acteur",
	'by-region'=>"Par région",
	'by-language'=>"Par langue",
	'by-tool'=>"Par Outils",
	'search-text'=>"Cette initiative est en pleine évolution, et des acteurs comme vous partagent ces ressources. Vous trouverez ici les documents et les outils recueillis jusqu'à présent.",
	'background'=>"Contexte: Agenda 2030",
	'initial'=>"INITIALISER LE PROCESSUS ODD",
	'enable'=>"FACILITER LES DISPOSITIFS INSTITUTIONNELS POUR METTRE EN ŒUVRE LES ODD",
	'capacity'=>"RENFORCEMENT DES CAPACITÉS",
	'recent'=>"<h6>Contenus</h6>
                <h5>récents</h5>",
	'ad-head'=>"Vous avez des documents ou des outils que vous souhaiteriez partager ?",
	'ad-text'=>"Nous serions ravis de les recevoir !",
	'ad-btn'=>"Contribuer",
	//search-results
	'search-results'=>"<h6>RÉSULTATS </h6>
			<h5>DE RECHERCHER:</h5>",
	'concept'=>"Notes et documents conceptuels",
	'case'=>'Études de cas et bonnes pratiques',
    'guidance'=>"Orientation et capitalisation d’expériences",
    'tools'=>"Outils",
    //library-inner
    'info'=>"Infos",
    'share'=>"Partager",
    'publish'=>'Vous aussi, vous pouvez publier !',
    'available'=>"Disponible en Anglais, Français et Espagnol",
    'downlaod'=>"Télécharger le PDF",
    'favorites'=>"Ajouter aux favoris",
    'report'=>"Signaler un problème",
    'related'=>"<h6>Publications</h6> <h5>connexes</h5> ",
    'report-text'=>"* Nous avons examiné la qualité des documents soumis. Cela dit, nous ne pouvons être tenus responsables pour des possibles relatifs aux documents. Si vous avez des commentaires ou questions concernant les contenus, s'il vous plaît veuillez les signaler para email, et nous allons examiner les questions.",
    
];

<?php

return[


'create-head'=>"<h6>CRÉEZ</h6><h5>VOTRE COMPTE</h5>",

'create-text'=>"Créez un compte et obtenez un accès complet ! Une fois inscrit, vous pourrez partager vos histoires, participer aux discussions, publier des événements, envoyer des documents dans la bibliothèque et établir des contacts avec les autres membres de cette communauté.",

'email'=>"Adresse e-mail",

'retype-email'=>"Retaper Adresse e-mail",

'confirmation'=>"Une confirmation par e-mail sera envoyée à cette adresse. Votre e-mail sera votre connexion. Cet email sera aussi votre nom d’utilisateur. ",

'pswd'=>"Mot de passe", 

'retype'=>"Vérification du mot de passe",

'pswd-must'=>"Les mots de passe doivent",

'pswd-must-one'=>"Comporter au moins 8 caractères.",

'pswd-must-two'=>"Inclure une combinaison de lettres et de chiffres.",

'pswd-must-three'=>"Ne comporter aucun caractère spécial.",

'personal'=>"Profil personnel",

'personal-name'=>"Prénom",

'personal-last'=>"Nom",

'personal-organ'=>"Organisation",

'country'=>"Pays",

'province'=>"Province / État",

'city'=>"Ville",

<<<<<<< HEAD
'profile-photo'=>"Image de profil",

'choose-file'=>"<strong>Sélectionnez un fichier</strong>",

'images-should'=>"Les images doivent être au format PDF et ne pas dépasser 2Mo",
=======
	'profile-photo'=>"Image de profil",

	'choose-file'=>"<strong>Sélectionnez un fichier<strong>",

	'images-should'=>"Les images doivent être au format JPEG / PNG et ne pas dépasser 2Mo",
>>>>>>> origin/undp_abhijit

'promotional-head'=>"<h4>Information promotionnelle</h4>",

'promotional-text'=>"S'il vous plaît envoyez-moi des mises à jour mensuelles sur le nouveau contenu sur le site.",

'verify'=>"Vérifier l’inscription",

'verify-text'=>"Veuillez entrer le texte que vous voyez ici:",

'agree'=>"Je suis d'accord avec les <a href=':terms' target='_blank'>conditions d'utilisation</a> et la <a target='_blank' href=':privacy'>politique de confidentialité</a>.",

'thankyou'=>"MERCI D’AVOIR REJOINT CETTE COMMUNAUTÉ DYNAMIQUE ! NOUS SOMMES HEUREUX DE VOUS ACCUEILLIR COMME MEMBRE ACTIF.",

'thankyou-text'=>"<p>Nous vous avons envoyé un e-mail de confirmation comportant un lien pour activer votre compte. Veuillez vérifier votre courrier et cliquer sur ce lien.</p><p>Si vous n’avez pas reçu notre e-mail, veuillez vérifier votre dossier de courriers indésirables / spam. Si vous n’arrivez toujours pas à le trouver, cliquez ici : </p>",

'resend'=>"Renvoyer l’e-mail",

'active'=>"Votre compte est activé!",

'update'=>"Mettez à jour votre <a href=':link'>page de Profil</a>, de façon à ce que les membres de cette communauté puissent prendre des décisions éclairées concernant des collaborations potentielles. Votre profil donne une idée de qui vous êtes et il contribuera à susciter des interactions pertinentes.",

'continue'=>"Poursuivre l’exploration",
'org'=>'e.g. Programme des Nations Unies pour le développement (PNUD)',


];
<?php

return[
	
	'share-story-head'=>"<h6>PARTAGEZ VOTRE </h6><h5>HISTOIRE</h5>",

	'share-story-text'=>"<p>Vous avez peut-être une histoire fascinante à raconter concernant les ODD au niveau local. Qu’il s’agisse d’un cheminement personnel de développement local, d’une nouvelle initiative que vous trouvez excitante, des défis auxquels vous faites face ou de la manière dont vous générez un impact positif, nous serions ravis de vous mettre en relation avec cette communauté.</p><p>Bien entendu, vous êtes également invité à évaluer votre expérience au moyen des outils proposés sur cette plate-forme. Cela se traduira par une expérience plus fiable et cela aidera d’autres membres à prendre des décisions éclairées. Nous voulons que nos utilisateurs puissent facilement se procurer et accéder à des contenus concernant toutes les ressources qu’ils trouvent ici.</p><p>Veuillez noter que nous pouvons être amenés à éditer les histoires et leur titre pour des questions de longueur, de clarté et de choix éditoriaux.</p>",

	'title'=>"Titre de l’histoire",

	'story'=>"Histoire",

	'cover'=>"Illustration de couverture",

	'country'=>"Pays",
	
	'select'=>"Choisissez le pays",

	'province'=>"Province / État",

	'select-province'=>"Choisissez la province / l’état",

	'city'=>"Ville",

	'select-city'=>"Choisissez la ville",

	'publications'=>"J’autorise cette initiative à citer mon histoire dans ses campagnes et publications.",

	'mail'=>"M’envoyer une copie par e-mail.",

	'thank'=>"MERCI DE VOTRE PARTAGE !",

	'approval'=>"Votre histoire apparaîtra en ligne après approbation.",

	'notification'=>"Vous en serez notifié.",

	'stories'=>"D’ici là, vous pouvez consulter les autres <a href=':link'><strong>Histoires</strong></a>",

	'Choose-file'=>"<strong>Sélectionnez un fichier</strong>",

	'image-should'=>"Les images doivent être au format JPEG / PNG et ne pas dépasser 2Mo",



];
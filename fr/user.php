<?php

return[

	'messages'=>"Messages",

	'favourites'=>"Favoris",

	'edit'=>"Modifier mon profil",

	'change'=>"Changer mon mot de passe",

	'log'=>"Déconnexion",

	'organ'=>"Organisation",

	'location'=>"Lieu",

	'total'=>"Messages publiés",

	'about'=>"À propos de moi",

	'documents'=>"Documents",

	'stories'=>"Histoires",

	'discussions'=>"Discussions",

	'events'=>"Événements",

	'send'=>"Envoyer un Message",

	'profile-photo'=>"Image de profil",

	'choose-file'=>"<strong>Sélectionnez un fichier</strong>",

	'image-shuld'=>"Les images doivent être au format JPEG / PNG et ne pas dépasser 2Mo",

	'coming'=>'Arrive bientôt',


];

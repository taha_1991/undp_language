<?php

return[
	
	'lead-head'=>"<h6>ANIMER</h6><h5>UNE DISCUSSION</h5>",

	'lead-text'=>"Démarrez des conversations sur les questions et idées pour obtenir des perspectives diverses sur la réalisation des ODD au niveau local. Proposez votre sujet, et nous sélectionnerons peut-être votre organisation pour modérer officiellement notre prochaine discussion !",

	'share'=>"Partagez votre question",

	'intro'=>"Introduction à la discussion",

	'file'=>"Fichier",

	'edit'=>"Édition",
	
	'insert'=>"Insertion",

	'view'=>"Affichage",

	'format'=>"Format",

	'table'=>"Tableau",

	'tools'=>"Outils",

	'upload'=>"Télécharger une image / un logo",

	'choose'=>"<p><strong>Sélectionnez un fichier ou déposez-le ici</strong></p> <br/><p>Les images doivent être au format PDF et ne pas dépasser 2Mo.</p>",

	'suggest'=>"Proposer un sujet",

	'thank'=>"MERCI D’AVOIR EXPRIMÉ VOTRE INTÉRÊT POUR MODÉRER UNE DISCUSSION.",

	'aim'=>"Notre objectif est de vous satisfaire !",

	'notify'=>"Nous vous informerons si la discussion que vous avez proposée est lancée.",

	'until'=>"D’ici là, vous pouvez consulter les <a href=':link'><strong>Discussions récentes</strong></a>",


];
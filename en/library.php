<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'search-head'=>'<h6>Search</h6> <h5>Library</h5>',
	'by-type'=>'By Type',
	'by-actor'=>'By Actor',
	'by-region'=>'By Region',
	'by-language'=>'By Language',
	'by-tool'=>'By Tools',
	'search-text'=>'This initiative is evolving, and actors like you are sharing these resources. You will find here the documents and tools collected so far.',
	'background'=>'Background and context: Agenda 2030',
	'initial'=>'INITIALIZING THE SDGs PROCESS',
	'enable'=>'ENABLING INSTITUTIONAL ARRANGEMENTS FOR SDGs’ IMPLEMENTATION',
	'capacity'=>'CAPACITY STRENGTHENING',
	'recent'=>'<h6>Recent </h6>
			<h5>Content</h5>',
	'ad-head'=>"Have any document or tool that you'd like to share? ",
	'ad-text'=>"We'd love to have it!",
	'ad-btn'=>"Make a contribution ",
	//search-results
	'search-results'=>'<h6>Search </h6>
			<h5>Results For:</h5>',
	'concept'=>'Concept notes and papers',
	'case'=>'Case studies and best practices',
    'guidance'=>'Guidance and systemization of experiences',
    'tools'=>'Tools',
    //library-inner
    'info'=>'Info',
    'share'=>'Share',
    'publish'=>'You can Publish too!',
    'available'=>'Available in ',
    'downlaod'=>'Download PDF',
    'favorites'=>'Add to Favorites',
    'report'=>'Report a Problem',
    'related'=>'<h6>Related </h6>
			<h5>Publications</h5> ',
    'report-text'=>"*Users are expected to adhere to the <a href='".url('/terms')."'>Terms and Conditions</a> of this website.
 ",
    
];

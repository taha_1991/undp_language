<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'banner'=>"<p>Identify areas relevant to your territory, community, political agenda or project and click to find flexible tools you may tailor to your needs. They can be adjusted to various settings, adaptable to different contexts and development challenges.</p>
    <p>Using these resources, you will have unparalleled access to applicable and locally generated content. All available for you to discover.</p>",
    'discover-head'=>"<h6>Discover</h6>	<h5>Tools</h5>",
    'initial'=>"Initializing the SDGs process",
    'raising-awareness'=>"Raising Awareness",
    'diagnostics'=>"Diagnostics",
    'strategies-and-plans'=>"Strategies and Plans",
    'monitoring-and-evaluation'=>"Monitoring and Evaluation",
    'enable'=>"Enabling institutional arrangements for SDGs implementation",
    'multilevel-governance'=>"Multilevel Governance",
    'territorial'=>"Territorial /<br> Multi Stakeholder Approach",
    'accountability'=>"Accountability",
    'development-cooperation-effectiveness'=>"Development Cooperation Effectiveness",
    'capacity-strengthening'=>"Capacity Strengthening",

    'diagnostics-text'=>"<p><strong>What is it for?</strong> <br>Setting up a diagnostic is a way to set a qualitative and quantitative baseline of the situation of a territory, or a specific thematic issue. It highlights the challenges and opportunities, and it constitutes the basis of the implementation process.</p>
    <p><strong>What do the tools help achieve?</strong><br>The methodologies will help identify gaps and assets. They will map stakeholders and relationships before engaging further into reforms.</p>",

    'explore'=>"Explore the Library",

    'raising-awareness-text'=>"<p><strong>What is it for?</strong> <br>Advocacy or raising awareness is the first step to start localizing the SDGs. The idea is to communicate to local office bearers the importance of the new agenda and apprise them of the critical role of their decisions in achieving the SDGs.</p>
    <p><strong>What do the tools help achieve?</strong> <br>Specifically lobby for governments’ and local actors’ attention at international, national, regional and local levels.</p>",

    'strategies-and-plans-text'=>"<p><strong>What are these for?</strong> <br>Strategies and plans can assure the translation of the political agenda into development objectives and tangible results. They give an overall framework for development (use of resources, services, corresponding financial needs, etc.) and aim to coordinate the work of local and other spheres of governments.</p>
    <p><strong>What do the tools help achieve?</strong> <br>Localizing the SDGs can provide a framework for local development policy. The integration of SDGs within sub-national level planning is a crucial step in landing the new agenda in regions and cities.</p>",

    'monitoring-and-evaluation-text'=>"<p><strong>What is it for?</strong> <br>The SDGs will be monitored and assessed through a system of 231 indicators, many of which can be localized by gathering data at the territorial level. While monitoring progress and reviewing the results of national plans, subnational data needs to be taken into account.</p>
    <p><strong>What do the tools help achieve?</strong> <br>Localizing the follow-up of the 2030 Agenda, by promoting the participation of local and regional governments in national monitoring, and by adapting national indicators to local and regional contexts.</p>",

    'multilevel-governance-text'=>"<p><strong>What is it for?</strong> <br>The 2030 Agenda for Development pushes for going beyond “governance as usual” in order to reach the SDGs.Implementation of the SDGs lies on adequate multi-level governance. The idea is to look at government structures and inter-institutional cooperation mechanisms to enable frameworks for SDG achievement.</p>
    <p><strong>What do the tools help achieve?</strong> <br>The tools offered here support vertical policy coherence and collaboration mechanisms between local, national and international layers of governance. This shared vision should strengthen policy design, planning and implementation. The tools also promote the inclusion of marginalized societal groups, and they promote specific institution set-ups for complex areas. They will break down to areas of interest and relevance to SDGs’ localization (i.e. local economic development policies, planning public policy processes).</p>",

    'territorial-text'=>"<p><strong>What is it for?</strong> <br>The territorial approach is a policy framework that can facilitate the effective implementation of the SDGs. These tools foster broad based participation, by enabling dialogues between core and peripheries, among members of civil society and the private sector. The approach also improves how local institutions relate and interact with each other, especially by incorporating their specific knowledge and practical know-how within policymaking. This allows for an inclusive response to development challenges in a given territory.</p>
    <p><strong>What do the tools help achieve?</strong> <br>The tools help bind and articulate various actors in a given territory. They further meaningful multi-stakeholder partnerships comprising the government, civil society and the private sector, stressing the need for interaction and coordination across society.</p>",

    'accountability-text'=>"<p><strong>What is it for?</strong> <br>Within the respective territories, systems of “accountability for results and constructive change” allow actors to: monitor progress towards agreed objectives, examine obstacles to implementation and suggest changes and remedy actions to those policies.</p>
    <p><strong>What do the tools help achieve?</strong> <br>Looking at local data management (including the indicators), local monitoring and specific local mechanisms will allow the increase of transparency, access to information, awareness and ownership of the 2030 Agenda. It moves accountability loops closer to the people and strengthens their voice.</p>",

    'development-cooperation-effectiveness-text'=>"<p><strong>What is it for?</strong> <br>Implementing the principles of development cooperation effectiveness at the local level means to facilitate the alignment and harmonization of development actors. This strengthens development results where it matters most – and where it impacts people’s lives. Local level development cooperation practices to implement the SDGs may feed back into the global agenda. They can allow the application of lessons, providing much-needed evidence for more effective policies.</p>
    <p><strong>What do the tools help achieve?</strong> <br>Using cooperation results frameworks at the local level prevents proliferation of isolated development initiatives and increases alignment, harmonization, accountability and transparency.</p>",

    'capacity-strengthening-text'=>"<p><strong>What is it for?</strong> <br>Local and regional governments, along with other local stakeholders, have a crucial role to play in the implementation and monitoring of the SDGs. This section is meant to provide tools that support their capacity building and strengthening for development and for the Agenda 2030’s landing.</p>
    <p><strong>What do the tools help achieve?</strong> <br>Capacity development is considered to be the engine of development processes. These tools provide guidance through which individuals, organizations and societies obtain, strengthen and maintain the capabilities to set and achieve their own development objectives over time, in coherence with the SDGs.</p>",
    'small'=>'This initiative is evolving, and actors like you are sharing these resources. You will find here the tools collected so far.',

];
<?php

return[

	'practical-head'=>"<h2>do you need practical methods to achieve sustainable development at the local level?</h2>",

	'practical-text'=>"Localizing the SDGs has resources and features that support you.",

	'log-in-head'=>"<h3>log in to your account</h3>",
	'email'=>"Email Address",
	'pswd'=>"Password",
	'remember'=>"Remember Me",
	'forgot'=>"Forgot Password?",
	'login-btn'=>"Login",
	'account'=>"Create Your Account",


];
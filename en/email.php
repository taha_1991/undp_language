<?php

return[

	'register'=>[
	    'subject'=>'Confirm your registration',
	    'body'=>'Dear User,<br><br>Thank you for registering to join the Localizing the SDGs community.<br>
        <br>
        You are one step away from gaining access to powerful knowledge, tools, resources, stories, discussions and events. These can support you as you put the Sustainable Development Goals into action.<br>
        <br>
        <br>
        Click on the following confirmation link and get started. In case you are not able to click the link directly, please copy and paste it into your browser’s address bar.
        <br><br>
        <a href=":link">:link</a>
        <br><br>
        <br><br>
        For any support or suggestions, please reach out to us via the <a href=":contact">Contact Us</a> section, and we will respond to you shortly.
        <br><br>
        Kind regards,<br>
        The Team<br><br>',
	    ],
	'newUser'=>[
	    'subject'=>'Welcome to the Localizing the SDGs community',
	    "body"=>"
	        Dear User,<br><br>
            Welcome to the Localizing the SDGs community.<br><br>
            
            Your account is now active, and we encourage you to update your profile. A complete profile helps people get a sense of who you are, and it helps them interact with you better.<br><br>
            
            We look forward to your participation and contribution! <br><br>
            
            For any support or suggestions, please reach out to us via the <a href=':contact'>Contact Us</a> section, and we will respond to you shortly. <br><br>
            
            Best regards,<br>
            
            The Team<br><br>
	    ",
	    ],
	'forgot'=>[
	    'subject'=>"Reset your password",
	    'body'=>"Dear User, <br><br>
                It's easy to reset your password.<br>
                <br>
                Please click on the following link and fill in the details requested. If you are unable to click on the link directly, you can also copy and paste it in your browser’s address bar.<br>
                <br>
                <br>
                <a href=':link'>:link</a>
                <br>
                We look forward to having you back. <br><br>
                Have a great day! <br>
                The Team<br><br>",
	    ],
	 'library'=>[
	     "subject"=>"Your document has been uploaded",
	     "body"=>"Dear User,<br><br>
                Congratulations! <br><br>

                Your document has been successfully uploaded and is being reviewed. You will soon hear back from our team regarding the status of your submission - within the next 15 working days. <br><br>
                
                In case you do not hear back from us within the next 15 working days, you can reach us via the <a href=':contact'>Contact Us</a> section on the website.  <br><br>

                
                Best regards,<br>
                
                The Team<br><br>",
	     ],
	 'story'=>[
	     "subject"=>"Your story has been submitted",
	     "body"=>"Dear User,<br><br>
                Congratulations! <br><br>

                Your story has been successfully submitted and is being reviewed. You will soon hear back from our team regarding the status of your submission - within the next 15 working days. <br><br>
                
                In case you do not hear back from us within the next 15 working days, you can reach us via the <a href=':contact'>Contact Us</a> section on the website.  <br>
                Thanks again for sharing your story with the community.<br><br>

                
                Best regards,<br>
                
                The Team<br><br>",
	     ],
	 'event'=>[
	     "subject"=>"Your event has been submitted",
	     "body"=>"Dear User,<br><br>
                Congratulations! <br><br>

                Your event has been successfully submitted and is being reviewed. You will soon hear back from our team regarding the status of your submission - within the next 15 working days. <br><br>
                
                In case you do not hear back from us within the next 15 working days, you can reach us via the <a href=':contact'>Contact Us</a> section on the website.  <br>
                Thanks again for sharing your event with the community.<br><br>

                
                Best regards,<br>
                
                The Team<br><br>",
	     ],
	 'discussion'=>[
	     "subject"=>"Your proposal of a discussion topic",
	     "body"=>"Dear User,<br><br>
                Congratulations! <br><br>

                Your discussion topic has been added and is under consideration. <br><br>
                You will soon hear back from our team regarding the status of your submission- within the next 15 working days. <br><br>
                
                In case you do not hear back from us within the next 15 working days, you can reach us via the <a href=':contact'>Contact Us</a> section on the website.  <br>
                Thanks for your contribution.<br><br>

                
                Best regards,<br>
                
                The Team<br><br>",
	     ],
	 'libraryApp'=>[
	     'subject'=>'Your document is now online',
	     'body'=>"Dear User,<br><br>
                Congratulations! <br><br>

                Your document has been approved, and it has been successfully published on the site. We are confident that it will be helpful to the members of our growing community. <br><br>
                You can view it here (<a href=':link'>:link</a>) . <br><br>
                We thank you for your contribution, and we look forward to your further engagement. <br><br>

                
                Best regards,<br>
                
                The Team<br><br>",
	     ],
	 'storyApp'=>[
	     'subject'=>'Your story is online',
	     'body'=>"Dear User,<br><br>
                    Congratulations! <br><br>

                    Your story has been approved, and it has been successfully published on the site. <br><br>
                    You can view it here (<a href=':link'>:link</a>) . <br><br>
                    We encourage you to respond to any queries, reactions or comments you receive from our community, as your insights might help them understand your views better. <br><br>
                    We thank you for your contribution, and we look forward to your further engagement. <br><br>

                    
                    Best regards,<br>
                    
                    The Team<br><br>",
	     ],
	 'eventApp'=>[
	     'subject'=>'Your event is now online',
	     'body'=>"Dear User<br><br>Congratulations! <br><br>

                Your event has been approved and it has been successfully published on the site.  <br><br>
                You can view it here<a href=':link'>:link</a> . <br><br>
                The event is visible to the whole community. <br><br>
                We thank you for your contribution, and we look forward to your further engagement. <br><br>

                
                Best regards,<br>
                
                The Team<br><br>",
	     ],
	   'discussionApp'=>[
	       'subject'=>'Your suggested discussion',
	       'body'=>"Dear User,<br><br>
                    Congratulations! <br><br>

                    Your suggested topic for discussion ':topic' has been approved. Kindly write in to <a href='mailto:info.art@undp.org'>info.art@undp.org</a>, and indicate when you will be prepared to open it to our community.  <br><br>
                   
                    As you will be featured as an official moderator, we encourage you to actively monitor and respond to comments made by all users for a lively and wholesome interaction.<br><br>
                    We thank you for your contribution, and we look forward to your further engagement. <br><br>

                    
                    Best regards,<br>
                    
                    The Team<br><br>"
	       ],
	   'discussionCom'=>[
	       'subject'=>'New comment on your discussion ',
	       'body'=>"Dear User,<br><br>

                    A new comment has been added to your discussion '<strong>:topic</strong>' by '<strong>:user</strong>'<br><br>
                   Comment:<br><strong>:comment</strong><br><br>
                   Link to the discussion - <br>
                   <a href=':link'>:link</a> <br><br>
                    We encourage you to actively monitor and respond to comments by all users for a lively and wholesome interaction. <br><br>

                    
                    Warm regards,<br>
                    
                    The Team<br><br>",
	       ],
	   'storyCom'=>[
	       'subject'=>'New comment on your story ',
	       'body'=>"Dear User, <br><br>

                    A new comment has been added to your story '<strong>:topic</strong>' by '<strong>:user</strong>'<br><br>
                   Comment:<br><strong>:comment</strong><br><br>
                   Link to the story - <br>
                   <a href=':link'>:link</a> <br><br>
                    We encourage you to actively monitor and respond to comments by all users for a lively and wholesome interaction. <br><br>

                    
                    Warm regards,<br>
                    
                    The Team<br><br>",
	       ],
	  'outlook'=>[
	      'subject'=>'Thanks for your inputs',
	      'body'=>"Dear User,<br><br>        
                Thank you for sharing your thoughts. We find your comments helpful, and we will be in touch for further discussion when the opportunity arises.  <br><br>
                We’d be glad to hear more from you. <br><br>

                
                Best regards,<br>
                
                The Team<br><br>"
	      ],
	      
	  'feedback'=>[
	      'subject'=>'Your technical feedback',
	      'body'=>"Dear User,<br><br>

                We thank you for sharing your concern and for suggesting how to improve the platform. Our technical team is looking into it and should be able to resolve the issue pretty quickly. We will write to you in case of any further questions.    <br><br>
                Thank you for alerting us about the issue, and we apologize for any inconvenience that this might have caused.<br><br>
                We look forward to your active participation in the community. <br><br>

                
                Best regards,<br>
                
                The Team<br><br>"
	      ],
	      
	      'sign'=>"<span style='color:rgb(127,127,127);font-weight:bold;font-size:12px'>Getting help for Localizing the SDGs</span><br>
                <span style='color:rgb(127,127,127);font-size:12px'>While you cannot reply to this email, you can get in touch with us through our <a href='".url('/contact-us')."'>Contact Us</a> section for assistance.</span>",



];
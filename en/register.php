<?php

return[


'create-head'=>"<h6>Create</h6> <h5>Your account</h5>",

'create-text'=>"Create an account, and gain complete access! As soon as you sign up, you may share stories, participate in discussions, post events, upload documents to the library & get connected with other members of this community.",

'email'=>"Email Address",

'retype-email'=>"Retype Email Address",

'confirmation'=>"A one-time confirmation e-mail will be sent to this address. Your e-mail will serve as your login.",

'pswd'=>"Password",

'retype'=>"Retype Password",

'pswd-must'=>"Les mots de passe doivent:",

'pswd-must-one'=>"comporter au moins 8 caractères.",

'pswd-must-one'=>"Include at least 8 characters.",

'pswd-must-two'=>"inclure une combinaison de lettres et de chiffres.",

'pswd-must-three'=>"Do not include any special characters.",

'personal'=>"Personal Profile",

'personal-name'=>"First Name",

'personal-last'=>"Last Name",

'personal-organ'=>"Organization",

'country'=>"Country",

'province'=>"Province / State",

'city'=>"City",

	'profile-photo'=>"Profile Photo",

	'choose-file'=>"<strong>Choose a file</strong>",

	'images-should'=>"Image should only be JPEG / PNG format and should not exceed 2Mb",

'promotional-head'=>"<h4>Promotional Information</h4>",

'promotional-text'=>"Please send me monthly updates about new content on website.",

'verify'=>"Verify registration",

'verify-text'=>"Enter the text you see here:",

'agree'=>"I agree with the <a href=':terms' target='_blank'>Terms and Conditions</a> and <a target='_blank' href=':privacy'>Privacy Policy</a>",

'thankyou'=>"THANK YOU FOR BECOMING A PART OF THIS VIBRANT COMMUNITY! WE WELCOME YOU AS AN ACTIVE PARTICIPANT.",

'thankyou-text'=>"<p>We sent you a confirmation email with a link to activate your account. Please check your email and click on the link.</p>
<p>If you have not received an email, please check your junk / spam folder. If you still have trouble finding it, click here: </p>",

'resend'=>"Resend Email",

'active'=>"Your account is active!",

'update'=>"Update your <a href=':link'>Profile Page</a> so members of this community can make informed decisions about potential collaboration. Your profile conveys a sense of who you are, and it helps to power relevant interactions. ",

'continue'=>"Continue Exploring",
'org'=>'e.g. United Nations Development Programme (UNDP)',


];
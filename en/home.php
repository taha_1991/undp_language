<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'banner-head'=>'toolbox for localizing the <br><span id="goals">sustainable development goals</span>',
    'banner-main-text'=>'A tailor-made goldmine of practical content awaits for you to use',
    'banner-text'=>'Discover powerful tools and resources, together with the real experiences and opinions of many development actors',
    'banner-sign-up'=>'Sign Up Here To Join Our Community',
    'banner-video'=>'Video',
    'why'=>'<h6>why should</h6> <h5>you be here?</h5>',
    'discover'=>'Discover Tools',
    'discover-text'=>'Across the world, governments and organizations are working within their specific local realities to drive sustainable progress. There is a lot of powerful learning that we have condensed here into practical, flexible tools & guides for you to use.',
    'contribute'=>'Contribute Documents',
    'contribute-text'=>'Have you developed relevant insights that might be helpful to other local actors? Add to our library and get featured! We seek to build an online repository of development solutions and practices.',
    'story'=>'Share Stories',
    'story-text'=>'Stories have the power to connect us with the common threads and journeys of others. A good story is also a strong currency for interactions. Share your story and establish a deeper relationship with our community.',
    'discuss'=>'Discuss',
    'discuss-text'=>'Open up conversations on pertinent ‘localizing’ issues and ideas, and seek multiple perspectives. You may suggest your topic, and we might handpick your organization to moderate our next discussion!',
    'event'=>'Post Events',
    'event-text'=>'Promote your events with a listing on our calendar. We feature events that aim to localize the SDGs worldwide.',
    
    'latest-head'=>'<h6>latest</h6><h5>updates</h5>',
    'twitter'=>'<h6>Join the</h6><h5>conversation</h5>',
    
    'local'=>'<h6>Our Local</h6><h5>contributors</h5>',
    'local-text'=>'See how our contributors<br />are spread out globally',
    

];

<?php

return[
	
	'lead-head'=>"<h6>Lead</h6><h5>a discussion</h5>",

	'lead-text'=>"Open up conversations on pertinent issues and ideas, seeking multiple perspectives on how to achieve the SDGs at the local level. You may suggest your topic, and we might handpick your organization to officially moderate our next discussion!",

	'share'=>"Share Your Question",

	'intro'=>"Introduction to Discussion",

	'file'=>"File ",

	'edit'=>"Edit",
	
	'insert'=>"Insert",

	'view'=>"View",

	'format'=>"Format",

	'table'=>"Table",
	
	'tools'=>"Tools ",

	'upload'=>"Upload Picture / Logo",

	'choose'=>"<p><strong>Choose a file</strong></p> <br/><p>Image should be in JPEG / PNG format and should not exceed 2Mb.</p>",

	'suggest'=>"Suggest a Topic",

	'thank'=>"THANK YOU FOR CONVEYING YOUR INTEREST IN MODERATING A DISCUSSION. ",

	'aim'=>"We aim to please!",

	'notify'=>"We will notify you if the discussion you suggest goes live. ",

	'until'=>"Until then, check out the <a href=':link'><strong>Latest Discussions</strong></a>",


];
<?php

return[


	'share-head'=>"<h6>SHARE </h6><h5>YOUR OUTLOOK</h5>",
 
	'outlook-text'=>"<p>Make a suggestion or express your wishes for the future of the #LocalizingSDGs platform.</p> <p>Please tell us what ‘localizing’ means to you, what sort of tools, programs and services make a difference in citizens and governments’ lives, and how a member of this community has helped you.</p> <p>We welcome your input.</p></br></br>	
	<a href='javascript:void(0);' class='contact-link linkOutlook' onclick='$('#outlook-form').show();'>Click Here to Share Your Outlook</a>",

	'name'=>"Name",
	'email'=>"Email Address",
	'organization'=>"Organization",
	'designation'=>"Designation",
	'coments'=>"Comments",
	'authorize'=>"<span class='checkboxen'>I authorize this initiative to quote my message on its online</br> <span class='checkbox1en'>platform and related publications.</span>  ",
	'wish'=>"<span class='checkbox2en'>I wish to remain anonymous.</span>",
	'send-me'=>"<span class='checkbox3en'>Send me a copied email.</span>",
	'enter Verification'=>"Enter Verification Code",
	'phone'=>"Phone No. ",
	'comments'=>"Your Comments or Questions in Detail",
	'web-link'=>"Web-link of the Issue",
	
	'send-head'=>"<h6>SEND </h6><h5>TECHNICAL FEEDBACK</h5>",

	'technical-text'=>"<p>Have you experienced any technical problems on this website? Please share your feedback! </p>

						<p><strong>Some guidelines:</strong><br/> 
						Please report one problem per message. Enter a brief description to explain your issue.</p>

						<p>Explain how to reproduce your issue: what steps you took, what went wrong, and what you think should have happened. Include examples and any error messages you received, if applicable.</p>
						<a href='javascript:void(0);' class='contact-link' onclick='$('#feedback-form').show();'>Click Here to Report Problems & Feedback</a>",

	'tech-name'=>"Name",
	'tech-email'=>"Email Address",
	'tech-Phone'=>"Phone No. ",
	'tech-organization'=>"Organization",
	'tech-coments'=>"Your comments or Question in Detail",
	'tech-Web-Link'=>"Web-link of the issue",
	'tech-Enter-Verification'=>"Enter Verification Code",
	
	'contact-head'=>"<h6>CONTACT </h6><h5>DETAILS</h5>",

	'gtf-head'=>"Global Taskforce of local and regional governments",

	'gtf-sub-head'=>"GTF",

	'gtf-address'=>"Hosted by United Cities and Local Governments World Secretariat.<br><br>Contact the GTF if you want more information on the work of local and regional governments on SDG localization and international advocacy.",

	// 'gtf-location'=>"Suite 5110, HCI Building<br/>1125 Colonel By Drive, Ottawa, K1S 586",

	// 'gtf-email'=>"ottawa@undp.com",

	// 'gtf-website'=>"www.gtf.com",



	'un-head'=>"United Nations Human Settlements Programme",

	'un-sub-head'=>"UN-HABITAT",

	'un-address'=>"Contact UN-Habitat if you are interested in knowing more about UN action towards a better urban future: where cities become inclusive and affordable drivers of economic growth and social development.",

	// 'un-location' =>"Luisenstrasse 40<br/>10117 Berlin",

	// 'un-email' =>"berlin@undp.com",

	// 'un-website' =>"www.unhabitat.com",



	'undp-head'=>"United Nations Development Programme",

	'undp-sub-head'=>"UNDP",

	'undp-address'=>"Contact UNDP if you want further information on the UN support provided to country partners, in order to effectively implement the new development agenda at the local level, and make long-term economic prosperity, human and environmental well being a reality.",

	// 'undp-location' =>"One United Nations Plaza<br />New York, NY 10017 USA",

	// 'undp-email' =>"info@undp.com",

	// 'undp-website' =>"www.undp.com",

	'outlook-confirm'=>"THANK YOU FOR SHARING YOUR THOUGHTS",

	'outlook-confirm-text'=>"Your views are extremely valuable! They help us understand what is most important to you. We make sure we read and consider your suggestions and experiences, and we might jump in to contact you from time to time.",

	'report-confirm'=>"THANK YOU FOR REPORTING YOUR ISSUE",

	'report-confirm-text'=>"We look forward to resolving your problem in a timely manner.",


];
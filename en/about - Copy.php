<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'banner-head'=>"Do you need practical<br> methods to achieve the <br>Sustainable Development Goals at the local level?",
    'banner-text'=>"The UNDP, UN-Habitat & Global Task Force are curating<br> valuable tools and guides to support you.<br>Contribute your insights as well!",
    
    'intro'=>"introducing: the toolbox",
    'purpose'=>"purpose",
    'local'=>"what is localization?",
    'intro-text'=>"<p class='animated fadeInDown'>Flexible. Simple. Yet Concrete. </p><p class='animated fadeInDown'>This Toolbox does not hold rigid guides, nor one-size-fits-all solutions. Instead, it has practical, adaptable mechanisms and instruments, which address various development challenges. The resources empower local actors and help you channel global goals into local actions.</p><p class='animated fadeInDown'>The search for the most appropriate tools and strategies for ‘localizing’ the SDGs is critical to the design, implementation, review and success of the 2030 Agenda for Sustainable Development. The tools you find here may be employed in heterogeneous and complex settings, where political, institutional, economic and social characteristics differ — not only between countries, but between territories of the same country, or even within parts of a specific territory. These guides are developed considering a wide variety of regional experiences, with different stakeholders strengthening them with inputs.</p><p class='animated fadeInDown'>Our focus is to be simple, providing applicable guidance in changing environments. It is also to remain flexible, yet concrete. We work to provide practical direction in assessing, planning, implementing and monitoring local policies, in accordance with the Sustainable Development Goals’ attainment strategies.</p>",

    'purpose-text'=>"<p class='animated fadeInDown'>The main goal of this Toolbox is to facilitate an articulated set of tools to support local stakeholders and their networks, under the leadership of local, regional and national governments. </p>

				<p class='animated fadeInDown'>The Toolbox seeks to <strong>raise awareness</strong> of the SDGs among local and national actors. It aims to improve their knowledge of the 2030 Agenda, familiarise them with the implications, opportunities and challenges in localizing it, and urge stakeholders to fully realise their crucial role. As an <strong>advocacy</strong> platform, it also seeks to create an enabling environment for the localization process, to support local ownership and ensure the SDGs integration in sub-national strategies and plans. </p>

				<p class='animated fadeInDown'>The Toolbox <strong>takes stock</strong> and reviews existing tools, systematizes outputs and avails the findings to policy-makers, local officers, experts and actors of local relevance. It aims at being a <strong>practical support</strong> for local stakeholders, and in particular local and regional governments, by pointing out best practices that are reliable and replicable in order to efficiently design, implement and monitor policies in line with the SDGs. </p>
				
				<p class='animated fadeInDown'>The process empowers actors within the new development architecture, including local, regional and national governments, civil society organizations, private firms, universities and research institutions. Our purpose is to better integrate various actors across the immensely dynamic social, regional, political and economic landscapes of territories. No matter what stage your initiative is at, or what SDG you pursue, you will find relevant, curated tools here to help achieve them.</p>",

	'local-text'=>"<p class='animated fadeInDown'>Localizing development means <strong>taking into account subnational</strong> contexts in the achievement of the 2030 Agenda, from the setting of goals and targets, to determining the <strong>means of implementation</strong> and <strong>using indicators</strong> to measure and monitor progress. It is also putting the territories and their peoples’ priorities, needs and resources at the centre of sustainable development.  There should be sustained exchanges between the global, national and local facets.</p>

				<p class='animated fadeInDown'>In the past, localization was mainly meant as the implementation of goals at the local level, by sub-national actors, in particular by local and regional governments. But this concept has evolved. All of the SDGs have targets directly related to the responsibilities of local and regional governments. That’s why the achievement of the SDGs depends, more than ever, on <strong>the ability of local and regional governments to promote integrated, inclusive and sustainable territorial development.</strong></p>

				<p class='animated fadeInDown'><strong>Subnational governments are policy makers</strong>, catalysts of change and the level of government best placed to link the global goals with local communities. Localizing development is then a process to empower all local stakeholders, aimed at making sustainable development more responsive, and therefore, relevant to local needs and aspirations. Development goals can be reached only if local actors fully participate, not only in the implementation, but also in the agenda-setting and monitoring. </p>

				<p class='animated fadeInDown'>Participation requires that public policies are not imposed from the top, but that the whole policy chain is shared. All relevant actors must be involved in the decision-making process, through <strong>consultative and participative mechanisms</strong>, at the local and national levels.</p>",

	'go-toolbox'=>"go to toolBox",
	'when'=>"<h6>When </h6><h5>it all started...</h5>",
	'work'=>"<h6>Working </h6><h5>with the tools</h5>",
	'thinks'=>"HERE'S WHAT OUR COMMUNITY THINKS",
    
    

];

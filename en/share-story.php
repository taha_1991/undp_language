	<?php

return[
	
	'share-story-head'=>"<h6>Share your</h6><h5>Story</h5>",

	'share-story-text'=>"<p>You may have a compelling story to tell about the SDGs at the local level. Be it a personal journey of local development, a new initiative you are excited about, the challenges you face or the ways in which you created positive impact. We would love to connect you to this community.</p> <p>You are also, of course, welcome to evaluate your experience using the tools provided on this platform. This will result in a more trusted experience, and it will help others make informed decisions. We want our users to easily source and publish content regarding any resource they find here.</p> <p>Please note that we might need to edit stories and their titles for length, clarity and editorial guidelines.</p>",

	'title'=>"Title of the Story",

	'story'=>"Story",

	'cover'=>"Cover Photo",

	'country'=>"Country",
	
	'select'=>"Select Country",

	'province'=>"Province / State",

	'select-province'=>"Select Province / State",

	'city'=>"City",

	'select-city'=>"Select City",

	'publications'=>"I authorize this initiative to quote my story in its campaigns and publications.",

	'mail'=>"Send me a copied mail.",

	'thank'=>"THANK YOU FOR SHARING WITH US!",

	'approval'=>"Your story will appear online upon approval.",

	'notification'=>"You will receive a notification.",

	'stories'=>"Until then, check out the other <a href=':link'><strong>Stories</strong></a>",

		'Choose-file'=>"Choose a file",

		'image-should'=>"Image should only be JPEG / PNG format and should not exceed 2Mb",




];
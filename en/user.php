<?php

return[

	'messages'=>"Messages",

	'favourites'=>"Favorites",

	'edit'=>"Edit Profile",

	'change'=>"Change Password",

	'log'=>"Log Out",

	'organ'=>"Organization",

	'location'=>"Location",

	'total'=>"Total Posts",

	'about'=>"About Me",

	'documents'=>"Documents",

	'stories'=>"Stories",

	'discussions'=>"Discussions",

	'events'=>"Events",

	'send'=>"Send a Message",

	'profile-photo'=>"Cover Photo",

	'choose-file'=>"Choose a file",

	'image-shuld'=>"Image should only be JPEG / PNG format and should not exceed 2Mb",

	'coming'=>'Coming Soon',

];

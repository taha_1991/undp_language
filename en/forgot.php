<?php

return[

	'forgot-head'=>"<h6>Forgot </h6><h5>Password</h5>",

	'forgot-text'=>"Enter your email address, and we will help you reset your password.",

	'email'=>"Email Address",

	'enter'=>"Enter Verification Code",

	'reset'=>"Reset My Password",

	'please'=>"<strong>Please check your inbox for a password reset email.</strong>",

	'click'=>"<p>Click on the link provided in the email and enter a new password.</p><p>If you have not received an email, please check your junk / spam folder. If you still have trouble finding it, click here:</p>",

	'resend'=>"Resend Email",

	'not-reg'=>"Sorry, this email address is not registered.",



];
<?php

return[
	
	'report-head'=>"<h6>Report</h6> <h5>Problem</h5>",

	'inaccurate'=>"This information is inaccurate.",

	'legitimate'=>"This information is not from a legitimate source.",

	'authority'=>"The user does not have the authority to share this information.",

	'Comments'=>"Your Comments / Suggestions",

	'Policy'=>"I agree with the <a href=':link'>Terms and Conditions</a> and <a href=':link'>Privacy Policy</a>",
	
	'thank'=>"Thank you for taking the time to report the problem that you have noticed.",



];



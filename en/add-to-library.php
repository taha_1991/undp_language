<?php

return[

	'add-head'=>"<h6>Add to the</h6><h5>Library</h5>",

	'purpose'=>"The purpose of this library",

	'add-text'=>"<p>Do you have a practical document that might help local communities achieve the targets related to the Sustainable Development Goals? Contribute to this library!</p><p>To ensure that the library is populated with relevant content, all contributions will be published after approval. Please note that we may make / suggest minor changes during moderation.</p>",

	'title'=>"Title of Document",
	'author'=>"Author",

	'developed'=>"Developed by: Organization Name(s)",

	'type'=>"Type of Document",

	'Seledoc'=> "Select Type of Document",

	'language'=>"Language",

	'Selelang'=> "Select Language",

	'upload'=>"Your upload",

	'choose'=>"Choose a file or Drop a file here",

	'image'=>"Image should be in PDF format and should not exceed 10Mb.",

	'description'=>"Please enter a brief description",

	'region'=>"Related Region",

	'Selereg'=> "Select Region",

	'country'=>"Related Country",

	'Selecon'=> "Select Country",

	'add-more'=>"Add more",

	'year'=>"Document Published In",

	'Seleyear'=> "Select Year",

	'another-language'=>"Add this Document in Another Language",

	'terms'=>"I agree with the <a href=':terms'>Terms and Conditions</a> and <a href=':privacy'>Privacy Policy</a>",

	'thank'=>"THANK YOU FOR SHARING THIS WORK",

	'contribution'=>"<p>Your contribution will appear online if the moderator approves it</p><br/><p>You will receive a notification.<p>",

	'until'=>"Until then, check out the <a href=':link'><strong>Library</strong></a>",

	'purpose-text'=>"<p>For everything that is possible to read and study on the Internet, there’s still so much out there that’s not online. Of particular interest to this community, is the untapped knowledge of development processes and discovered solutions squirrelled away in office libraries. Meanwhile, there are organizations that are ‘reinventing the wheel’ and navigating through processes that have already been completed by others.</p> 
	<p>There are amazing online collections and tools out there. An ambitious new project, Localizing the SDGs seeks to centralize this wealth of knowledge and <strong>build an online repository of documents related to achieving the SDGs at the local level</strong>. The goal is to be inclusive and participatory, and discover exactly what local communities have identified as the most effective agents of change.</p><p>And so, you are invited to contribute your insights. Sharing your work will empower other actors. It may also give some members a chance to point you in the right direction, give you feedback on your ideas, and help you find out if someone else is working on similar themes.</p>",
	

];
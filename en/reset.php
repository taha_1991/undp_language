<?php

return[

'reset-head'=>"<h6>Reset</h6> <h5>Password?</h5>",

'pswd'=>"New Password",

'retype'=>"Retype New Password",

'set-pswd'=>"Set Password ",

'Passwords-must'=>"Passwords must:",

'Passwords-must-one'=>"Include at least 8 characters.",

'Passwords-must-two'=>"Include a combination of letters and numbers.",

'Passwords-must-three'=>"Not include any special characters.",

'successfully'=>"Your password has been reset successfully!",

'return'=>"<a href='javascript:void(0);' class ='check'><strong>Click here</strong></a> to return to the login page.",

	'old-pass'=>"Old Password",


];
<?php

return[
	
	'edit'=>"<h6>EDIT</h6> <h5>PROFILE</h5>",

	'please'=>"Please fill the information you would like to share with members of this community.",

	'upload'=>"Upload Profile Picture",

	'name'=>"Name",

	'organ'=>"Organization",

	'desig'=>"Designation ",
	
	'Contact'=>"Contact Details",

	'location'=>"Location or Address",

	'telphone'=>"Telephone Number",

	'email'=>"Email Address",

	'facebook'=>"Link to Facebook Page",

	'twitter'=>"Link to Twitter Feed",

	'profile'=>"Link to LinkedIn Profile",

	'website'=>"Website link",

	'about-me'=>"About Me (maximum 300 words)",

	'pswd'=>"CHANGE PASSWORD",

	'old-pswd'=>"Old password",

	'new-pswd'=>"New password",

	'retype'=>"Retype new password",

	'change-pswd'=>"Change password",

	'pswd-must'=>"Passwords must:",

	'characters'=>"Include at least 8 characters.",

	'numbers'=>"Include a combination of letters and numbers.",

	'special-characters'=>"Not include any special characters.",

	'log-out'=>"LOG OUT",

	'logged-out'=>"You are now logged out. ",

	'click-here'=>"Click here to return to the homepage.",

];
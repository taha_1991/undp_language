<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'su'=>'',

    'links'=> [
        'about'=>"About",
        'partner'=>"Partners",
        'contact'=>"Contact Us",
        'discover'=>"Discover Tools",
        'library'=>"Library",
        'discuss'=>"Discuss & Engage",
    ],
    'languages'=>[
        'english'=>"English",
        'french'=>"Español", 
        'spanish'=>"Français"
    ],
    'login'=>"Log In",
    'footer'=>"AN INITIATIVE SUPPORTED BY",
    'all'=>"All Rights Reserved",
    'terms'=>"Terms & Conditions",
    'privacy'=>"Privacy Policy",
    'tool'=>"Tools",
    'document'=>"Documents",
    'story'=>"Stories",
    'discussion'=>"Discussions",
    'event'=>"Events",
    'vid-link'=>'<iframe width="532" height="300" src="https://www.youtube.com/embed/PRq8D_W1F84?enablejsapi=1" frameborder="0" allowfullscreen></iframe>',
    'home'=>"Home",
    'search-results'=>"Search Results",
    'search-results-for'=>"Search results for:",
    'partner'=>"Partner",
    'partner-listing'=>"Partners' Listing",
    'discover-tools'=>"Discover Tools",

    'general'=>"General elements",
    'develop'=>"Developed by",
    'posted'=>"Posted by",
    'moderated'=>"Moderated by ",
    'organized'=>"Organized by ",
    'view-all'=>"View All",
    'view-more'=>"View More",
    'add-to-cal'=>"Add to Calendar",
    'published'=>"Published In",
    'sub'=>"Submit",
    'january'=>"January",
    'february'=>"February",
    'march'=>"March",
    'april'=>"April",
    'may'=>"May",
    'june'=>"June",
    'july'=>"July",
    'august'=>"August",
    'september'=>"September",
    'october'=>"October",
    'november'=>"November",
    'december'=>"December ",
    'jan'=>"Jan",
    'feb'=>"Feb",
    'mar'=>"Mar",
    'apr'=>"Apr",
    'may'=>"May",
    'jun'=>"Jun",
    'jul'=>"Jul",
    'aug'=>"Aug",
    'sept'=>"Sept",
    'oct'=>"Oct",
    'nov'=>"Nov",
    'dec'=>"Dec",
    'first'=>"1st",
    'second'=>"2nd",
    'third'=>"3rd",

    'search'=>"Search",

    'submit'=>"Submit",
    'home'=>"Home",
    'result'=>"Search Results",
    'raise'=>"Raising Awareness",
    'library'=>"Library",
    'stories'=>"Stories",
    'discussions'=>"Discussions",
    'events'=>"Events",
    'register'=>"Register",
    'active'=>"Active Account",
    'forgot'=>"Forgot Password",
    'reset'=>"Reset Password",
    'messages'=>"Messages",
    'add-to-library'=>"Add to the library",
    'report'=>"Report Flag",
    'lead'=>"Lead a Discussion ",
    'share-story'=>"Share Your Story ",
    'share-event'=>"Share an Event",



];
<?php

return[
	
	'share-event-head'=>"<h6>Share an</h6> <h5>Event</h5>",

	'share-event-text'=>"<p>This space is open to organizations, individuals and communities for promoting their events. We would be glad to know about events that start conversations and add voices to the world of local development. </p>
	<p>You may request to feature events, such as interactive workshops, community talks, lectures, conferences, summits etc. related to localizing the SDGs. In doing so, you will allow this community to be wholly engaged with you. </p>
	<p>To ensure that this calendar is populated with relevant continent, all promotion requests will be featured upon approval.</p>",

	'title'=>"Event Title",

	'organ'=>"Organized By",

	'description'=>"Description",

	'file'=>"File ",
	
	'edit'=>"Edit",

	'insert'=>"Insert",

	'view'=>"View",

	'format'=>"Format",

	'table'=>"Table",

	'tools'=>"Tools",

	'date'=>"Date",

	'Seledate'=>"Select Date",

	'time'=>"Time",

	'Selefrom'=>"From",

	'Seleto'=> "To",

	'country'=>"Country",

	'Selecoun'=>"Select Country",

	'state'=>"Province / State",

	'Selestate'=>"Select Province / State",

	'city'=>"City",

	'Selecity'=>"Select City",

	'venue'=>"Venue",

	'cover-photo'=>"Cover Photo",

	'upload'=>"Upload Additional Information",

	'add-more-docu'=>"Add More Documents",

	'agenda-pdf'=>"Please format the title of your document to indicate its content<br> (e.g. Concept Note.pdf / Agenda.pdf)",

	'web-link'=>"Event web-link",

	'thank-you'=>"THANK YOU FOR SHARING THIS EVENT.",

	'it-will'=>"It will be promoted online once the moderator approves it.",

	'you-will'=>"You will receive a notification.",

	'until-then'=>"Until then, check out the other <a href=':link'><strong>Events</strong></a>",

	'choose-file'=>"Choose a file",

	'image-should'=>"Image should only be JPEG / PNG format and should not exceed 2Mb",



];
<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'core'=>"<h6>Core</h6> <h5>partners</h5>",
    'gtf'=>"Global Taskforce of local and regional governments",
    'gtf-small'=>"GTF",
    'gtf-text'=>"The <strong>Global Taskforce of Local and Regional Governments</strong> is a coordination mechanism that brings together the major international networks of local governments to undertake joint advocacy. These efforts relate to international policy processes, particularly the climate change agenda, the Sustainable Development Goals and Habitat III.",
    'habitat'=>"United Nations Human Settlements Programme",
    'habitat-small'=>"UN-HABITAT",
    'habitat-text'=>"<strong>UN-Habitat</strong> is the lead agency for human settlements. It is mandated by the UN General Assembly to promote socially and environmentally sustainable towns and cities, with the goal of providing adequate shelter for all. UN-Habitat also has the specific mandate within the United Nations System to act as a focal point for local governments and their associations, supporting local and territorial governments as essential agents for development.",
    'undp'=>"United Nations Development Programme",
    'undp-small'=>"UNDP",
    'undp-text'=>"<strong>UNDP</strong> works in nearly 170 countries and territories, helping to achieve the eradication of poverty, and the reduction of inequalities and exclusion. We help countries to develop policies, leadership skills, partnering abilities, institutional capabilities and resilience in order to sustain development results. UNDP also focuses its work on strengthening the inclusiveness and accountability of sub-national governments. It ensures they have the capacity to manage the opportunities and responsibilities created by decentralization and devolution. It also seeks to optimize their potential and role as development partners.",
    'contributing'=>"<h6>Contributing</h6><h5>partners</h5>",
    'contributing-text'=>"The genuine participation of all actors is a cornerstone of this initiative. We would be pleased to represent other voices, as we receive more contributions from these and other sectors.",
    
    'lrg'=>"Local and Regional Governments",
    'ng'=>"National <br>Governments",
    'io'=>"International <br>Organizations",
    'cso'=>"Civil Society <br>Organizations",
    'ps'=>"Private <br>Sector",
    'ari'=>"Academic and Research Institutions",
    
    'lrg-head'=>"<h6>Local and regional</h6> <h5>governments</h5>",
    'ng-head'=>"<h6>National</h6> <h5>governments</h5>",
    'io-head'=>"<h6>International</h6> <h5>organizations</h5>",
    'cso-head'=>"<h6>Civil Society</h6> <h5>organizations</h5>",
    'ps-head'=>"<h6>Private</h6> <h5>Sector</h5>",
    'ari-head'=>"<h6>Academic & Research </h6> <h5>institutions</h5>",
						
	'go-to-website'=>"Go to the Website",
	'why'=>"Why are they important to the SDGs?",
	'role'=>"Role in localizing the SDGs:",
	
	'lrg-pop-head'=>"Local and regional governments (and their associations)",
	'ng-pop-head'=>"NATIONAL GOVERNMENTS",
	'cso-pop-head'=>"CIVIL SOCIETY ORGANIZATIONS",
	'io-pop-head'=>"International Organizations",
	'ps-pop-head'=>"PRIVATE SECTOR",
	'ari-pop-head'=>"ACADEMIC AND RESEARCH INSTITUTIONS",
	
	'lrg-role'=>"<ul><li>As the sphere of government closest to the people and their first gate for participation in public affairs, LRGs are in a unique position to identify and respond to development needs and gaps, supporting local goals and national objectives.   </li>
				<li>Their political leadership is instrumental to foster coordination and coherence among different tiers of government. </li>
				<li>Their convening power plays a facilitative role in achieving consensus among different institutional actors, interest groups and stakeholders (private, public, academia, etc.) They can take the lead in promoting joint visioning and strategic planning in their specific areas.</li>
				<li>LRGs are also best placed to maintain development effectiveness at the local level. They strengthen ownership of projects on the ground; promote inclusive partnerships between public authorities, private sector and CSOs; focus on tangible results; and increase accountability and transparency. </li>
				<li>The active role of LRGs in international development cooperation is crucial to achieving development results, democratizing processes, and promoting inclusive ownership.</li>
				<li>LRG associations represent a diversity of local governments (in terms of size, geography, capacity, etc.). They play an advocating role, training their members and speaking with a unified voice with other actors. </li>
			</ul>
			<p>The Toolbox attempts to identify the most effective tools, mechanisms and existing best practices for strengthening the capacities of LRGs to more effectively promote and implement the SDGs within their territories. It aims to enable LRGs to become the champions and facilitators of local level integrated, sustainable and inclusive development processes.  </p>",
	'ng-role'=>"<ul><li>National governments lead the development policies linked to the SDG agenda. They have a <strong>development and coordination role </strong>in the SDG implementation process. They are responsible for defining and developing coherent national strategies in line with the SDGs. </li>
				<li>National governments’ commitment is crucial to facilitate an effective localization of the SDGs. They are the ones ultimately setting up an <strong>enabling environment  </strong>and a decentralized framework, which allows local and regional governments and stakeholders to deliver services. </li>
				<li>They have the capacity to establish dialogue and collaborative relations with the spectrum of stakeholders involved in local development. They may design appropriate instruments to make the stakeholders, particularly the LRGs, capable of managing local development strategies.</li>
				<li>National governments are accountable to their citizens for the implementation of the SDGs but also to the international community via annual dialogues at the High-Level Political Fora. They may <strong>show concrete successes </strong> from the ground, as they prepare for regional and global dialogues on SDG implementation. </li>
				<li>They are responsible <strong>for designing a national monitoring framework and for reporting </strong>, with an associated set of national indicators. These include disaggregated indicators that must be collected at the local level. National governments collect feedback from the local level, so they may report on the progress of implementation, as they compile or update the annual sustainable development reports.</li>
			</ul>
			<p>The Toolbox proposes instruments, methodologies and existing best practices to strengthen the capacities of national governments, to effectively include LRGs in defining, implementing and monitoring the SDGs’ strategies.</p>",
			
	'io-role'=>"<ul><li>International Organizations can support communication of the new agenda, strengthening partnerships for implementation, and filling in the gaps in available data for monitoring and review.  </li>
				<li>Within this framework, in particular, the United Nations Development Group has developed a strategy for effective and coherent implementation support of the new sustainable development agenda under the acronym ‘MAPS’ (Mainstreaming, Acceleration, and Policy Support).  </li>
				<li>Through the MAPS approach, the United Nations support countries in three different ways. Firstly, by providing support to governments to reflect the new global agenda in national development plans and policies. This work is already underway in many countries at national request. Secondly, by supporting countries to accelerate progress on SDG targets. Lastly, by making the UN’s policy expertise on sustainable development and governance available to governments at all stages of implementation. </li>
				<li>Well-equipped with this integrated package of policy support services, the United Nations and its agencies and programmes stand ready to support country partners to effectively implement the new development agenda and make long-term economic prosperity, human and environmental well-being a reality.</li>
				
			</ul>
			<p>The Toolbox proposes instruments, methodologies and existing best practices to effectively land the SDGs at local level, with the support of IOs. </p>",
	'cso-role'=>"<ul><li>CSOs are instrumental to the implementation of the SDGs because they require <strong>systematic community outreach</strong> and volunteer engagement. They can help <strong>raise awareness of the SDGs </strong>at the community level. They also have an advocacy role in national and international campaigns, providing examples of how to make the 2030 Agenda relevant and grounded in local action. </li>
				<li>CSOs play important mediation roles in policy development: They provide a link between the public and decision makers. They help identify critical development priorities, suggest practical solutions and detect policy opportunities for localizing the SDGs.</li>
				<li>Participatory CSOs may foster the possibility of <strong>integrating citizens’ voices in the decisional process </strong>, especially with regard to LRGs. CSOs may offer neutral spaces for regular multi-stakeholder dialogue, and they may create connections to decision making processes and implementation measures. Coordination mechanisms may help to properly articulate territorial cooperation and ensure the effective localization of the SDGs.</li>
				<li>CSOs can provide tools for the citizens to <strong>monitor progress</strong> and hold their governments accountable for the implementation of the SDGs in a transparent and participatory way. </li>
				<li>They can also train <strong>relevant stakeholders</strong> and <strong>decision-makers</strong> in implementation and enforcement practices.</li>
				<li>CSOs can ensure transparent data collection and gather reliable and globally used data on issues relevant to the SDGs, both with and alongside governments.</li>
			</ul>
			<p>The Toolbox focuses on the analysis of the role of CSOs for the localization of development policies. It proposes practical tools to make their experience valuable for a more effective implementation of the SDGs. </p>",
	'ps-role'=>"<ul>
				<li>When flexible and social based, business can be a strategic and valuable <strong>means for reaching local people </strong>and meeting local needs.  </li>
				<li>It can support specific governance issues and be engaged in the SDGs’ implementation. Regulatory structures and incentive systems can foster cooperation, encouraging <strong>public-private partnerships and finance mechanisms</strong>.</li>
				<li>Business should be included in the planning of national and local agendas to implement the SDGs. When governments include the private sector in the planning, not just the implementation, companies <strong>develop a shared ownership</strong> of challenges and potential solutions. </li>
				<li>Inclusive partnerships among governments, companies and civil society organizations are central to allow dialogue, set common policy agendas, mobilize resources, and ensure shared accountability. They help address systemic challenges beyond individual initiatives and project-based partnerships. Business can generate <strong>linkages in key areas such as research and innovation, vocational training,</strong> value chain development, small and medium enterprises’ growth, etc.   </li>
				<li><strong>Private sector investment and market-based solutions </strong>are essential to achieve scale and sustained impact. A business model and a development model can align to create a hybrid strategy. For instance, philanthropic and commercial dimensions can be combined to leverage different competencies and resources.  </li>
				<li>Business can help <strong>create quantifiable metrics </strong>for the SDGs, with respect to targeting, monitoring, evaluation and accountability. This will help measure the impact of business operations, align corporate indicators, and integrate the results into performance management and reporting mechanisms. Independent monitoring systems should also verify the results of development partnerships. </li>
			</ul>
			<p>The Toolbox sets to identify and document existing practices and tools. It aims to build on possible channels to heighten the awareness, commitment and mobilization of private actors, in partnership with other stakeholders, for the implementation of the SDGs.  </p>",
	'ari-role'=>"<ul>
				<li>Universities play a crucial role in <strong>capacitating qualified human resources.</strong> They can educate and train citizens and managers on localizing the SDGs - and more in general - on the importance of development policies. </li>
				<li>They conduct research on development and provide <strong>research inputs </strong>for policymaking. This increases the stock of knowledge about the SDGs, creating policy-relevant knowledge. The effort helps define proper policy and conceive governance schemes to localize the SDGs.</li>
				<li>Academia plays the main role of legitimizing and <strong>defining theoretical frameworks </strong>to practical ideas, and making them accessible to communities and to the public.</li>
				<li>Academic and research institutions can also play a role in <strong>disseminating knowledge  </strong>about Toolbox mechanisms, enriching the scientific debate about development localization. They can share information, data, materials and experiences on localizing processes. </li>
				<li>They can contribute to policy dialogue in their countries and in the international community by strengthening collaboration with public officials and decision-makers, and <strong>building partnerships</strong>.</li>
			</ul>
			<p>The Toolbox revises existing best practices and case histories about the role of academia in development, proposing practical tools to strengthen the involvement of academia in the implementation of the SDGs.</p>",
			'nudge'=>"<span class='nudge-text'>Are you sharing tools, documents, stories and events, or moderating discussions? <br>
We would be happy to list you as a partner in Localizing the SDGs. <br></span>
<span class='nudge-main'>Please <a href='mailto:info.art@undp.org'>contact us</a> to be featured.</span>  ",
];

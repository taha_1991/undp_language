<?php

return[

	'stories-discussions-head'=>"<h5>STORIES, DISCUSSIONS & EVENTS</h5>",

	'stories-text'=>"A rich ground to exchange ideas, experiences and events for everyone.",

	'local-head'=>"<h6>Local</h6><h5>Stories</h5>",

	'current-head'=>"<h6>Current</h6><h5>Discussions</h5>",

	'events-head'=>"<h5>EVENTS</h5>",

	'share-btn'=>"Share Your Story",

	'lead-btn'=>"Lead a Discussion",

	'post-btn'=>"Post an Event",

	'stories-head'=>"<h6>latest</h6><h5>Stories</h5>",

	'your-stories'=>"<strong>Your stories are central to everything.</strong><br> Be it trying or triumphant, insightful or inspiring, we would love to hear your story and consider it for publication. ",

	'share-your-story-btn'=>"Share your story",

	'discussion-head'=>"<h5>Discussions</h5>",

	'evnt-head'=>"<h5>Events</h5>",

	'have-any'=>"Have any story that you’d like to contribute?",

	'we-love'=>"We’d love to have it!",
	
	'share-button'=>"Share Your Story",

	'facebook-btn'=>"Share on Facebook",

	'twitter-btn'=>"Share on Twitter",

	'email-btn'=>"Share via Email",

	'contact-btn'=>"Contact Contributor ",

	'add-to'=>"Add to Favorites",
	
	'report-btn'=>"Report a Problem ",

	'other-head'=>"<h6>Other</h6><h5>Stories</h5>",

	'comments'=>"Comments",

	'write-comm'=>"Write your comments",

	'post'=>"Post",

	'responses'=>"Response|Responses",

	'reply'=>"Reply",

	'report'=>"Report",

	'today'=>"Today",

	'yesterday'=>"Yesterday",

	'days-ago'=>":number Days Ago",

	'administrator'=>"*Comments may later be moderated if found unsuitable or offensive. The discretion lies solely with the administrator.",

	'latest-head'=>"<h6>latest</h6><h5>Discussions</h5>",

	'chat-responses'=>"Responses",

	'we-like-to'=>"We’d like to discuss topics that are important to you. If your organization wishes to propose and lead a discussion on a subject applicable to the #LocalizingSDGs community, we’d be happy to feature it. Suggest a topic and be the next moderator. ",

	'suggest-btn'=>"Suggest a Discussion Topic",

	'previous-heading'=>"<h6>Previous</h6><h5>Discussions</h5>",

	'stories-heading'=>"Stories",

	'events-heading'=>"Events",

	'does-your'=>"Does your organization wish to start a discussion? ",

	'featured'=>"We’d love to feature it! ",

	'suggest-a-topic'=>"Suggest a Topic",

	'feature'=>"Add Comment",

	'other-current'=>"<h6>Other Current</h6><h5> Discussions</h5>",

	'previous-discussions'=>"<h6>Previous </h6><h5>Discussions</h5>",

	'we-look-forward'=>"We look forward to reading your insights and perspectives. As you frame your comment, please extend meaningful discussion with concise and pertinent responses. You may build on previous posts, teach us something new, or even apply concepts in new ways.",

	'search-head'=>"<h6>Search events</h6><h5>closest to you</h5>",

	'searchbar-text'=>"Search",

	'upcoming-head'=>"<h6>Upcoming</h6><h5>Events</h5>",

	'help-us'=>"Help us join the voices that speak of localizing the SDGs. By sharing an event that helps further the initiative, you create more opportunities for people and stakeholders to engage and benefit. Be it a lecture, seminar or workshop, we’d like to showcase your event to the #LocalizingSDGs community.",

	'submit-an-event'=>"Submit an Event",

	'past-head'=>"<h6>Past</h6><h5>Events<h5>",

	'promote'=>"Have any event that you’d like to promote?",

	'feature-it'=>"We’d love to feature it!",

	'share'=>"Share an event",

	'other'=>"<h6>Other</h6><h5> events</h5>",
	'discuss-comment'=>'We look forward to reading your insights and perspectives. <br>As you frame your comment, please extend meaningful discussion with concise and pertinent responses. <br>You may build on previous posts, teach us something new, or even apply concepts in new ways.',
	
];